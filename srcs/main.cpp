//
// main.cpp for moulitriche in /home/neut_g/PROJETS/Tek3/moulitriche/git/trichounette
//
// Made by Grégory Neut
// Login   <neut_g@epitech.net>
//
// Started on Sat Jan 11 14:45:15 2014 Grégory Neut
// Last update Mon May  5 15:26:39 2014 geoffrey bauduin
//

#include	<getopt.h>
#include	"Core.hh"
#include	"Logger.hh"
#include	"Option.hpp"
#include        "Date.hh"

static void	usage(char *prog)
{
  std::cout << "Usage : " << prog << " [OPTION]... filename" << std::endl;
  std::cout << "\t-v, --verbose     :    Print more informations about what is going on." << std::endl;
  std::cout << "\t-h, --help        :    Print the usage of the program." << std::endl;
  std::cout << "\t-f, --hardverbose :    Print in details what is going on. Hardverbose include verbose." << std::endl;
}

int				main(int argc, char **argv)
{
  Logger::initialize();
  Option::initialize();
  option long_options[] = {
    {"verbose", no_argument, 0, 'v'},
    {"help", no_argument, 0, 'h'},
    {"hardverbose", no_argument, 0, 'f'},
  };
  int index = 0;
  int c;
  std::string filename("");
  do {
    c = getopt_long(argc, argv, "fvh", long_options, &index);
    switch (c) {
    case -1:
      if (argv[optind] != NULL) {
	filename = argv[optind];
      }
      break;
    case 'f':
      Option::getSingleton()->setOption(Option::HARDVERBOSE);
    case 'v':
      Option::getSingleton()->setOption(Option::VERBOSE);
      break;
    case 'h':
      usage(*argv);
      return (0);
    case '?':
      usage(*argv);
      return (1);
    default:
      break;
    }
  } while (c != -1);
  int ret = 0;
  if (filename.empty()) {
    usage(*argv);
    ret = 1;
  }
  else {
    try {
      Core core(filename);
      core.run();
    }
    catch (std::exception &e) {
      std::cerr << "Catched an exception : " << e.what() << std::endl;
      ret = 1;
    }
    catch (std::exception *e) {
      std::cerr << "Catched an exception : " << e->what() << std::endl;
      delete e;
      ret = 1;
    }
  }
  Logger::dispose();
  Option::dispose();
  return (ret);
}
