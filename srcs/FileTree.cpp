//
// FileTree.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:53:17 2012 romuald scharre
// Last update Tue Jun 19 09:53:18 2012 romuald scharre
//

#include					"FileTree.hh"

FileTree::FileTree()
{

}

std::unordered_map<std::string,
		   std::vector<std::string> >	&FileTree::getAllTree()
{
  return (_tree);
}

std::vector<std::string>			&FileTree::getTreeByKey(const std::string &key)
{
  return (_tree[key]);
}

void						FileTree::push(const std::string &key, const std::string &value)
{
  _tree[key].push_back(value);
}

void						FileTree::erase(const std::string &key, const std::string &value)
{
  std::unordered_map<std::string,
		     std::vector<std::string> >::iterator	it;
  std::vector<std::string>::iterator		itPath;

  if ((it = _tree.find(key)) != _tree.end())
    {
      for (itPath = (it->second).begin(); itPath != (it->second).end(); ++it)
	{
	  if ((*itPath).compare(value) == true)
	    {
	      (it->second).erase(itPath);
	      return ;
	    }
	}
    }
}

void						FileTree::eraseByKey(const std::string &key)
{
  std::unordered_map<std::string,
		     std::vector<std::string> >::iterator	it;

  if ((it = _tree.find(key)) != _tree.end())
    _tree.erase(it);
}

void						FileTree::clearAll()
{
  std::unordered_map<std::string,
		     std::vector<std::string> >::iterator	it;

  for (it = _tree.begin(); it != _tree.end(); ++it)
    (it->second).clear();
  _tree.clear();
}
