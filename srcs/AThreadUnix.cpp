//
// AThreadUnix.cpp for  in /home/romuald/Projects/moulitriche
//
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
//
// Started on  Tue Jun 19 09:55:10 2012 romuald scharre
// Last update Fri Jan 10 14:28:17 2014 Grégory Neut
//

#include			"AThreadUnix.hh"

AThreadUnix::AThreadUnix()
{

}

AThreadUnix::~AThreadUnix()
{

}

bool				AThreadUnix::Create(void *(*func)(void *), void *param)
{
  if (pthread_create(&_t, NULL, func, param) != 0)
    return (false);
  return (true);
}

bool				AThreadUnix::Destroy()
{
  if (pthread_cancel(_t) != 0)
    return (false);
  return (true);
}

bool				AThreadUnix::Wait()
{
  if (pthread_join(_t, NULL) != 0)
    return (false);
  return (true);
}
