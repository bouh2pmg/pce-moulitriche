//
// LanguageDetector.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:53:31 2012 romuald scharre
// Last update Tue Nov 25 22:45:30 2014 Philip Garnero
//

#include				"LanguageDetector.hh"

using namespace				boost::filesystem;

LanguageDetector::LanguageDetector(std::vector<std::pair<ILoader *, ILanguage *> > &language) : _vLanguage(language)
{

}

bool				LanguageDetector::getByShebang(std::vector<std::string> &vShebang,
							       const std::string &file)
{
  std::vector<std::string>::iterator	it;
  std::ifstream				infile;
  std::string				line;

  infile.open(file.c_str());
  if (infile.is_open() == true)
    {
      std::getline(infile, line);
      if (line != "" && line[0] == '#')
	{
	  for (it = vShebang.begin(); it != vShebang.end(); ++it)
	    if (RE2::PartialMatch(line.c_str(), std::string("(" + *it + ")").c_str()) == true)
	      {
		infile.close();
		return (true);
	      }
	}
      infile.close();
    }
  return (false);
}

bool				LanguageDetector::getByExtension(std::vector<std::string> &vExt,
								 const std::string &file)
{
  std::vector<std::string>::iterator	it;

  for (it = vExt.begin(); it != vExt.end(); ++it)
    if (RE2::PartialMatch(file.c_str(), std::string("(" + *it + ")").c_str()) == true)
      return (true);
  return (false);
}

void				LanguageDetector::flag(FileTree &tree, const std::string &file)
{
  std::vector<std::pair<ILoader *,
			ILanguage *> >::iterator	it;
  bool				identifier = false;

  for (it = _vLanguage.begin(); it != _vLanguage.end(); ++it)
    {
      if ((it->second)->cleanRelease(file) == true)
	return ;
      if ((identifier = getByExtension((it->second)->identifier().first, file)) == true)
	{
	  tree.push((it->second)->getLanguageName(), file);
	  break;
	}
      else
	{
	  if ((identifier = getByShebang((it->second)->identifier().second, file)) == true)
	    {
	      tree.push((it->second)->getLanguageName(), file);
	      break;
	    }
	}
    }
}

