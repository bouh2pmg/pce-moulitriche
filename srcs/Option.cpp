//
// Option.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed Apr  9 09:55:50 2014 geoffrey bauduin
// Last update Wed Apr  9 10:00:57 2014 geoffrey bauduin
//

#include	"Option.hpp"

Option::Option(void): _options(Option::NONE) {

}

Option::~Option(void) {

}

void	Option::setOption(Option::Opt option) {
  this->_options |= option;
}

void	Option::unsetOption(Option::Opt option) {
  this->_options &= ~(option);
}

bool	Option::hasOption(Option::Opt option) const {
  return ((this->_options & option) == option);
}
