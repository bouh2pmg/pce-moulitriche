//
// Instance.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:53:27 2012 romuald scharre
// Last update Wed Nov 20 15:26:40 2013 Grégory Neut
//

#include			"Instance.hh"

IMutex				*Instance::getMutexInstance()
{
  IMutex			*mut;

  mut = new MutexUnix();

  mut->Initialise();
  return (mut);
}

IThread				*Instance::getThreadInstance()
{
  return (new AThreadUnix());

}

ILoader				*Instance::getLoaderInstance()
{
  return (new LoaderUnix());

}
