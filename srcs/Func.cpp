//
// Func.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:53:06 2012 romuald scharre
// Last update Fri Apr 25 11:09:40 2014 geoffrey bauduin
//

#include		"Func.hh"

Func::Func() : _content(0), _finalContent(""), _name(""), _percent(0), _nbHit(0), _path("")
{

}

const VectorString		&Func::getContent() const
{
  return (_content);
}

const std::string		&Func::getFinalContent() const
{
  return (_finalContent);
}

std::string			&Func::getFinalContent()
{
  return (_finalContent);
}

const std::string		&Func::getName() const
{
  return (_name);
}

const std::string		&Func::getPath() const
{
  return (_path);
}

float				Func::getPercent() const
{
  return (_percent);
}

int				Func::getNbHit() const
{
  return (_nbHit);
}

void				Func::addContent(const std::string &content)
{
  _content.push_back(content);
}

void				Func::addFinalContent(const std::string &content)
{
  _finalContent += content + "\n";
}

void				Func::setPercent(float percent)
{
  _percent = percent;
}

void				Func::setName(const std::string &name)
{
  _name = name;
}

void				Func::setPath(const std::string &path)
{
  _path = path;
}

VectorString			&Func::content()
{
  return (_content);
}

void				Func::setNbHit(int nbHit)
{
  _nbHit = nbHit;
}

std::ostream			&operator<<(std::ostream &os, const Func &func)
{
  VectorString::const_iterator	it;

  os << "Path : "<< func.getPath() << std::endl;
  os << "Hit : "<< func.getNbHit() << " : Percent : " << func.getPercent() << "%" << std::endl;
  os << "Func name : " << func.getName() << std::endl;
  if (func.getFinalContent() == "")
    {
      for (it = func.getContent().begin(); it != func.getContent().end(); ++it)
	os << *it << std::endl;
    }
  else
    os << func.getFinalContent() << std::endl;
  return (os);
}
