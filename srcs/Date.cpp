//
// Date.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:53:11 2012 romuald scharre
// Last update Sat Jan 11 15:47:42 2014 Grégory Neut
//

#include				"Date.hh"

Date::Date()
{

}

Date::~Date()
{

}

const std::string		Date::getDate()
{
  std::string			day[8] = {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi",
					  "Vendredi", "Samedi", "\0"};
  std::string			month[13] = {"Janvier", "Fevrier", "Mars", "Avril", "Mai",
					     "Juin", "Juillet", "Aout", "Septembre", "Octobre",
					     "Novembre", "Decembre", "\0"};
  struct tm			*timeval;
  time_t			t;
  std::ostringstream		_day;
  std::ostringstream		_year;
  std::string			date;

  date.clear();
  t = time(&t);
  timeval = localtime(&t);
  date += day[timeval->tm_wday] + "_";
  _day << timeval->tm_mday;
  date += _day.str() + "_";
  date += month[timeval->tm_mon] + "_";
  _year << 1900 + timeval->tm_year;
  date += _year.str();
  return (date);
}

const std::string		Date::getTime()
{
  struct tm			*timeval;
  time_t			t;
  std::string			_time;
  std::ostringstream		hour;
  std::ostringstream		min;
  std::ostringstream		sec;

  _time.clear();
  _time += "-- ";
   t = time(&t);
  timeval = localtime(&t);
  if (timeval->tm_hour < 10)
    hour << 0 << timeval->tm_hour;
  else
    hour << timeval->tm_hour;
  _time += hour.str() + ":";
  if (timeval->tm_min < 10)
    min << 0 << timeval->tm_min;
  else
    min << timeval->tm_min;
  _time += min.str() + ":";
  if (timeval->tm_sec < 10)
    sec << 0 << timeval->tm_sec;
  else
    sec << timeval->tm_sec;
  _time += sec.str() + " -- ";
  return (_time);
}
