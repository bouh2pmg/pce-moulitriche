//
// AMutexUnix.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:55:13 2012 romuald scharre
// Last update Tue Jun 19 09:55:14 2012 romuald scharre
//

#include				"MutexUnix.hh"

MutexUnix::MutexUnix()
{
  _mutex = PTHREAD_MUTEX_INITIALIZER;
}

MutexUnix::~MutexUnix()
{

}

bool					MutexUnix::Initialise()
{
  if (pthread_mutex_init(&_mutex, NULL) != 0)
    return (false);
  return (true);
}

bool					MutexUnix::Destroy()
{
  if (pthread_mutex_destroy(&_mutex) != 0)
    return (false);
  return (true);
}

bool					MutexUnix::Lock()
{
  if (pthread_mutex_lock(&_mutex) != 0)
    return (false);
  return (true);
}

bool					MutexUnix::Unlock()
{
  if (pthread_mutex_unlock(&_mutex) != 0)
    return (false);
  return (true);
}

bool					MutexUnix::Trylock()
{
  if (pthread_mutex_trylock(&_mutex) != 0)
    return (false);
  return (true);
}
