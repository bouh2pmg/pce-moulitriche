//
// Cheaters.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Mon Jun 18 15:19:22 2012 romuald scharre
// Last update Fri Jun 22 15:49:18 2012 romuald scharre
//

#include			"Cheaters.hh"

Cheaters::Cheaters(const std::pair<Release *, Release *> &couple, const std::pair<float, float> &power,
		   const std::string &language) : _couple(couple), _power(power), _language(language)
{

}

Cheaters::~Cheaters()
{

}

std::pair<Release *, Release *>	&Cheaters::getCouple()
{
  return (_couple);
}

std::pair<float, float>		&Cheaters::getPower()
{
  return (_power);
}

const std::string		&Cheaters::getLanguage() const
{
  return (_language);
}

std::pair<float, float>		&Cheaters::getPercent()
{
  return (_percent);
}

int				Cheaters::getNbHit() const
{
  return (_nbHit);
}

void				Cheaters::setPercent(std::pair<float, float> &percent)
{
  _percent = percent;
}

void				Cheaters::setNbHit(int nbHit)
{
  _nbHit = nbHit;
}

std::ostream			&operator<<(std::ostream &os, Cheaters &cheaters)
{
  Release			*r1, *r2;

  r1 = cheaters.getCouple().first;
  r2 = cheaters.getCouple().second;
  os << "Cheaters 1 : {" << r1->getLogin() << ", " << r1->getPromo() << ", " << r1->getCity()
     << ", " << r1->getPotentiel(cheaters.getLanguage())
     << ", " << std::fixed << std::setprecision(2) << cheaters.getPower().first << ", " << cheaters.getLanguage() << "}"
     << "\t\t\tCheaters 2 : {" << r2->getLogin() << ", " << r2->getPromo() << ", " << r2->getCity()
     << ", " << r2->getPotentiel(cheaters.getLanguage())
     << ", " << std::fixed << std::setprecision(2) << cheaters.getPower().second << ", " << cheaters.getLanguage() << "}";
  return (os);
}
