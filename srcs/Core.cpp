/*
// Core.cpp for  in /home/romuald/Projects/moulitriche
//
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
//
// Started on  Tue Jun 19 09:43:55 2012 romuald scharre
// Last update Tue Nov  8 12:08:33 2016 raphael defreitas
*/

#include	"Core.hh"
#include	"Date.hh"
#include	"Instance.hh"
#include	"Logger.hh"
#include	"Convert.hh"
#include	"Option.hpp"
#include	"Config.hh"

using namespace				boost::filesystem;

/**
 ** Fonction utilitaire qui remplace une chaine par une autre dans une chaine et
 ** qui enregistre le résultat dans une chaine passée en paramètre
 */
void					replace(std::string &chain, std::string old, std::string newChar, int pos)
{
  int					s;

  if ((s = chain.find(old, pos)) >= 0 && s >= pos)
    {
      if (chain[s - 1] != '\\' || (chain[s - 1] == '\\'  && s - 2 > 0 && chain[s - 2] == '\\'))
	{
	  chain.insert(s, newChar);
	  replace(chain, old, newChar, s + 2);
	}
      else
	replace(chain, old, newChar, s + 1);
    }
}

/**
 ** Constructeur de la classe Core
 **
 ** Remarques :
 **
 ** - Prise en charge d'un fichier de conf en paramètre : conf.xml
 ** - Nom des villes en Dur
 ** - Charge un fichier default.xml si le chemin est vide.
 **
 ** @param path Chemin vers le fichier de configuration (par défaut "")
 **
 */
Core::Core(const std::string &path) : _conf(NULL),
				      _languageDetector(_vLanguage),
				      _nbCheater(0),
				      _nbRequest(0) {
  this->_conf = new Project::Parser(path);
  _mCities["lil"] = "lille";
  _mCities["bdx"] = "bordeaux";
  _mCities["lyo"] = "lyon";
  _mCities["mpl"] = "montpellier";
  _mCities["msl"] = "marseille";
  _mCities["nce"] = "nice";
  _mCities["nts"] = "nantes";
  _mCities["prs"] = "paris";
  _mCities["stg"] = "strasbourg";
  _mCities["tls"] = "toulouse";
  _mCities["ncy"] = "nancy";
  _mCities["rns"] = "rennes";
  boost::filesystem::path directory = CURRENT_DIR + "/trichounette";
  if (boost::filesystem::exists(directory)) {
    boost::filesystem::remove_all(directory);
  }
  boost::filesystem::create_directory(directory);
  (void) Config::getSingleton();
}

/**
** Destructeur de la classe
*/
Core::~Core()
{
  delete this->_conf;
}

/**
** Fonction qui parse l'arborescence de dossier et ressort l'année de promotion
**
** Phrase de base : "login_l-2016"
**
** ressort        : "2016"
**
** Découpe par rapport au -
**
*/
int					Core::getPromo(const std::string &path)
{
  int					idx;
  std::istringstream			iss;
  int					value;

  for (idx = path.size(); idx > 0 && path[idx] != '-'; --idx);
  ++idx;
  iss.str(path.substr(idx, path.size() - idx + 1).c_str());
  iss >> value;
  return (value);
}

/**
** Fonction qui parse l'arborescence de dossier et fait ressort le login
**
** Phrase de base : "login_l-2016"
**
** ressort        : "login_l"
**
** Découpe par rapport au -
**
*/
const std::string			Core::getLogin(const std::string &path)
{
  int					idx_begin;
  int					idx_end;
  std::string				login;

  for (idx_begin = path.size(); idx_begin > 0 && path[idx_begin] != '/'; --idx_begin);
  ++idx_begin;
  for (idx_end = idx_begin; (size_t)idx_end < path.size(); ++idx_end)
    if (path[idx_end] == '-' && (path[idx_end + 1] >= '0' &&
				 path[idx_end + 1] <= '9'))
      break;
    login = path.substr(idx_begin, idx_end - idx_begin);
  return (login);
}

/**
** Crée deux fichiers .cheat (un par users)
**
** Insère à l'intérieur les fonctions concerné par la suspicion de triche
**
*/
void					Core::insertFiles(Cheaters *cheaters, std::vector<Match> &v_match)
{
  std::vector<std::pair<Func, Func> >::iterator itt;
  std::vector<Match>::iterator		itM;
  std::ofstream			out_log1, out_log2;
  Release			*r1, *r2;
  std::string			line;
  int				nb = 0;

  r1 = cheaters->getCouple().first;
  r2 = cheaters->getCouple().second;
  Logger::getSingleton()->log("insert cheaters '" + r1->getLogin() + "' promotion '" + stringToInt(r1->getPromo())
			      + "' from '"
			      + r1->getCity() + "' and '" + r2->getLogin() + "' promotion '"
			      + stringToInt(r2->getPromo()) + "' from '" +r2->getCity() + "' in files in progress ...", NONE);
  line = "Hit Catched between both release : " + stringToInt(cheaters->getNbHit()) + "\n";
  line += "Cheaters : " + r1->getLogin() + " cheat with about "
    + stringToInt(cheaters->getPercent().first) + "% of his release\n";
  line += "Cheaters : " + r2->getLogin() + " cheat with about "
    + stringToInt(cheaters->getPercent().second) + "% of his release\n";

  out_log1.open(r1->getPath() + r2->getLogin() + ".cheat");
  out_log1 << line << "\n";
  out_log2.open(r2->getPath() + r1->getLogin() + ".cheat");
  out_log2 << line << "\n";
  for (itM = v_match.begin(); itM != v_match.end(); ++itM)
    {
      ++nb;
      itt = (*itM).func().begin();
      if (out_log1.is_open() == true)
	{
	  out_log1 << "------------------------------------------------\n";
	  out_log1 << "Func Numero " << nb << "\n";
	  out_log1 << (*itt).first;
	  out_log1 << "------------------------------------------------\n\n";
	}
      if (out_log2.is_open() == true)
	{
	  out_log2 << "------------------------------------------------\n";
	  for (; itt != (*itM).func().end(); ++itt)
	    {
	      out_log2 << "Func Numero " << nb << "\n";
	      out_log2 << (*itt).second;
	    }
	  out_log2 << "------------------------------------------------\n\n";
	}
    }
  out_log1.close();
  out_log2.close();
  Logger::getSingleton()->log("insert cheaters in files ended", NONE);
}

/**
** Insert un cheater en case de donnée
** 1/ Récupère les ids des users présumés cheaters si ceux-ci existent dans la base
**   > si les users ne sont pas présent dans la base
**   1-a/ On crée les users dans la BDD
** 2/ Récupère l'id d'une entrée de la table "couple" (aux cas ou la cheat serait déja dans la base)
**   > si l'entrée n'est pas présente
**   2-a/ On crée une entrée dans la table "couple"
**
**  La fonction ne renvoi rien
*/
void					Core::insertBdd(Cheaters *cheaters, std::vector<Match> &v_match,
							Mysql::Connector *bdd, const std::string &id_project,
							Mysql::Query::Transaction &functions)
{
  static_cast<void>(functions);
  Release *r1 = cheaters->getCouple().first;
  Release *r2 = cheaters->getCouple().second;


  if (Option::getSingleton()->hasOption(Option::HARDVERBOSE)) {
    Logger::getSingleton()->log("insert cheaters '" + r1->getLogin() + "' promotion '" + stringToInt(r1->getPromo())
				+ "' from '" +r1->getCity() + "' and '" + r2->getLogin() + "' promotion '"
				+ stringToInt(r2->getPromo()) + "' from '" +r2->getCity() + "' in bdd in progress ...", NONE);
  }
  Mysql::Query::Select q1("users");
  q1.addField("id");
  q1.addField("login");
  q1.where("login", r1->getLogin());
  q1.where("login", r2->getLogin(), Mysql::Query::HasWhereCondition::OR);
  q1.orderBy("last_update", Mysql::Query::HasOrderBy::DESC);
  Mysql::Result *result = bdd->execute(q1);
  this->_nbRequest++;
  std::map<std::string, int> users;
  {
    std::string login;
    std::vector<Release *> usersToPush;
    usersToPush.push_back(r1);
    usersToPush.push_back(r2);
    for (unsigned int i = 0 ; i < result->rowCount() ; ++i) {
      login = (*result)[i].at("login");
      if (users.find(login) == users.end()) {
	users[login] = Convert<int, std::string>::convert((*result)[i].at("id"));
	for (auto it = usersToPush.begin() ; it != usersToPush.end() ; ++it) {
	  if ((*it)->getLogin() == login) {
	    usersToPush.erase(it);
	    break;
	  }
	}
      }
    }
    for (auto it = usersToPush.begin() ; it != usersToPush.end() ; ++it) {
      Mysql::Query::Select u1("users");
      u1.addField("id");
      u1.where("login", (*it)->getLogin());
      Mysql::Result *r1 = bdd->execute(u1);
      if (r1->rowCount() == 0) {
	Mysql::Query::Insert l("users");
	l.addField("login", (*it)->getLogin());
	l.addField("promo", Convert<std::string, int>::convert((*it)->getPromo()));
	l.addField("city", (*it)->getCityCode());
	Mysql::Result *r = bdd->execute(l);
	users[(*it)->getLogin()] = Convert<int, std::string>::convert(bdd->lastInsertId());
	delete r;
      }
      delete r1;
    }
  }

  int user1_id = users[r1->getLogin()];
  int user2_id = users[r2->getLogin()];
  delete result;
  Mysql::Query::Select q2("couples");
  q2.addField("id");
  q2.where("user1_id", stringToInt(user1_id));
  q2.where("user2_id", stringToInt(user2_id), Mysql::Query::HasWhereCondition::AND);
  q2.where("project_id", id_project, Mysql::Query::HasWhereCondition::AND);
  result = bdd->execute(q2);
  this->_nbRequest++;
  std::string id_couple;
  if (result->rowCount() == 0) {
    Mysql::Query::Insert c1("couples");
    c1.addField("user1_id", stringToInt(user1_id));
    c1.addField("user2_id", stringToInt(user2_id));
    c1.addField("project_id", id_project);
    c1.addField("hit", stringToInt(cheaters->getNbHit()));
    c1.addField("percent_1", stringToInt(cheaters->getPercent().first));
    c1.addField("percent_2", stringToInt(cheaters->getPercent().first));
    Mysql::Result *r = bdd->execute(c1);
    this->_nbRequest++;
    delete r;
    id_couple = bdd->lastInsertId();
  }
  else {
    id_couple = (*result)[0].at("id");
  }

  for (std::vector<Match>::iterator itM = v_match.begin(); itM != v_match.end(); ++itM) {
    std::vector<std::pair<Func, Func> >::iterator it = (*itM).func().begin();
    std::string content_user1 = (*it).first.getFinalContent();
    std::string name_user1 = (*it).first.getName();
    replace(content_user1, "\"", "\\", 0);
    for ( ; it != (*itM).func().end(); ++it)
      {
	replace((*it).second.getFinalContent(), "\"", "\\", 0);
	Mysql::Query::Insert query("functions");
	query.addField("couple_id", id_couple);
	query.addField("name_1", name_user1);
	query.addField("name_2", (*it).second.getName());
	query.addField("src_1", content_user1);
	query.addField("src_2", (*it).second.getFinalContent());
	query.addField("percent_1", stringToInt((*it).first.getPercent()));
	query.addField("percent_2", stringToInt((*it).second.getPercent()));
	Mysql::Result *r = bdd->execute(query);
	this->_nbRequest++;
	delete r;
      }
  }
  delete result;
  this->_nbCheater++;
  if (Option::getSingleton()->hasOption(Option::HARDVERBOSE)) {
    Logger::getSingleton()->log("insert cheaters in bdd ended", NONE);
  }
}

/**
** Récupère l'id d'un projet en base de donnée si il, est existant
** Si il ne l'est pas alors il crée l'entrée du projet en base et renvoi l'id associé.
*/
const std::string                       Core::insertProject(Mysql::Connector *bdd, const std::string &project,
                                                            const std::string &promotion,
                                                            const std::string &module,
                                                            const std::string &semester, const std::string &project_path)
{
  std::string id;
  Mysql::Query::Select query("projects");
  query.addField("id");
  query.where("name", project, Mysql::Query::HasWhereCondition::AND);
  query.where("promo", promotion, Mysql::Query::HasWhereCondition::AND);
  query.where("module", module, Mysql::Query::HasWhereCondition::AND);
  query.where("semester", semester, Mysql::Query::HasWhereCondition::AND);
  query.where("path", project_path, Mysql::Query::HasWhereCondition::AND);
  Mysql::Result *result = bdd->execute(query);
  this->_nbRequest++;
  if (result->rowCount() == 0) {
    Mysql::Query::Insert q("projects");
    q.addField("name", project);
    q.addField("promo", promotion);
    q.addField("module", module);
    q.addField("semester", semester);
    q.addField("path", project_path);
    Mysql::Result *r2 = bdd->execute(q);
    this->_nbRequest++;
    id = bdd->lastInsertId();
    delete r2;
  }
  else {
    id = (*result)[0].at("id");
  }
  delete result;
  return (id);
}

/**
**
** 1/ On se connecte à la base de donnée
** 2/ On récupère l'id du projet global (ex: Bomberman) (INSERT si il n'existe pas en base)
**  -> Pour chaque cheater
**        3/ On rappelle match sur le projet du cheater
**        4/ On insère le cheater dans la base de donnée
**        5/ On crée un fichier .cheat par cheater contenant les fonctions suspectes
** /!\ ATTENTION CETTE FONCTION EST RECURSIVE
**
*/
void					Core::match(const std::list<Cheaters *> &cheaters, const std::string &project,
						    const std::string &promotion, const std::string &project_path,
						    const std::string &module, const std::string &semester)
{
  std::list<Cheaters *>::const_iterator		it;
  std::vector<std::pair<ILoader *,
			ILanguage *> >::iterator	itL;
  std::vector<Match>					v_match;
  bool							is_bdd;
  std::string						id_project, path;
  Mysql::Connector					*bdd = new Mysql::Connector;
  bool							insert = false;
  Mysql::Query::Transaction				functions;
  std::size_t cheatIdx = 1;

  const Config::sqlData &sql = Config::getSingleton()->getSQLData();
  Logger::getSingleton()->log("match cheaters in progress ...", FORCE_NONE);
  if ((is_bdd = bdd->connect(sql.hostname, sql.username, sql.password, sql.dbname)) == true)
    {
      Logger::getSingleton()->log("connection to the database '" + sql.dbname + "' succeeded with success.", FORCE_NONE);
      try {
	id_project = insertProject(bdd, project, promotion, module, semester, project_path);
      }
      catch (Mysql::Exception *e) {
	Logger::getSingleton()->log("impossible to insert project '" + project + "' ...", FORCE_FATAL);
      }
    }
  else
    Logger::getSingleton()->log("impossible to connect bdd '" + sql.dbname + "' ...", FORCE_FATAL);


  try {
    for (it = cheaters.begin(); it != cheaters.end(); ++it)
      {
	Logger::getSingleton()->log("(" + stringToInt(cheatIdx++) + "/" + stringToInt(cheaters.size()) + ") Dealing with cheater couple " + (*it)->getCouple().first->getLogin() + "/" + (*it)->getCouple().second->getLogin(), FORCE_NONE);
	for (itL = _vLanguage.begin(); itL != _vLanguage.end(); ++itL)
	  {
	    if (project_path[project_path.length() - 1] == '/')
	      path = project_path + (*itL).second->getLanguageName() + "/";
	    else
	      path = project_path + "/" + (*itL).second->getLanguageName() + "/";
	    if ((*it)->getLanguage() == (*itL).second->getLanguageName())
	      {
		v_match = (*itL).second->match(*it, path);
		if (v_match.size() > 0)
		  {
		    if (is_bdd == true) {
		      try {
			if (insert == false)
			  Logger::getSingleton()->log("insert cheaters in database in progress ...", NONE);
			insert = true;
			insertBdd(*it, v_match, bdd, id_project, functions);
		      }
		      catch (Mysql::Exception *e) {
			Logger::getSingleton()->log("impossible to insert bdd for project '" + project + "' : " + e->what(), FORCE_FATAL);
			delete e;
		      }
		    }
		    else {
		      insertFiles(*it, v_match);
		    }
		  }
		delete *it;
		break;
	      }
	  }
      }
  }
  catch (Mysql::Exception *e) {
    Logger::getSingleton()->log(std::string("Cannot insert in bdd : ") + e->what(), FORCE_FATAL);
  }
  if (insert == true)
    Logger::getSingleton()->log("insert cheaters ended", FORCE_NONE);
  bdd->disconnect();
  Logger::getSingleton()->log("match cheaters ended", FORCE_NONE);
}

/**
**
** Enleve les doublons entre la liste quantity & average (enleve par average)
** Puis bascule tout les tricheurs dans la liste 'cheaters'
** Efface les listes quantity & average
**
*/
void					Core::selection(std::list<Cheaters *> &quantity, std::list<Cheaters *> &average,
							std::list<Cheaters *> &cheaters)
{
  std::list<Cheaters *>::iterator	itA, itQ;

  for (itQ = quantity.begin(); itQ != quantity.end(); ++itQ)
    {
      for (itA = average.begin(); itA != average.end(); ++itA)
    	{
    	  if ((*itA)->getCouple().first->getID() == (*itQ)->getCouple().first->getID() &&
    	      (*itA)->getCouple().second->getID() == (*itQ)->getCouple().second->getID())
    	    {
    	      average.erase(itA);
    	      break;
    	    }
    	}
    }
  cheaters.resize(average.size() + quantity.size());
  merge(average.begin(), average.end(),
	quantity.begin(), quantity.end(),
	cheaters.begin());
  quantity.clear();
  average.clear();
}

/**
** Cette fonction est le coeur de la moulitriche, elle effectue toutes les actions nécéssaires
**
**   1/ Load des différents modules (CPP, C ... ect)
**   2/ Pour chaque projet défini dans le fichier de conf (voir constructeur de la classe)
**   3/ Pour chaque langage on crée un dossier
**   4/ On crée des Release (Toutes les releases forment la hashtab)
**   5/ Pour chaque release on compile et récupère les tokens des différents langages
**   6/ On épure la hashtab, en gardant seulement un pourcentage des matcheurs (trié par grosseur)
**   7/ On effectue une préselection pour les tricheurs
**   8/ On crée une liste de cheater
**   9/ Ecris dans deux fichiers distinct l'information de "quantity" et d' "average" de chaque tricheur
**  10/ On effectue une sélection des tricheurs
**  11/ On sauvegarde en base tout les projets, ainsi que les users impliqués dans les supposés triches,
**      ainsi que leur supposés crimes
**  12/ Détruit tout les cheaters
**  13/ Détruit tout les projets
*/
void					Core::run()
{
  if (Config::getSingleton()->isCorrect() == false) {
    Logger::getSingleton()->log("Exiting...", FORCE_FATAL);
    return ;
  }
  std::vector<Project *>::const_iterator		it;
  std::vector<Release *>::iterator	itR;
  std::vector<std::pair<ILoader*, ILanguage *> >::iterator	itL;
  std::list<Cheaters *>		average, quantity, cheaters;
  std::list<ILoader *> modulesList;

  loadModules(modulesList);
  for (it = _conf->getProjects().begin(); it != _conf->getProjects().end(); ++it)
    {
      for (itL = _vLanguage.begin(); itL != _vLanguage.end(); ++itL) {
	boost::filesystem::path moduleFolder = (*it)->getValue("path_project") + "/" + (*itL).second->getLanguageName();
	if (!boost::filesystem::exists(moduleFolder)) {
	  /* L'exception est catchée dans le main, c'est une erreur fatale ! */
	  boost::filesystem::create_directory(moduleFolder);
	}
      }
      Logger::getSingleton()->log("Launching moulitriche with project name '" + (*it)->getValue("name")
				  + "' in path '" + (*it)->getValue("path_project")
				  + "' for the module '" + (*it)->getValue("module") + "'.", FORCE_NONE);
      createRelease((*it)->getValue("path_project"));
      for (itR = _vRelease.begin(); itR != _vRelease.end(); ++itR)
	compilation(*itR, (*it)->getValue("path_project"));
      cleanHashTab();
      preSelection();
      createCheaters(quantity, average, Convert<int, std::string>::convert((*it)->getValue("promotion")));
      printInFile((*it)->getValue("path_project") + "/",
		  quantity, average);
      selection(quantity, average, cheaters);
      match(cheaters, (*it)->getValue("name"), (*it)->getValue("promotion"),
	    (*it)->getValue("path_project"), (*it)->getValue("module"), (*it)->getValue("semester"));
      for (; !_vRelease.empty(); delete _vRelease.back(), _vRelease.pop_back());
      cheaters.clear();
      _vRelease.clear();
    }
  unloadModules(modulesList);
}

/**
** Fonction qui ecrit dans les fichiers 'match_par_quantite' et 'match_par_valeur_moyenne_des_rendus'
** les tricheurs potentiel.
*/
void					Core::printInFile(const std::string &path,
							  std::list<Cheaters *> &quantity,
							  std::list<Cheaters *> &average)
{
  std::list<Cheaters *>::iterator	it;
  std::ofstream				outfile;
  std::string				value;
  std::map<std::string, int>		m;

  value = "## correspondance des parametres\n";
  outfile.open(path + "match_par_quantite", std::ios::trunc);
  if (outfile.is_open() == true)
    {
      outfile << value << "## {login, promotion, ville, potentiel, puissance, langage}\n";
      for (it = quantity.begin(); it != quantity.end(); ++it)
	outfile << *(*it) << "\n";
      outfile.close();
    }
  outfile.open(path + "match_par_valeur_moyenne_des_rendus", std::ios::trunc);
  if (outfile.is_open() == true)
    {
      outfile << value << "## {login, promotion, ville, potentiel, moyenne, langage}\n";
      for (it = average.begin(); it != average.end(); ++it)
	outfile << *(*it) << "\n";
      outfile.close();
    }
}

/**
** affiche les informations des rendus avec la liste des tricheurs potentiels entre les rendus
*/
void					Core::printRelease()
{
  std::vector<std::pair<ILoader *,
			ILanguage *> >::iterator		itL;
  std::vector<Release *>::iterator				it;
  std::string							l;
  std::unordered_map<Release *, int>::iterator			it2;
  std::stringstream ss;

  for (itL = _vLanguage.begin(); itL != _vLanguage.end(); ++itL)
    {
      l = (itL->second)->getLanguageName();
      it = _vRelease.begin();
      while (it != _vRelease.end())
	{
	  if ((*it)->getCommonRelease(l).size() > 0)
	    {
	      ss << "STUDENT : " << (*it)->getLogin() << " <<==>> "
			<< (*it)->getPotentiel(l) << std::endl << "MATCH WITH : " << std::endl;
	      it2 = (*it)->getCommonRelease(l).begin();
	      while (it2 != (*it)->getCommonRelease(l).end())
		{
		  ss << "\t\tRelease id : "<< it2->first->getID() << " === "
			    << it2->first->getLogin() << " - ville : "
			    << it2->first->getCity() << " <<==>> " << it2->second << std::endl;
		  ++it2;
		}
	    }
	  ++it;
	}
    }
  Logger::getSingleton()->log(ss.str(), NONE);
}

/**
** Affiche le contenu de la table de hash
*/
void					Core::printHash()
{
  std::vector<std::pair<ILoader*, ILanguage *> >::iterator	it;
  HashTab::iterator			       	itH;
  std::unordered_map<Release *, int>::iterator		itS;
  std::stringstream ss;

  it = _vLanguage.begin();
  while (it != _vLanguage.end())
    {
      itH = _hash[(it->second)->getLanguageName()].begin();
      ss << "####### HashTab [" << (it->second)->getLanguageName() << "] BEGIN #######" << std::endl;
      while (itH != _hash[it->second->getLanguageName()].end())
	{
	  ss << "\tToken : " <<  itH->first << ":" << std::endl;
	  ss << "\tNombre d'occurence  max : " <<  itH->second._nbOccurTot << ":" << std::endl;
	  ss << "\tPoid : " <<  itH->second._weight << ":" << std::endl;
	  itS = itH->second._users.begin();
	  while (itS != itH->second._users.end())
	    {
	      ss << "\t\t\tRelease id : " << (itS->first)->getLogin()
		 << " === " << itS->second << " path : " << (itS->first)->getPath() << std::endl;
	      ++itS;
	    }
	  ++itH;
	}
      ss << "####### HashTab [" << (it->second)->getLanguageName() << "] END #######" << std::endl;
      ++it;
    }
  Logger::getSingleton()->log(ss.str(), NONE);
}

/**
** Comme son nom l'indique, cette fonction sert a compiler les fichiers sources des rendus
** Et comme son nom ne l'indique pas, sert également à créer la Hashtab et à la remplir par rapport
** aux informations (Token) renvoyés par le module moulitiche appelé
**
** Fil d'execution :    Appel du module X.
**                      Le module X compile les fichiers sources si besoin. *
**		        Le module X génére un fichier .concat. *
**                      Le module X parse le fichier .concat et retourne des tokens. **
**                      Creation d'une Hashtab, et remplissage avec les tokens. ***
**
**
** Méthodes associés aux actions :
**
**                       * preprocess()
**                      ** lexer()
**                     *** createHashTab()
*/
void					Core::compilation(Release *release, const std::string &project_path)
{
  std::vector<std::pair<ILoader*, ILanguage *> >::iterator	it;
  VectorToken			vToken;
  std::string			path;

  it = _vLanguage.begin();
  while (it != _vLanguage.end())
    {
      if (project_path[project_path.length() - 1] == '/')
        path = project_path + (*it).second->getLanguageName() + "/";
      else
        path = project_path + "/" + (*it).second->getLanguageName() + "/";
      vToken = (it->second)->getCache(release, path);
      if (vToken.size() == 0)
	{
	  (it->second)->preprocess(release, path);
	  vToken = (it->second)->lexer(release, path);
	  if (vToken.size() > 0)
	    createHashTab(vToken, release, (it->second)->getLanguageName());
	}
      else
	createHashTab(vToken, release, (it->second)->getLanguageName());
      ++it;
    }
}

/**
** pre-selectionne les potentiels tricheurs en vidant la table de hash en passant par un systeme de recursion
** tout en calculant la puissance entre les rendus
**
** Ajoute aux projets une valeur dépendante du nombre d'occurence du token multiplié par son poids (weight)
**
**
**  /!\ ATTENTION : FONCTION RECURSIVE
*/
void					Core::preSelection_bis(const std::string &language, HashTab &hashtab)
{
  HashTab::iterator			it, tmp;
  Release				*release;
  std::unordered_map<Release *, int>::iterator	itCheaters, tmpCheaters;
  bool					isPresent;
  int					occur1, occur2, tot;

  it = hashtab.begin();
  if (hashtab.size() > 0)
    if (it->second._users.size() > 0)
      {
	release = it->second._users.begin()->first;
	while (it != hashtab.end())
	  {
	    isPresent = false;
	    if (it->second._users.find(release) != it->second._users.end())
	      if ((occur1 = it->second._users[release]) > 0)
		isPresent = true;
	    if (isPresent == true)
	      {
		itCheaters = it->second._users.begin();
		while (itCheaters != it->second._users.end())
		  {
		    if (itCheaters->first->getID() == release->getID())
		      {
			tmpCheaters = itCheaters;
			++itCheaters;
			it->second._users.erase(tmpCheaters);
		      }
		    else
		      {
			occur2 = itCheaters->second;
			if (occur1 > occur2)
			  tot = occur2 * it->second._weight;
			else
			  tot = occur1 * it->second._weight;
			release->addRelease(language, itCheaters->first, tot);
			++itCheaters;
		      }
		  }
	      }
	    if (it->second._users.size() == 0)
	      {
		tmp = it;
		++it;
		hashtab.erase(tmp);
	      }
	    else
	      ++it;
	  }
	if (hashtab.begin() != hashtab.end())
	  preSelection_bis(language, hashtab);
      }
}

/**
** pre-selectionne les potentiels tricheurs
**
** -> Pour chaque langage fait appel a preSelection_bis
*/
void					Core::preSelection()
{
  std::vector<std::pair<ILoader *,
			ILanguage *> >::iterator		itL;

  Logger::getSingleton()->log("pre-selection in progress ...", FORCE_NONE);
  for (itL = _vLanguage.begin(); itL != _vLanguage.end(); ++itL)
    preSelection_bis((itL->second)->getLanguageName(), _hash[(itL->second)->getLanguageName()]);
  Logger::getSingleton()->log("pre-selection ended", FORCE_NONE);
}

/**
** 
*/

#define	COMPARE_RELEASE_ID(couple, release)	(couple.first->getID() == release->getID() || couple.second->getID() == release->getID())

bool					Core::getRecurrentLogin(Release *release1, Release *release2,
								float value,
								std::list<Cheaters *> &cheaters,
								bool isAverage) {
  std::list<Cheaters *>::iterator	it;
  int occur[2] = {0, 0};
  Release *releases[2] = {release1, release2};
  Cheaters *release_match[2] = {NULL, NULL};
  std::list<Cheaters *> toDelete;

  for (it = cheaters.begin() ; it != cheaters.end() ; ++it) {
    for (int i = 0 ; i < 2 ; ++i) {
      if (COMPARE_RELEASE_ID((*it)->getCouple(), releases[i])) {
	if (occur[i] == 0) {
	  release_match[i] = *it;
	}
	occur[i]++;
      }
    }
  }
  for (int i = 0 ; i < 2 ; ++i) {
    if (occur[i] >= MAX_OCCUR_LOGIN) {
      if (isAverage == true) {
	if (!(release_match[i]->getPower().first > value ||
	      release_match[i]->getPower().second > value)) {
	  return (false);
	}
      }
      else {
	if (!(release_match[i]->getPower().first < value ||
	      release_match[i]->getPower().second < value)) {
	  return (false);
	}
      }
      toDelete.push_back(release_match[i]);
    }
  }
  toDelete.sort();
  toDelete.unique();
  for (auto it = toDelete.begin() ; it != toDelete.end() ; ++it) {
    delete *it;
    cheaters.remove(*it);
  }
  return (true);
}

/**
 **
 */
void					Core::insertCheatersByQuantity(Release *release1, Release *release2,
								       float power, const std::string &language,
								       std::list<Cheaters *> &cheaters)
{
  std::list<Cheaters *>::iterator	it;

  if (cheaters.size() == 0) {
    cheaters.push_back(new Cheaters(std::make_pair(release1, release2),
				    std::make_pair(power, power), language));
  }
  else {
    if (this->getRecurrentLogin(release1, release2, power, cheaters, false)) {
      for (it = cheaters.begin(); it != cheaters.end() && (*it)->getPower().first >= power; ++it);
      cheaters.insert(it, new Cheaters(std::make_pair(release1, release2),
				       std::make_pair(power, power), language));
    }
  }
  if (cheaters.size() > 100) {
    cheaters.pop_back();
  }
}

/**
 ** Parcourt la liste des cheaters, le but est de supprimer les logins récurrents.
 **
 ** Si la liste de cheater possède le même au moins trois fois on supprime la plus petite valeur
 */
bool					Core::getRecurrentLoginByAverage(Release *release1, Release *release2,
									 float value,
									 std::list<Cheaters *> &cheaters) {
  std::list<Cheaters *>::iterator	it;
  int occur[2] = {0, 0};
  Release *releases[2] = {release1, release2};
  Cheaters *release_match[2] = {NULL, NULL};

  for (it = cheaters.begin() ; it != cheaters.end() ; ++it) {
    for (int i = 0 ; i < 2 ; ++i) {
      if (COMPARE_RELEASE_ID((*it)->getCouple(), releases[i])) {
	if (occur[i] == 0) {
	  release_match[i] = *it;
	}
	occur[i]++;
      }
    }
  }
  std::list<Cheaters *> toDelete;
  for (int i = 0 ; i < 2 ; ++i) {
    if (occur[i] >= MAX_OCCUR_LOGIN) {
      if (value > std::min(release_match[i]->getPower().first, release_match[i]->getPower().first)) {
	toDelete.push_back(release_match[i]);
      }
      else {
	return (false);
      }
    }
  }
  toDelete.sort();
  toDelete.unique();
  for (auto it = toDelete.begin() ; it != toDelete.end() ; ++it) {
    cheaters.remove(*it);
    delete *it;
  }
  return (true);
}

/**
 **
 */
void					Core::insertCheatersByAverage(Release *release1, Release *release2,
								      float power, const std::string &language,
								      std::list<Cheaters *> &cheaters)
{
  std::list<Cheaters *>::iterator	it;
  float					average1, average2, min;

  average1 = power / release1->getPotentiel(language);
  average2 = power / release2->getPotentiel(language);
  min = std::min(average1, average2);
  if (cheaters.size() == 0) {
    cheaters.push_back(new Cheaters(std::make_pair(release1, release2),
				    std::make_pair(average1, average2), language));
  }
  else if (this->getRecurrentLoginByAverage(release1, release2, min, cheaters)) {
    for (it = cheaters.begin(); it != cheaters.end(); ++it) {
      if (min <= std::min((*it)->getPower().first, (*it)->getPower().second))
	break ;
    }
    cheaters.insert(it, new Cheaters(std::make_pair(release1, release2),
				     std::make_pair(average1, average2), language));
  }
  if (cheaters.size() > 100) {
    cheaters.pop_front();
  }
}

/**
** tout est dans le nom de la fonction ..., elle permet des creer les tricheurs potentiels
** en parcourant la liste des rendus communs de chaque rendu
*/
void								Core::createCheaters(std::list<Cheaters *> &quantity,
										     std::list<Cheaters *> &average,
										     int promotion)
{
  std::vector<std::pair<ILoader *,
			ILanguage *> >::iterator		itL;
  std::vector<Release *>::iterator				itR;
  std::string							language;
  std::unordered_map<Release *, int>::iterator			itS;
  std::unordered_map<Release *, int>				commonRelease;

  Logger::getSingleton()->log("create cheaters by quantity and average in progress ...", FORCE_NONE);
  for (itL = _vLanguage.begin(); itL != _vLanguage.end(); ++itL)
    {
      language = (itL->second)->getLanguageName();
      for (itR = _vRelease.begin(); itR != _vRelease.end(); ++itR)
	if ((*itR)->getPotentiel(language) > 0 && (*itR)->getCommonRelease(language).size() > 0)
	  {
	    commonRelease = (*itR)->getCommonRelease(language);
	    for (itS = commonRelease.begin();
		 itS != commonRelease.end(); ++itS)
	      {
		if ((*itR)->getPromo() == promotion || itS->first->getPromo() == promotion)
		  {
		    insertCheatersByQuantity((*itR), itS->first, itS->second, language, quantity);
		    insertCheatersByAverage((*itR), itS->first, itS->second, language, average);
		  }
	      }
	  }
    }
  // this->printCheaters(quantity, average);
  Logger::getSingleton()->log("create cheaters by quantity and average ended", FORCE_NONE);
}

/**
 ** Affiche tous les cheaters trouvés, et le nombre d'occurences de ceux ci
 */
void	Core::printCheaters(const std::list<Cheaters *> &quantity, const std::list<Cheaters *> &average) const {
  std::map<int, int> matchQ, matchA;
  for (auto it : quantity) {
    std::cout << *it << std::endl;
    matchQ[it->getCouple().first->getID()]++;
    matchQ[it->getCouple().second->getID()]++;
  }
  for (auto it : average) {
    std::cout << *it << std::endl;
    matchA[it->getCouple().first->getID()]++;
    matchA[it->getCouple().second->getID()]++;
  }
  for (auto it : matchQ) {
    std::cout << "Quantity: R " << it.first << " : " << it.second << std::endl;
  }
  for (auto it : matchA) {
    std::cout << "Average: R " << it.first << " : " << it.second << std::endl;
  }
}

/**
** Creation de la table de hash et remplissage
**
** En détail :
**
** On ajoute à chaque token de la hastab
**   -> un +1 sur la release de tel utilisateur
**   -> un +1 sur le nombre total d'occurence du token
**   -> on set le potentiel du projet concerné par le token à 1 pour le langage "name"
*/
void					Core::createHashTab(VectorToken &token, Release *release,
							    const std::string &name)
{
  VectorToken::iterator		it;

  for (it = token.begin(); it != token.end(); ++it)
    {
      ((_hash[name])[*it])._users[release] += 1;
      ((_hash[name])[*it])._nbOccurTot += 1;
      release->setPotentiel(name, 1);
    }
}

/**
** Utilité de la fonction : Faire un tri parmis les tricheurs
**
**
** Problématique : Une grande partie des tricheurs rescensé le sont sur des fonctions de bases (cf : my_putchar)
** Solution      : On garde 40% des matcheurs (Trié par Nombre de match en ASC) --> Les plus gros nombres de match sont virés
**
** 1/ On calcule un nombre égal au nombre de release * un % (Core.hh)
** 2/ On enleve les releases dont le nombre de matcheur est supérieur au nombre calculé si dessus
**
**
** Exemple :
**
**    13 match my_putstr
**    12 match my_strlen 
**    12 match my_putchar
**     9 match my_strcmp
**     8 match my_strdup
**     7 match my_str_capitalize
**     6 match my_str_minimilize
**     6 match my_XXXX
**     2 match my_jaimelesbananes
**     2 match my_tomate
**
**
**   -> Aprés epuration
**
**     6 match my_str_minimilize
**     6 match my_XXXX
**     2 match my_jaimelesbananes
**     2 match my_tomate
**
** Ceci n'est pas la meilleure solution, mais c'est la solution actuelle
**
**
*/
void					Core::cleanHashTab()
{
  HashTab::iterator			itHash;
  HashTab::iterator			tmp;
  std::unordered_map<std::string,
		     std::unordered_map<std::string, InfosNgram > >::iterator	itL;
  int					max = 0;
  int					percent = 0;

  Logger::getSingleton()->log("cleaning HashTab in progress ...", FORCE_NONE);
  percent = FOURTY_PERCENT(_vRelease.size());
  itL = _hash.begin();
  while (itL != _hash.end())
    {
      for (itHash = (itL->second).begin(); itHash != (itL->second).end(); ++itHash)
	if (max < (itHash->second)._nbOccurTot)
	  max = (itHash->second)._nbOccurTot;
      itHash = (itL->second).begin();
      while (itHash != (itL->second).end())
	{
	  (itHash->second)._weight = max - (itHash->second)._nbOccurTot;
	  if (static_cast<int>((itHash->second)._users.size()) > percent ||
	      (itHash->second)._weight == 0
	      || (itHash->second)._users.size() == 1)
	    {
	      tmp = itHash;
	      ++itHash;
	      _hash[(itL->first)].erase(tmp);
	    }
	  else
	    ++itHash;
	}
      ++itL;
      max = 0;
    }
  Logger::getSingleton()->log("cleaning HashTab ended", FORCE_NONE);
}

/**
** Fonction qui check l'extension d'un fichier
**
** Argument1 : Nom du fichier
** Argument2 : Nom de l'extension
**
**
** Retourne un booléen
**
*/
bool					Core::checkExtension(const std::string &file,
							     const std::string &ext)
{
  if (file.find(ext) == file.size() - ext.size())
    return (true);
  return (false);
}

/**
** Chargement des modules
**
** 1/ Ouvre le dossier courant et va dans le répertoire défini par la macro MOD_DIR (Core.hh) 
** 2/ Charge chaque module dont le dossier comporte l'extension défini par la macro EXT (Core.hh)
**
*/
void					Core::loadModules(std::list<ILoader *> &modulesList)
{
  std::vector<path>::iterator		it;
  std::vector<path>			vPath;
  ILoader				*loader;
  ILanguage				*module;
  std::stringstream			ss;
  int					nbModuleInitialized = 0;

  Logger::getSingleton()->log("Initialize : modules", FORCE_NONE);
  copy(directory_iterator(path(CURRENT_DIR + "/" + MOD_DIR)),
       directory_iterator(), back_inserter(vPath));
  it = vPath.begin();
  while (it != vPath.end())
    {
      if (checkExtension(it->string(), EXT) == true)
	{
	  loader = Instance::getLoaderInstance();
	  modulesList.push_front(loader);
	  if (loader->open("/", it->string()) == true)
	    {
	      if ((module = loader->load()) != NULL)
		{
		  ss << "module " << it->string() << " successfully initialized";
		  Logger::getSingleton()->log(ss.str(), FORCE_NONE);
		  ss.str("");
		  _vLanguage.push_back(std::make_pair(loader, module));
		  nbModuleInitialized++;
		}
	      else
		Logger::getSingleton()->log("impossible to load library '" + it->string() + "'", FORCE_WARNING);
	    }
	  else
	    Logger::getSingleton()->log("impossible to open library '" + it->string() + "'", FORCE_WARNING);
	}
      ++it;
    }
  if (nbModuleInitialized == 0)
    Logger::getSingleton()->log("No module is initialized (maybe compile `modules/` directory", FORCE_WARNING);
  Logger::getSingleton()->log("modules initialized", FORCE_NONE);
}

/*
  This function as been added by chaber_a (etienne1.chabert@epitech.eu)
  The modules wasn't unloaded and caused a leak resulting in a kernel panic
 */
void					Core::unloadModules(std::list<ILoader *> &modulesList)
{
  while (modulesList.empty() == false)
    {
      ILoader *module = modulesList.front();
      modulesList.pop_front();
      module->unload();
    }
}

/**
** Creation des FileTree
**
** Fonction utilisée par createRelease pour ouvrir les dossiers et les parcourirs
**
** Remarque : FONCTION RECURSIVE
*/
void					Core::createFileTree(Release *release, const path &p)
{
  std::vector<path>			tree;
  std::vector<path>::iterator		it;
  FileTree				&realTree = release->getTree();

  copy(directory_iterator(p),
       directory_iterator(), back_inserter(tree));
  it = tree.begin();
  while (it != tree.end())
    {
      if (is_directory(*it)) {
	if (boost::filesystem::is_symlink(*it)) {
	  Logger::getSingleton()->log(std::string("Removed symlink '") + it->c_str() + "' -> '" + boost::filesystem::read_symlink(*it).c_str() + "'", FORCE_WARNING);
	  boost::filesystem::remove(*it);
	}
	else {
	  createFileTree(release, (*it));
	}
      }
      else {
	_languageDetector.flag(realTree, (*it).string());
      }
      ++it;
    }
}

/**
** Recuperation des logins et des rendus
**
**  Pour chaque ville on crée une nouvelle release qui contiendras      *
**        -> Tout les fichiers du projet
**        -> Le login du propriétaire
**        -> La ville du propriétaire
**        -> L'année de promotion du propriétaire
**
**    * On cherche dans le répertoire donné en paramètre dans le fichier de conf des dossiers dont les noms
**      sont ceux des villes (Déclarés en dur dans le constructeur de la classe Core
**
**
**  ===================================== ARBORESCENCE ===================================
**
**  _DossierSpecifieDansLeFichierDeConf/
**                                     |
**                                      \_lil
**                                          \_login_l-20XX                          *
**      	   			                 \_NomDunDossierQuelconque  **
**               			                 \_SourcesCouCPPouAutre     ***
**                                     |
**                                     ... ect pour chaque non de ville
**
**      *
**      **   -> ... Ouvre en récursif tout les dossiers trouvés dans le dossier et sous dossiers des répertoires de ville
**	***  -> ... Add tout les fichiers dont le langage est reconnu traitable par la moulitriche
**
** =======================================================================================
**
*/
void					Core::createRelease(const std::string &p)
{
  std::map<std::string, std::string >::iterator	it;
  int					id = 0;
  std::vector<path>::iterator		itPath;
  std::vector<path>			vPath;
  path					dir;
  Release				*release;
  boost::system::error_code ign;

  Logger::getSingleton()->log("create release in progress ...", NONE);
  it = _mCities.begin();
  while (it != _mCities.end())
    {
      if (p[p.length() - 1] == '/')
        dir = p + (*it).first;
      else
        dir = p + "/" +  (*it).first;
      if (exists(dir))
	{
	  copy(directory_iterator(dir),
	       directory_iterator(), back_inserter(vPath));
	  itPath = vPath.begin();
	  while (itPath != vPath.end())
	    {
	      if (is_directory(*itPath))
		{
		  release = new Release(id, getLogin((*itPath).string()),
					getPromo((*itPath).string()),
					(*it).second, (*it).first, (*itPath).string());
		  if (Option::getSingleton()->hasOption(Option::HARDVERBOSE)) {
		    Logger::getSingleton()->log("create FileTree for student '" + release->getLogin() + "' promotion '"
						+ stringToInt(release->getPromo()) + "' and city '"
						+ release->getCity() + "' in progress ...", NONE);
		  }
		  try {
		    createFileTree(release, *itPath);
		  } catch (const std::exception& e) {
		    Logger::getSingleton()->log("Catched exception: " + std::string(e.what()), NONE);
		  }
		  if (Option::getSingleton()->hasOption(Option::HARDVERBOSE)) {
		    Logger::getSingleton()->log("create FileTree for student '" + release->getLogin() + "' promotion '"
						+ stringToInt(release->getPromo()) + "' and city '"
						+ release->getCity() + "' ended", NONE);
		  }
		  _vRelease.push_back(release);
		}
	      else if (RE2::PartialMatch((*itPath).string(), ".*\\.(concat|gram)$") == true) {
		boost::filesystem::remove((*itPath).string(), ign);
	      }
	      ++id;
	      ++itPath;
	    }
	  vPath.clear();
	}
      ++it;
    }
  Logger::getSingleton()->log("create release ended.", NONE);
}

/**
 ** Les deux fonctions suivantes servent à manipuler l'attribut nbCheater qui represente
 ** le nombre de cheater entré en base de donnée
 */
void			Core::resetNbCheater()
{
  this->_nbCheater = 0;
}

int			Core::getNbCheater()
{
  return (this->_nbCheater);
}


void			Core::resetNbRequest()
{
  this->_nbRequest = 0;
}

int			Core::getNbRequest()
{
  return (this->_nbRequest);
}
