//
// HasFields.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 15:13:11 2014 geoffrey bauduin
// Last update Wed May  7 15:43:04 2014 geoffrey bauduin
//

#include	"mysql/Mysql.hpp"

Mysql::Query::HasFields::HasFields(void):
  _fields() {

}

Mysql::Query::HasFields::~HasFields(void) {

}

void	Mysql::Query::HasFields::addField(const std::string &field, const std::string &value) {
  this->_fields.insert(std::make_pair(field, value));
}

void	Mysql::Query::HasFields::removeField(const std::string &field) {
  Mysql::Fields::iterator it = this->_fields.find(field);
  if (it != this->_fields.end()) {
    this->_fields.erase(it);
  }
}

const Mysql::Fields &Mysql::Query::HasFields::getFields(void) const {
  return (this->_fields);
}
