//
// HasTable.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 15:16:08 2014 geoffrey bauduin
// Last update Wed May  7 15:43:09 2014 geoffrey bauduin
//

#include	"mysql/Mysql.hpp"

Mysql::Query::HasTable::HasTable(const std::string &table):
  _table(table) {

}

Mysql::Query::HasTable::~HasTable(void) {

}

void	Mysql::Query::HasTable::setTable(const std::string &table) {
  this->_table = table;
}

const std::string	&Mysql::Query::HasTable::getTable(void) const {
  return (this->_table);
}

