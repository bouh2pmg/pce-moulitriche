//
// HasOrderBy.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Mon May 19 16:18:39 2014 geoffrey bauduin
// Last update Mon May 19 16:30:04 2014 geoffrey bauduin
//

#include	"mysql/Mysql.hpp"

Mysql::Query::HasOrderBy::HasOrderBy(void):
  _orderBy() {

}

Mysql::Query::HasOrderBy::~HasOrderBy(void) {
}

void	Mysql::Query::HasOrderBy::orderBy(const std::string &field, Mysql::Query::HasOrderBy::Sort order) {
  this->_orderBy.push_back(std::make_pair(field, order));
}

const Mysql::Query::HasOrderBy::Container &Mysql::Query::HasOrderBy::getOrderBy(void) const {
  return (this->_orderBy);
}
