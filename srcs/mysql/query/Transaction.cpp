//
// Transaction.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Mon May 12 14:17:11 2014 geoffrey bauduin
// Last update Mon May 12 16:45:06 2014 geoffrey bauduin
//

#include	<algorithm>
#include	<sstream>
#include	<iostream>
#include	"mysql/Mysql.hpp"

Mysql::Query::Transaction::Transaction(void):
  Mysql::Query::AQuery(Mysql::Query::TRANSACTION), _queries() {

}

static void	deleteQuery(Mysql::Query::AQuery *query) {
  delete query;
}

Mysql::Query::Transaction::~Transaction(void) {
  for_each(this->_queries.begin(), this->_queries.end(), &deleteQuery);
}

void	Mysql::Query::Transaction::addQuery(Mysql::Query::AQuery *query) {
  this->_queries.push_back(query);
}

void	Mysql::Query::Transaction::compute(void) const {
  std::stringstream stream;
  stream << "START TRANSACTION;" << std::endl;
  for (auto &it : this->_queries) {
    it->compute();
    stream << it->toString();
    if (*(it->toString().rbegin()) != ';') {
      stream << ";";
    }
    stream << std::endl;
  }
  stream << "COMMIT;" << std::endl;
  this->_string = stream.str();
}
