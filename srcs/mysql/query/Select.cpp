//
// Select.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 15:20:30 2014 geoffrey bauduin
// Last update Mon May 19 16:34:50 2014 geoffrey bauduin
//

#include	<sstream>
#include	"mysql/Mysql.hpp"

Mysql::Query::Select::Select(const std::string &table):
  Mysql::Query::AQuery(Mysql::Query::SELECT), Mysql::Query::HasWhereCondition(),
  Mysql::Query::HasFields(), Mysql::Query::HasTable(table) {

}

Mysql::Query::Select::~Select(void) {

}

void	Mysql::Query::Select::compute(void) const {
  std::stringstream	stream;
  stream << "SELECT ";
  const Mysql::Fields &fields = this->getFields();
  for (auto it = fields.begin() ; it != fields.end() ; ++it) {
    if (it != fields.begin()) {
      stream << ", ";
    }
    stream << "`" << it->first << "`";
    if (it->second != "") {
      stream << " AS `" << it->second << "`";
    }
  }
  stream << " FROM " << this->getTable() << " WHERE ";
  const Mysql::Query::HasWhereCondition::Where &where = this->getWhere();
  for (auto it = where.begin() ; it != where.end() ; ++it) {
    if (it != where.begin()) {
      switch (it->link) {
      case Mysql::Query::HasWhereCondition::AND:
	stream << " AND ";
	break;
      case Mysql::Query::HasWhereCondition::OR:
	stream << " OR ";
	break;
      default:
	break;
      }
    }
    stream << "`" << it->field << "` = \"" << it->value << "\"";
  }
  const Mysql::Query::HasOrderBy::Container &orderBy = this->getOrderBy();
  if (orderBy.empty() == false) {
    stream << " ORDER BY ";
    for (auto it = orderBy.begin() ; it != orderBy.end() ; ++it) {
      if (it != orderBy.begin()) {
	stream << ", ";
      }
      stream << it->first << " ";
      if (it->second == Mysql::Query::HasOrderBy::ASC) {
	stream << "ASC";
      }
      else {
	stream << "DESC";
      }
    }
  }
  this->_string = stream.str();
}

