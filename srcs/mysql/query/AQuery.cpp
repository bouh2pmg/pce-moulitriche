//
// Query.cpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec  6 17:38:15 2013 geoffrey bauduin
// Last update Wed May  7 15:34:45 2014 geoffrey bauduin
//

#include	"mysql/Mysql.hpp"

Mysql::Query::AQuery::AQuery(Mysql::Query::Type type):
  _type(type), _string("") {

}

Mysql::Query::AQuery::AQuery(const std::string &query):
  _type(Mysql::Query::NONE), _string(query) {

}

Mysql::Query::AQuery::~AQuery(void) {

}

const std::string	&Mysql::Query::AQuery::toString(void) const {
  return (this->_string);
}
