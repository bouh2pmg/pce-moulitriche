//
// HasWhereCondition.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 15:14:34 2014 geoffrey bauduin
// Last update Wed May  7 15:46:01 2014 geoffrey bauduin
//

#include	"mysql/Mysql.hpp"

Mysql::Query::HasWhereCondition::HasWhereCondition(void):
  _where() {

}

Mysql::Query::HasWhereCondition::~HasWhereCondition(void) {

}

void	Mysql::Query::HasWhereCondition::where(const std::string &field,
					       const std::string &value,
					       Mysql::Query::HasWhereCondition::Link link) {
  struct where w;
  w.link = link;
  w.field = field;
  w.value = value;
  this->_where.push_back(w);
}

const Mysql::Query::HasWhereCondition::Where	&Mysql::Query::HasWhereCondition::getWhere(void) const {
  return (this->_where);
}
