//
// Insert.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 15:16:58 2014 geoffrey bauduin
// Last update Mon May 12 15:28:54 2014 geoffrey bauduin
//

#include	"mysql/Mysql.hpp"

Mysql::Query::Insert::Insert(const std::string &table):
  Mysql::Query::AQuery(Mysql::Query::SELECT), Mysql::Query::HasTable(table),
  Mysql::Query::HasFields(),
  _delayed(false) {

}

Mysql::Query::Insert::~Insert(void) {

}

void	Mysql::Query::Insert::setDelayed(bool status) {
  this->_delayed = status;
}

void	Mysql::Query::Insert::compute(void) const {
  this->_string = "INSERT";
  if (this->_delayed) {
    this->_string += " DELAYED";
  }
  this->_string += " INTO `" + this->getTable() + "` (";
  std::string values = "";
  const Mysql::Fields &fields = this->getFields();
  for (auto it = fields.begin() ; it != fields.end() ; ++it) {
    if (it != fields.begin()) {
      this->_string += ", ";
      values += ", ";
    }
    this->_string += "`" + it->first + "`";
    values += "\"" + it->second + "\"";
  }
  this->_string += ") VALUES (" + values + ")";
}
