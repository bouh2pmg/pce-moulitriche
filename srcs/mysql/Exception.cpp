//
// Exception.cpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec 13 10:42:47 2013 geoffrey bauduin
// Last update Fri Dec 13 14:34:13 2013 geoffrey bauduin
//

#include	"mysql/Exception.hpp"

Mysql::Exception::Exception(const std::string &what) throw():
  _what(what) {

}

const char	*Mysql::Exception::what(void) const throw() {
  return (this->_what.c_str());
}
