//
// Database.cpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec  6 17:26:30 2013 geoffrey bauduin
// Last update Mon May 12 16:35:49 2014 geoffrey bauduin
//

#include	<iostream>
#include	<cstddef>
#include	"mysql/Mysql.hpp"
#include	"Logger.hh"
#include	"Convert.hh"
#include	"Option.hpp"

Mysql::Connector::Connector(void):
  _bdd(NULL), _logfile() {
  this->_logfile.open(std::string("/tmp/") + Convert<std::string, time_t>::convert(time(NULL)) + ".log", std::ofstream::out | std::ofstream::trunc);
}

Mysql::Connector::~Connector(void) {
  if (this->_bdd) {
    this->disconnect();
  }
  this->_logfile.close();
}

void	Mysql::Connector::log(const std::string &request) {
  if (this->_logfile.fail() == false) {
    this->_logfile << request << ";" << std::endl;
  }
}

bool	Mysql::Connector::connect(const std::string &hostname,
				 const std::string &user,
				 const std::string &password,
				 const std::string &database) {
  this->_bdd = new MYSQL;
  mysql_init(this->_bdd);
  mysql_options(this->_bdd, MYSQL_READ_DEFAULT_GROUP, "option");
  return (mysql_real_connect(this->_bdd, hostname.c_str(), user.c_str(),
			     password.c_str(), database.c_str(), 0, NULL, CLIENT_MULTI_STATEMENTS));
}

void	Mysql::Connector::disconnect(void) {
  mysql_close(this->_bdd);
  delete this->_bdd; // *
}

Mysql::Result	*Mysql::Connector::execute(const Mysql::Query::AQuery &query) {
  query.compute();
  this->log(query.toString());
  if (mysql_real_query(this->_bdd, query.toString().c_str(), query.toString().size()) != 0) {
    throw new Mysql::Exception(std::string("MySQL Error: ") + this->error());
  }
  Mysql::Result *result = new Mysql::Result(this->_bdd);
  if (Option::getSingleton()->hasOption(Option::HARDVERBOSE)) {
    Logger::getSingleton()->log(query.toString() + " -- " + Convert<std::string, unsigned int>::convert(result->rowCount()) + " rows", WARNING);
  }
  return (result);
}

const std::string	Mysql::Connector::lastInsertId(void) {
  return (std::to_string(mysql_insert_id(this->_bdd)));
}

const char	*Mysql::Connector::error(void) const {
  return (mysql_error(this->_bdd));
}
