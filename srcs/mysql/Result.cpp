//
// Result.cpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec  6 17:43:13 2013 geoffrey bauduin
// Last update Fri Dec 13 14:34:22 2013 geoffrey bauduin
//

#include	<vector>
#include	"mysql/Mysql.hpp"

Mysql::Result::Result(MYSQL *bdd):
  _rows(0), _valid(false), _fields() {
  MYSQL_RES *res = mysql_store_result(bdd);
  if (res != NULL) {
    std::vector<std::string> fields;
    MYSQL_FIELD *field;
    while ((field = mysql_fetch_field(res))) {
      fields.push_back(field->name);
    }
    this->_rows = mysql_num_rows(res);
    MYSQL_ROW row;
    for (unsigned int i = 0 ; (row = mysql_fetch_row(res)) ; ++i) {
      for (unsigned int j = 0 ; j < fields.size() ; ++j) {
	this->_fields[i][fields[j]] = row[j];
      }
    }
  }
}

Mysql::Result::~Result(void) {

}

unsigned int	Mysql::Result::rowCount(void) const {
  return (this->_rows);
}

bool	Mysql::Result::valid(void) const {
  return (this->_valid);
}

const Mysql::Fields	&Mysql::Result::operator[](unsigned int row) const {
  if (this->_fields.find(row) == this->_fields.end()) {
    throw new Mysql::Exception("No such index: " + row);
  }
  return (this->_fields.at(row));
}
