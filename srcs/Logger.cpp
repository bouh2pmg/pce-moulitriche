//
// Logger.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:55:55 2012 romuald scharre
// Last update Wed Apr  9 10:39:45 2014 geoffrey bauduin
//

#include			"../headers/Logger.hh"
#include			"../headers/Instance.hh"
#include			"../headers/Date.hh"
#include			"../headers/Option.hpp"

Logger::Logger() : _level(NONE), _mutex(Instance::getMutexInstance()), _stringLevel()
{
  this->_stringLevel[ERROR] = "\033[0;31mError : ";
  this->_stringLevel[FORCE_ERROR] = this->_stringLevel[ERROR];
  this->_stringLevel[WARNING] = "\033[0;33mWarning : ";
  this->_stringLevel[FORCE_WARNING] = this->_stringLevel[WARNING];
  this->_stringLevel[FATAL] = "\033[0;31mFatal : ";
  this->_stringLevel[FORCE_FATAL] = this->_stringLevel[FATAL];
  this->_stringLevel[NONE] = "\033[0m";
  this->_stringLevel[FORCE_NONE] = this->_stringLevel[NONE];
  if (Option::getSingleton()->hasOption(Option::VERBOSE)) {
    std::cout << Date::getTime() << "Logger created and initialized" << std::endl;
  }
}

Logger::~Logger()
{
  delete _mutex;
  _mutex = NULL;
  if (Option::getSingleton()->hasOption(Option::VERBOSE)) {
    std::cout << Date::getTime() << "Logger destroyed" << std::endl;
  }
}

void				Logger::setLevel(LOG_LEVEL level)
{
  _mutex->Lock();
  _level = level;
  _mutex->Unlock();
}

void				Logger::log(const std::string &log, LOG_LEVEL level)
{
  if ((Option::getSingleton()->hasOption(Option::VERBOSE)) || (level >= 3))
    {
      _mutex->Lock();
      std::cout << Date::getTime() << this->_stringLevel[level] << log << this->_stringLevel[NONE] << std::endl;
      _mutex->Unlock();
    }
}
