//
// LoaderUnix.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:55:48 2012 romuald scharre
// Last update Tue Apr 21 16:05:39 2015 raphael defreitas
//

#include		"../headers/LoaderUnix.hh"
#include		"../headers/Logger.hh"

LoaderUnix::LoaderUnix()
{
}

LoaderUnix::~LoaderUnix()
{
}
#include		<iostream>

bool			LoaderUnix::open(const std::string &path, const std::string &name)
{
  _object = 0;
  _handle = dlopen(std::string(path + "/"  + name).c_str(), RTLD_LAZY);
  if (!_handle)
    {
      Logger::getSingleton()->setLevel(ERROR);
      Logger::getSingleton()->log(dlerror());
      return (false);
    }
  return (true);
}

ILanguage		*LoaderUnix::load()
{
  ILanguage		*(*entry_point)();

  entry_point = reinterpret_cast<ILanguage*(*)()>(dlsym(_handle, "create_module"));
  if (!entry_point)
    {
      Logger::getSingleton()->setLevel(ERROR);
      Logger::getSingleton()->log(dlerror());
      return NULL;
    }
  _object = entry_point();
  return (_object);
}

void			LoaderUnix::unload()
{
  if (_object)
    delete _object;
  if (dlclose(_handle))
    {
      Logger::getSingleton()->setLevel(ERROR);
      Logger::getSingleton()->log(dlerror());
    }
}
