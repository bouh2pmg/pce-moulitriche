//
// Project.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:56:08 2012 romuald scharre
// Last update Thu Apr  3 10:32:51 2014 geoffrey bauduin
//

#include	<algorithm>
#include	"Project.hh"
#include	"Logger.hh"
using namespace tinyxml2;

Project::Project()
{
  _value.clear();
}

const std::string		&Project::getValue(const std::string &key) const
{
  return (_value.at(key));
}

void				Project::setValue(const std::string &key,
						  const std::string &value)
{
  _value[key] = value;
}

std::ostream			&operator<<(std::ostream &os, Project &proj)
{
  os << "Loading project '" << proj.getValue("name")
     << "' in path " << proj.getValue("path_project")
     << "' for the module '" << proj.getValue("module")
     << "'.";
  return (os);
}

Project::Parser::Parser(const std::string &filename):
  _tokens(), _projects() {
  this->_tokens[PROJECTS] = "projects";
  this->_tokens[PROJECT] = "project";
  this->_tokens[PRJ_NAME] = "name";
  this->_tokens[PRJ_PATH_PROJECT] = "path_project";
  this->_tokens[PRJ_SEMESTER] = "semester";
  this->_tokens[PRJ_MODULE] = "module";
  this->_tokens[PRJ_PROMOTION] = "promotion";
  this->parseFile(filename);
}

static void	deleteProject(Project *p) {
  delete p;
}

Project::Parser::~Parser(void) {
  for_each(this->_projects.begin(), this->_projects.end(), &deleteProject);
}

void	Project::Parser::parseFile(const std::string &filename) {
  Logger::getSingleton()->log("Using " + filename, FORCE_WARNING);

  XMLDocument document;
  if (document.LoadFile(filename.c_str()) != 0) {
    Logger::getSingleton()->log("Cannot open " + filename, FORCE_FATAL);
    return ;
  }
  const XMLElement *projects = document.FirstChildElement(this->_tokens[PROJECTS].c_str());
  if (!projects) {
    Logger::getSingleton()->log("Cannot find tag '" + this->_tokens[PROJECTS] + "'", FORCE_FATAL);
    return ;
  }
  std::vector<Token> v;
  v.push_back(PRJ_NAME);
  v.push_back(PRJ_PATH_PROJECT);
  v.push_back(PRJ_SEMESTER);
  v.push_back(PRJ_MODULE);
  v.push_back(PRJ_PROMOTION);
  for (const XMLElement *element = projects->FirstChildElement(this->_tokens[PROJECT].c_str()) ;
       element ;
       element = element->NextSiblingElement()) {
    this->parseProject(element, v);
  }
}

void	Project::Parser::parseProject(const XMLElement *element, const std::vector<Token> &v) {
  Project *project = new Project;
  for (auto it : v) {
    const XMLElement *data = element->FirstChildElement(this->_tokens[it].c_str());
    if (data == NULL) {
      Logger::getSingleton()->log("Cannot find tag '" + this->_tokens[it] + "' in tag '" +
				  this->_tokens[PROJECT] + "'", FORCE_FATAL);
      delete project;
      return ;
    }
    project->setValue(this->_tokens[it], data->GetText());
  }
  this->_projects.push_back(project);
}

const std::vector<Project *>	&Project::Parser::getProjects(void) const {
  return (this->_projects);
}
