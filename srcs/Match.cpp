//
// Match.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Mon Jun 18 15:15:31 2012 romuald scharre
// Last update Fri Apr 25 11:44:56 2014 geoffrey bauduin
//

#include			"Match.hh"

Match::Match():
  _nbHit(0)
{

}

int						Match::getNbHit() const
{
  return (_nbHit);
}

const std::vector<std::pair<Func, Func> >	&Match::getFunc() const
{
  return (_func);
}

std::vector<std::pair<Func, Func> >		&Match::func()
{
  return (_func);
}

void						Match::setNbHit(int nbHit)
{
  _nbHit = nbHit;
}

void						Match::addFunc(const std::pair<Func, Func> &func)
{
  _func.push_back(func);
  this->_nbHit += func.first.getNbHit();
}

std::ostream	&operator<<(std::ostream &os, const Match &match) {
  os << "nbHit : " << match.getNbHit() << std::endl;
  for (auto &it : match.getFunc()) {
    os << "R1: {" << std::endl << it.first << std::endl << "}" << std::endl;
    os << "R2: {" << std::endl << it.second << std::endl << "}" << std::endl;
  }
  return (os);
}
