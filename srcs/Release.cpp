//
// Release.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec 20 10:54:23 2013 geoffrey bauduin
// Last update Tue Dec 16 15:10:36 2014 Philip Garnero
//

#include			"Release.hh"

using namespace			boost::filesystem;

Release::Release(int id, const std::string &login,
		 int promo, const std::string &city,
		 const std::string &code,
		 const std::string &path) : _path(path + "/"), _id(id), _login(login),
					    _promo(promo), _city(city), _cityCode(code), _potentiel(0)
{

}

Release::~Release()
{

}

const std::string		&Release::getPath() const
{
  return (_path);
}

FileTree			&Release::getTree()
{
  return (_tree);
}

int				Release::getID() const
{
  return (_id);
}

const std::string		&Release::getLogin() const
{
  return (_login);
}

std::unordered_map<Release *, int>	&Release::getCommonRelease(const std::string &language)
{
  return (_commonRelease[language]);
}

int				Release::getPotentiel(const std::string &language)
{
  return (_potentiel[language]);
}

int				Release::getPromo() const
{
  return (_promo);
}

const std::string		&Release::getCity() const
{
  return (_city);
}

const std::string		&Release::getCityCode() const
{
  return (_cityCode);
}

void				Release::setPotentiel(const std::string &language, int potentiel)
{
  _potentiel[language] += potentiel;
}

void				Release::addRelease(const std::string &language, Release *release, int value)
{
  (_commonRelease[language])[release] += value;
}

std::ostream			&operator<<(std::ostream &os, const Release &release)
{
  os << "Login : " << release.getLogin()
     << "\nPromo : " << release.getPromo()
     << "\nCity : " << release.getCity()
     << "\nPath :" << release.getPath();
  return (os);
}
