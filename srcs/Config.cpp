//
// Config.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed Apr  2 11:12:14 2014 geoffrey bauduin
// Last update Wed Apr  2 14:39:43 2014 geoffrey bauduin
//

#include	"Config.hh"
#include	"tinyxml2.h"
#include	"Logger.hh"

#define	FILENAME	"./config.xml"

using namespace tinyxml2;

Config::Config(void):
  _tokens(), _modulesFlags(), _sql(), _correct(true) {
  this->_tokens[CONFIG] = "config";
  this->_tokens[CAT_MYSQL] = "mysql";
  this->_tokens[SQL_HOSTNAME] = "hostname";
  this->_tokens[SQL_USER] = "user";
  this->_tokens[SQL_PASSWORD] = "password";
  this->_tokens[SQL_DBNAME] = "dbname";
  this->_tokens[CAT_MODULES] = "modules";
  this->_tokens[MOD_NAME] = "name";
  this->_tokens[MOD_FLAG] = "flag";
  this->parseFile(FILENAME);
}

Config::~Config(void) {

}

void	Config::parseFile(const std::string &filename) {
  Logger::getSingleton()->log("READING " + filename, FORCE_WARNING);
  std::map<Token, void (Config::*)(const XMLElement *)> table;
  table[CAT_MYSQL] = &Config::parseSQL;
  table[CAT_MODULES] = &Config::parseModules;

  XMLDocument document;
  if (document.LoadFile(filename.c_str()) != 0) {
    Logger::getSingleton()->log(std::string("Cannot open config file '") + filename + "'", FORCE_FATAL);
    this->_correct = false;
    return ;
  }
  const XMLElement *config = document.FirstChildElement(this->_tokens[CONFIG].c_str());
  if (!config) {
    this->_correct = false;
    Logger::getSingleton()->log(std::string("Cannot find tag '") + this->_tokens[CONFIG] + "'", FORCE_FATAL);
    return ;
  }
  for (auto it : table) {
    const XMLElement *element = config->FirstChildElement(this->_tokens[it.first].c_str());
    if (!element) {
    this->_correct = false;
      Logger::getSingleton()->log(std::string("Cannot find tag '") + this->_tokens[it.first] +
				  "' inside tag '" + this->_tokens[CONFIG] + "'", FORCE_FATAL);
      return ;
    }
    (this->*it.second)(element);
  }
}

void	Config::parseSQL(const XMLElement *mysql) {
  std::map<Token, std::string *> table;
  table[SQL_HOSTNAME] = &this->_sql.hostname;
  table[SQL_USER] = &this->_sql.username;
  table[SQL_PASSWORD] = &this->_sql.password;
  table[SQL_DBNAME] = &this->_sql.dbname;
  for (auto &it : table) {
    const XMLElement *element = mysql->FirstChildElement(this->_tokens[it.first].c_str());
    if (element == NULL) {
      this->_correct = false;
      Logger::getSingleton()->log(std::string("Cannot find tag '") + this->_tokens[it.first] + "' inside tag '" +
				  this->_tokens[CAT_MYSQL], FORCE_FATAL);
      return ;
    }
    it.second->assign(element->GetText());
  }
}

void	Config::parseModules(const XMLElement *modules) {
  for (const XMLElement *element = modules->FirstChildElement() ; element ; element = element->NextSiblingElement()) {
    const XMLElement *name = element->FirstChildElement(this->_tokens[MOD_NAME].c_str());
    const XMLElement *flag = element->FirstChildElement(this->_tokens[MOD_FLAG].c_str());
    if (!name) {
      this->_correct = false;
      Logger::getSingleton()->log(std::string("Cannot find tag '") + this->_tokens[MOD_NAME] +
				  "' inside tag '" + this->_tokens[CAT_MODULES], FORCE_FATAL);
      return ;
    }
    if (!flag) {
      this->_correct = false;
      Logger::getSingleton()->log(std::string("Cannot find tag '") + this->_tokens[MOD_FLAG] +
				  "' inside tag '" + this->_tokens[CAT_MODULES], FORCE_FATAL);
      return ;
    }
    this->_modulesFlags[name->GetText()] = flag->GetText();
  }
}

const std::string	&Config::getModuleFlag(const std::string &module) const {
  return (this->_modulesFlags.at(module));
}

const Config::sqlData	&Config::getSQLData(void) const {
  return (this->_sql);
}

bool	Config::isCorrect(void) const {
  return (this->_correct);
}
