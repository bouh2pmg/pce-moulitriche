//
// StaticFunc.cpp for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Mon Jun 18 15:23:02 2012 romuald scharre
// Last update Sat Jan 11 14:22:43 2014 Grégory Neut
//

#include			"StaticFunc.hh"

StaticFunc::StaticFunc()
{

}

bool				StaticFunc::compareString(const std::string &s1, const std::string &s2)
{
  if (s1.compare(s2) < 0)
    return (true);
  return (false);
}

void				StaticFunc::func(Cheaters *cheat)
{
  std::stringstream		ss;

  ss << *cheat;
  Logger::getSingleton()->log(ss.str(), NONE);
}

bool				StaticFunc::greater(Cheaters *cheat1, Cheaters *cheat2)
{
  if (cheat1->getPower().first > cheat2->getPower().first)
    return (true);
  else if (cheat1->getPower().first == cheat2->getPower().first
	   && cheat1->getCouple().first->getLogin().compare(cheat2->getCouple().first->getLogin()) < 0)
    return (true);
  return (false);
}

bool				StaticFunc::smaller(Cheaters *cheat1, Cheaters *cheat2)
{
  float				min1, min2;

  if (cheat1->getPower().first < cheat1->getPower().second)
    min1 = cheat1->getPower().first;
  else
    min1 = cheat1->getPower().second;
  if (cheat2->getPower().first < cheat2->getPower().second)
    min2 = cheat2->getPower().first;
  else
    min2 = cheat2->getPower().second;
  return (min1 < min2);
}

bool				StaticFunc::greaterMatch(const std::pair<Func, Func> &f1, const std::pair<Func, Func> &f2)
{
  if (f1.first.getPercent() > f2.first.getPercent())
    return (true);
  else if (f1.first.getPercent() == f2.first.getPercent() &&
	   f1.second.getPercent() > f2.second.getPercent())
    return (true);
  return (false);
}
