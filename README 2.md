# Documentation Trichounette

Vous trouverez sur cette page, une petite documentation concernant le fonctionnement de la moulitriche.
Tout d'abord, la moulitriche a pour but d'analyser 1 ou plusieurs projets realises par les étudiants durant leur cursus scolaire, quand je dis analyser cela veut dire retrouver les potentiels tricheurs ;).
Je vous recommande donc de lire cette documentation attentivement si vous ne savez pas vous servir de la moulitriche !

 - [Utilisation du dépôt](#utilisation-du-depot)
 - [Les fichiers de configuration](#les-fichiers-de-configuration)
 - [Les modules](#les-modules)
 - [Les principales étapes de la moulitriche](#les-principales-étapes-de-la-moulitriche)
 - [Documentation](#documentation)

## Utilisation du dépôt

Le dépôt doit être utilisé de la manière suivante :

 - Créer une branche par modification
 - Une fois la modification effectuée, merge votre branche avec la branche `dev`
 - Tester les modifications directement sur le serveur de dev (importez un projet que vous avez en local si nécessaire)
 - Une fois la modification fonctionnelle, merge la branche `dev` avec la branche `master` (après avoir vu avec le responsable du projet)

## Les fichiers de configuration

### Le fichier de projet

Le fichier de projet est un élément essentiel au fonctionnement de la moulitriche, sans lui aucun projet ne sera analysé.
Il est formaté en XML et contient :

 - Le nom du projet
 - Le path du projet
 - Le semestre
 - Le module
 - La promotion

```xml
<?xml version="1.0"?>
<projects>
  <project>
    <name>Test my_script 2</name>
    <path_project>/home/geoffrey/Projects/trichounette/myscript</path_project>
    <semester>B4</semester>
    <module>PSU</module>
    <promotion>2017</promotion>
  </project>
</projects>
```
Il est possible d'enchainer plusieurs projets à la suite.

### Le fichier de configuration de la moulitriche

Le fichier de configuration de la moulitriche permet de passer d'un environnement de développement à un environnement de production sans modifier pas mal de morceaux du code.

Il doit être appellé `config.xml`, et il contient les informations suivantes :

 - La configuration SQL : adresse du serveur, nom d'utilisateur, mot de passe et nom de la base
 - Pour chaque module, il contient l'extension ajoutée par le compilateur lors de l génération de l'AST.

```xml
<?xml version="1.0"?>
<config>
  <mysql>
    <hostname>10.11.254.102</hostname>
    <user>trichounette-web</user>
    <password>LpGBNr5dJwMubveW</password>
    <dbname>trichounette-web</dbname>
  </mysql>
  <modules>
    <module>
      <name>C</name>
      <flag>.165t.optimized</flag>
    </module>
    <module>
      <name>CPP</name>
      <flag>.165t.optimized</flag>
    </module>
  </modules>
</config>
```

 
## Les modules

Les modules sont très importants pour le fonctionnement de la moulitriche, sans eux, elle n'effectuera aucune analyse sur les projets.
Ils permettent de :

 - supprimer les fichiers inutiles se trouvant dans les rendus des étudiants (`ILanguage::cleanRelease`)
 - vérifier les extensions/shebangs de chaque fichier se trouvant dans les rendus des étudiants (`ILanguage::identifier`)
 - compiler les fichiers sources pour créer un AST qui nous évitera les falsifications des fichiers (variables inutiles, modification du nom des fonctions, ...) (`ILanguage::preprocess`)
 - tokeniser, c'est a dire lire le fichier créer lors de la compilation pour en créer des grammes, qui seront utilisé par la moulitriche pour détecter les potentiels tricheurs (`ILanguage::lexer`)
 - récupérer les codes sources des fichiers qui match (`ILanguage::match`)

Chaque module doit hériter de l'interface `ILanguage` ( [modules/headers/ILanguage.hh](modules/headers/ILanguage.hh)).

```c++
class                   ILanguage
{
public:
  virtual ~ILanguage() {}

public:
  /**
   ** Clean la release
   ** @param file Fichier à supprimer
   ** @return true si le fichier a été supprimé
   */
  virtual bool                                  cleanRelease(const std::string &file) = 0;

  /**
   ** Renvoies les tokens d'un fichier ayant l'extention .gram
   ** @param release La release sur laquelle on travaille
   ** @param path Le chemin du fichier
   ** @return Les tokens du fichier stockés dans un VectorToken
   */
  virtual VectorToken                           getCache(Release *release, const std::string &path) = 0;

  /**
   ** Retournes l'identificateur des fichier du langage
   ** @return Paire de VectorString nécessaire à l'identification
   */
  virtual std::pair<VectorString, VectorString> &identifier() = 0;

  /**
   ** "Compile" les fichiers sources puis crée un fichier .concat des sources "compilées"
   ** @param release La Release
   ** @param path Le chemin
   */
  virtual void                                  preprocess(Release *release, const std::string &path) = 0;

  /**
   ** Parser le fichier (avec l'extension .concat, créé via ILanguage::preprocess) en utilisant les regex (libRE2)
   ** @param release La Release
   ** @param path Le chemin
   ** @return Les tokens stockés dans un VectorToken
   */
  virtual VectorToken                           lexer(Release *release, const std::string &path) = 0;

  /**
   ** Strip le fichier avec l'extension .concat pour matcher les lignes de ces fichiers entre les 2 cheaters
   ** @param cheaters Les cheaters
   ** @param path Le chemin
   ** @return Vector de Match
   */
  virtual std::vector<Match>                    match(Cheaters *cheaters, const std::string &path) = 0;

  /**
   ** Retournes le nom du module
   ** @return Le nom du module
   */
  virtual const std::string                     &getLanguageName() const = 0;
};
```

## Les principales étapes de la moulitriche

 - [Ramassage](#ramassage)
 - [Ré-order](#ré-order)
 - [Détection des tricheurs](#détection-des-tricheurs)
   - [Chargement des modules et création des FileTree](#chargement-des-modules-et-création-des-filetree)
   - [Compilation](#compilation)
   - [Tokenisation](#tokenisation)
   - [Détection des potentiels tricheurs](#détection-des-potentiels-tricheurs)
   - [Récupération des codes sources et insertion en base de données](#récupération-des-codes-sources-et-insertion-en-base-de-données)

### Ramassage

### Ré-order

### Détection des tricheurs

Le robot lance la moulitriche après avoir créé un fichier de configuration en fonction du projet.

#### Chargement des modules et création des FileTree

Tout d'abord, au lancement de la moulitriche, le fichier de configuration est chargeé en mémoire qui sera utilisé pour l'analyse des projets.
Dès le fichier de configuration chargé, c'est les modules qui seront chargés via la méthode `Core::loadModules`. Aussitôt les modules chargés, elle procédera à la création des FileTree de chaque projet pour chaque rendu via la méthode `Core::createRelease` qui appellera la méthode `Core::createFileTree`.

#### Compilation

Après que les fileTree soient crée, la méthode `ILanguage::preprocess` du module sera appelée est servira a compiler les fichiers du rendu selon le module, s'ils ne l'ont pas été fait précédemment, et créera un fichier `login_x-promo.concat` contenant l'AST du code de l'étudiant.
Dans le cas contraire, une vérification est faite afin d'eviter une recompilation inutile du rendu via la méthode de l'interface des modules `ILanguage::getCache` et retournera un vecteur de grammes.
Ce vecteur de grammes, retourné par la méthode `ILanguage::getCache`, aura été généré grace à la méthode `ILanguage::preprocess`

#### Tokenisation

Cette partie de la moulitriche est très importante, car c'est ici qu'elle parse le fichier `login_x-promo.gram` généré par la méthode `ILanguage::preprocess` via la méthode `ILanguage::lexer` et qu'elle transforme les lignes du fichier par des tokens qui cette suite de tokens créeront des grammes (par label, selon le module).
Aussitôt le fichier `login_x-promo.gram` parsé et tokenisé, la méthode `ILanguage::lexer` retournera un vecteur de grammes qui sera utile pour la suite.

#### Détection des potentiels tricheurs

Dès le vecteur de grammes récupéré, une hashtab sera créé qui contiendra comme `clé` un gramme et comme `valeur` une structure qui contient :

 - une map de rendu/nombre d'apparition
 - le poid du gramme
 - le nombre d'occurences

Ensuite grace à cette hashtab rempli, deux types de tricheurs sont repérés **les tricheurs par quantités**, et **les tricheurs par valeur moyenne**.

 - tricheurs par quantités = nombre de grammes en communs avec un autre tricheur * le poid du gramme
 - tricheurs par valeur moyenne = tricheurs par quantités / nombre de gramme total du tricheur

Seul 100 des tricheurs par quantités et 100 des tricheurs par valeur moyenne sont gardés.

#### Récupération des codes sources et insertion en base de données

La dernière étape de la moulitriche consiste à identifier les fichiers générés par les modules.
Un vecteur de Cheaters a été créé précédemment, grace à ce vecteur, la moulitriche peut enfin procéder au match de fichiers entre un couple de tricheurs.
Le vecteur contenant les tricheurs potentiels sera envoyé a la méthode `ILanguage::match`, qui dans un premier temps, fera une abstraction des noms des variables et des fonctions du fichier (`login_x-promo.concat`), précédemment généré par la méthode `ILanguage::preprocess`, et qui générera un nouveau fichier stripé (`login_x-promo.concat_strip`).
Dans un deuxième temps, elle matchera les fichiers lignes par lignes.
Puis dans un troisième temps, elle matchera fonction par fonction.
Et pour terminer, elle extraira le code des fichiers sources dont le pourcentage de match relevé est supérieur ou égal à 50 %.
Aussitôt les codes sources du couple de tricheur retourner par la méthode `ILanguage::match`, ils sont directement insérés en base de données et sont identifiables via le site de la [trichounette](https://10.11.254.100).

## Documentation

Une documentation doxygen est disponible pour les développeurs.

