GREEN	=	\033[0;32m
BLUE=		\033[0;34m
NOCOLOR	=	\033[0m

MainDir=	./srcs/

DEBUG =	0

NAME=		moulitriche
SRCS=		$(MainDir)main.cpp				\
		$(MainDir)Logger.cpp				\
		$(MainDir)Cheaters.cpp				\
		$(MainDir)Release.cpp				\
		$(MainDir)Func.cpp				\
		$(MainDir)Match.cpp				\
		$(MainDir)AThreadUnix.cpp			\
		$(MainDir)Instance.cpp				\
		$(MainDir)LoaderUnix.cpp			\
		$(MainDir)Core.cpp				\
		$(MainDir)Date.cpp				\
		$(MainDir)Project.cpp				\
		$(MainDir)FileTree.cpp				\
		$(MainDir)StaticFunc.cpp			\
		$(MainDir)MutexUnix.cpp				\
		$(MainDir)LanguageDetector.cpp			\
		$(MainDir)Config.cpp				\
		$(MainDir)Option.cpp				\
		$(MainDir)mysql/Connector.cpp			\
		$(MainDir)mysql/Result.cpp			\
		$(MainDir)mysql/Exception.cpp			\
		$(MainDir)mysql/query/AQuery.cpp		\
		$(MainDir)mysql/query/Select.cpp		\
		$(MainDir)mysql/query/Insert.cpp		\
		$(MainDir)mysql/query/HasWhereCondition.cpp	\
		$(MainDir)mysql/query/HasFields.cpp		\
		$(MainDir)mysql/query/HasTable.cpp		\
		$(MainDir)mysql/query/HasOrderBy.cpp		\
		$(MainDir)mysql/query/Transaction.cpp


OBJS=		$(SRCS:.cpp=.o)

DEPS_EXT=	.deps

DEPS=		$(OBJS:.o=$(DEPS_EXT))

CXX=		g++

RE2_PATH=		./libs/re2/
RE2_PATH_SO=		$(RE2_PATH)/obj/so/
RE2_PATH_INC=		$(RE2_PATH)
TINYXML2_PATH=		./libs/tinyxml2/
TINYXML2_PATH_SO=	$(TINYXML2_PATH)
TINYXML2_PATH_INC=	$(TINYXML2_PATH)

MODULES_PATH=	./modules
MODULES_FILE=	$(MODULES_PATH)/modules.make

LDFLAGS=	-lpthread -ldl -lboost_filesystem -lmysqlclient -lboost_system
LDFLAGS+=	-Wl,-rpath,$(TINYXML2_PATH_SO) -L$(TINYXML2_PATH_SO) -ltinyxml2 -L$(RE2_PATH_SO)
LDFLAGS+=	-Wl,-rpath,$(RE2_PATH_SO) -lre2
LDFLAGS+=	-export-dynamic

CXXFLAGS=	-W -Wall -Wextra -std=c++0x -I./headers -I./modules/headers -I/usr/include/mysql
CXXFLAGS+=	-I$(TINYXML2_PATH_INC) -I$(RE2_PATH_INC)

ifeq ($(DEBUG), 1)
	CXXFLAGS+=	-ggdb3 -DDEBUG
else
	CXXFLAGS+= 	-DNDEBUG
endif

RM=		rm -rf

all:		tinyxml2 re2 module $(NAME)

$(NAME):	$(OBJS)
		$(CXX) -o $(NAME) $(CXXFLAGS) $(OBJS) $(LDFLAGS)

clean:		clean_module
		@$(RM) $(OBJS)
		@echo "Object(s) cleaned."

fclean:		clean
		@$(RM) $(NAME)
		@echo "Executable cleaned."

include $(MODULES_FILE)

doxygen:
		doxygen doc/config.doxy
		@echo "Running doxygen to create documentation"

tinyxml2:
		@cd $(TINYXML2_PATH) && cmake . && $(MAKE)

re2:
		@cd $(RE2_PATH) && $(MAKE)

hardclean:	fclean fclean_module hardclean_module clean_deps

clean_deps:
		@$(RM) $(DEPS)

reset:		hardclean
		@cd $(RE2_PATH) && make clean
		@cd $(TINYXML2_PATH) && make clean

re: 		hardclean all

%.o:		%.cpp
		@$(CXX) -c $(CXXFLAGS) -MMD $< -o $@ -MF $(patsubst %.o, %$(DEPS_EXT), $@) && echo "CXX\t$<"

-include $(DEPS)
