//
// LoaderUnix.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:59:44 2012 romuald scharre
// Last update Tue Apr 21 16:04:28 2015 raphael defreitas
//

#ifndef			__LOADERUNIX_HH__
# define		__LOADERUNIX_HH__

#include		<dlfcn.h>
#include		<iostream>
#include		<cstdlib>
#include		<string>

#include		"ILoader.hh"

/**
 ** Abstraction du chargement de librairies dynamiques sous Unix
 */
class			LoaderUnix : public ILoader
{
private:
  void			*_handle; /**< Pointeur sur la zone mémoire contenant la librairie chargée */
  ILanguage		*_object; /**< Pointeur sur l'objet instancié par la librairie */

public:

  /**
   ** Charge une librairie dynamique (dlsym)
   ** @return Le point d'entrée de la librairie
   */
  ILanguage		*load();

  /**
   ** Ferme une librairie dynamique (dlclose)
   */
  void			unload();

  /**
   ** Ouvre une librairie dynamique (dlopen)
   ** @param path Chemin vers la librairie
   ** @param name Nom de la librairie
   ** @return True si la librairie est bien ouverte
   */
  bool			open(const std::string &, const std::string &);

public:
  LoaderUnix();
  virtual ~LoaderUnix();
};

#endif			/* ! __DLLOADERUNIX_HH__ */
