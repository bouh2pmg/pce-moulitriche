//
// Insert.hpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 15:07:58 2014 geoffrey bauduin
// Last update Wed May  7 15:34:06 2014 geoffrey bauduin
//

#ifndef MYSQL_QUERY_INSERT_HPP_
# define MYSQL_QUERY_INSERT_HPP_

#include	"mysql/query/AQuery.hpp"
#include	"mysql/query/HasTable.hpp"
#include	"mysql/query/HasFields.hpp"

namespace	Mysql {

  namespace	Query {

    class	Insert: public AQuery, public HasTable, public HasFields {

    private:
      bool	_delayed; /**<INSERT DELAYED ? */

    public:
      Insert(const std::string &table = "");
      virtual ~Insert(void);

      virtual void	compute(void) const;

      /**
       ** Ajoute / Retire le mot clé DELAYED dans la requête
       ** @param status true si il faut spécifier le mot clé
       **
       */
      void	setDelayed(bool);

    };

  }

}

#endif
