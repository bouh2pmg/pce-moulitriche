//
// Query.hpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec  6 17:06:34 2013 geoffrey bauduin
// Last update Mon May 12 14:23:07 2014 geoffrey bauduin
//

#ifndef MYSQL_QUERY_HPP_
# define MYSQL_QUERY_HPP_

#include	<list>
#include	<string>

namespace	Mysql {

  namespace Query {

    /** Type de requête */
    enum Type {
      INSERT = 0,
      UPDATE,
      DELETE,
      SELECT,
      TRANSACTION,
      NONE
    };

    class	AQuery {

    private:
      Mysql::Query::Type	_type; /**<Type de la requête */

    protected:
      mutable std::string	_string; /**<La requête en format SQL */

    public:
      AQuery(Mysql::Query::Type);
      AQuery(const std::string &);
      virtual ~AQuery(void);

      /**
       ** Retourne la requête SQL créée après appel à la fonction 'compute'
       ** @return La requête SQL au format string correspondant à l'objet courant
       */
      const std::string	&toString(void) const;

      /**
       ** Crée une string contenant la requête SQL à partir des informations contenues dans l'objet
       */
      virtual void	compute(void) const = 0;

    };

  }

}

#endif
