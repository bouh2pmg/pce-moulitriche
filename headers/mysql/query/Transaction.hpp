//
// Transaction.hpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Mon May 12 14:03:30 2014 geoffrey bauduin
// Last update Mon May 12 14:19:50 2014 geoffrey bauduin
//

#ifndef MYSQL_QUERY_TRANSACTION_HPP_
# define MYSQL_QUERY_TRANSACTION_HPP_

#include	<vector>
#include	"mysql/query/AQuery.hpp"

namespace	Mysql {

  namespace	Query {

    /**
     ** Contient une transaction MySQL
     ** (START TRANSACTION ... COMMIT ;)
     */
    class	Transaction : public AQuery {

    private:
      std::vector<Mysql::Query::AQuery *>	_queries;

    public:
      Transaction(void);
      virtual ~Transaction(void);

      void		addQuery(Mysql::Query::AQuery *);
      virtual void	compute(void) const;

    };

  }

}

#endif
