//
// HasWhereCondition.hpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 14:57:33 2014 geoffrey bauduin
// Last update Wed May  7 15:40:14 2014 geoffrey bauduin
//

#ifndef MYSQL_QUERY_HASWHERECONDITION_HPP_
# define MYSQL_QUERY_HASWHERECONDITION_HPP_

#include	<list>

namespace	Mysql {

  namespace	Query {

    /**
     ** Classe implémentée par toutes les requêtes pouvant dépendre du mot clé WHERE
     */
    class HasWhereCondition {

    public:
      /** Liens entre les conditions */
      enum Link {
	AND = 0,
	OR,
	NONE
      };
      struct where {
	Mysql::Query::HasWhereCondition::Link link; /**<Lien */
	std::string field; /**<Champ */
	std::string value; /**<Valeur */
      };
      typedef std::list<where> Where; /**<Liste de conditions */

    private:
      Mysql::Query::HasWhereCondition::Where	_where; /**<Conditions de la requête */

      protected:
      /**
       ** Constructeur
       */
      HasWhereCondition(void);

      /**
       ** Destructeur
       */
      virtual ~HasWhereCondition(void);

      /**
       ** Retournes les conditions
       ** @return Les conditions
       */
      const Mysql::Query::HasWhereCondition::Where	&getWhere(void) const;

    public:
      /**
       ** Ajoute une condition à la requête
       ** @param field Nom du champ
       ** @param value Valeur (ne gère que le '=')
       ** @param link Le lien logique entre la dernière condition et celle-ci (NONE si c'est la première condition
       */
      void	where(const std::string &, const std::string &, Mysql::Query::HasWhereCondition::Link link = Mysql::Query::HasWhereCondition::NONE);

    };

  }

}

#endif
