//
// HasOrderBy.hpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Mon May 19 16:18:54 2014 geoffrey bauduin
// Last update Mon May 19 16:29:46 2014 geoffrey bauduin
//

#ifndef MYSQL_QUERY_HASORDERBY_HPP_
# define MYSQL_QUERY_HASORDERBY_HPP_

#include	<vector>
#include	<utility>
#include	<string>

namespace	Mysql {

  namespace	Query {

    /**
     ** Classe implémentée par toutes les classes qui peuvent effectuer un tri lors d'une requête
     */

    class	HasOrderBy {

    public:
      enum	Sort {
	ASC,
	DESC
      };

      typedef std::vector<std::pair<std::string, Sort> >	Container;

    private:
      Container	_orderBy;

    protected:
      HasOrderBy(void);
      virtual ~HasOrderBy(void);

    public:
      void	orderBy(const std::string &, Sort order = ASC);
      const Container	&getOrderBy(void) const;

    };

  }

}

#endif
