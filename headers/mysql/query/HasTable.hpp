//
// HasTable.hpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 15:04:06 2014 geoffrey bauduin
// Last update Wed May  7 15:38:44 2014 geoffrey bauduin
//

#ifndef MYSQL_QUERY_HASTABLE_HPP_
# define MYSQL_QUERY_HASTABLE_HPP_

#include	<string>

namespace	Mysql {

  namespace	Query {

    class	HasTable {

    private:
      std::string	_table;

    protected:
      HasTable(const std::string &table = "");
      virtual ~HasTable(void);

      /**
       ** Retournes le nom de la table sur laquelle la requête a lieu
       ** @return Le nom de la table sur laquelle la requête aura lieu
       */
      const std::string	&getTable(void) const;

    public:
      /**
       ** Change le nom de la table sur laquelle la requête aura lieu
       ** @param table La table sur laquelle la requête doit avoir lieu
       */
      void	setTable(const std::string &);

    };

  }

}

#endif
