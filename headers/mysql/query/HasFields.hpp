//
// HasFields.hpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 14:55:18 2014 geoffrey bauduin
// Last update Wed May  7 15:39:37 2014 geoffrey bauduin
//

#ifndef MYSQL_QUERY_HASFIELDS_HPP_
# define MYSQL_QUERY_HASFIELDS_HPP_

namespace Mysql {

  namespace	Query {

    /**
     ** Classe implémentée par toutes les classes qui possèdent des champs dans leur requête
     ** (ailleurs que dans les conditions)
     */
    class HasFields {

    private:
      Mysql::Fields	_fields; /**<Champs/Valeurs de la requête */

    protected:
      HasFields(void);
      virtual ~HasFields(void);

      /**
       ** Retournes les champs
       ** @return Les champs
       */
      const Mysql::Fields &getFields(void) const;

    public:
      /**
       ** Ajoutes un champ et une valeur (optionnel) dans la requête
       ** @param field Nom du champ
       ** @param value Valeur (vide par défaut)
       */
      void	addField(const std::string &, const std::string &value = "");

      /**
       ** Retires un champ de la requête
       ** @param field Nom du champ
       */
      void	removeField(const std::string &);

    };

  }
}

#endif
