//
// Select.hpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed May  7 15:00:17 2014 geoffrey bauduin
// Last update Mon May 19 16:26:12 2014 geoffrey bauduin
//

#ifndef MYSQL_QUERY_SELECT_HPP_
# define MYSQL_QUERY_SELECT_HPP_

#include	"mysql/query/AQuery.hpp"
#include	"mysql/query/HasWhereCondition.hpp"
#include	"mysql/query/HasFields.hpp"
#include	"mysql/query/HasTable.hpp"
#include	"mysql/query/HasOrderBy.hpp"

namespace	Mysql {

  namespace	Query {

    class	Select: public AQuery, public HasWhereCondition, public HasFields, public HasTable, public HasOrderBy {

    public:
      Select(const std::string &table = "");
      virtual ~Select(void);

      virtual void	compute(void) const;

    };

  }

}

#endif
