//
// Mysql.hpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec  6 17:24:13 2013 geoffrey bauduin
// Last update Mon May 12 14:29:23 2014 geoffrey bauduin
//

#ifndef MYSQL_MYSQL_HPP_
# define MYSQL_MYSQL_HPP_

#include	<map>
#include	<string>

namespace	Mysql {

  /**
   ** Typedef pour définir les conteneurs de champs
   */
  typedef std::map<std::string, std::string>	Fields;

}

#include	"query/Insert.hpp"
#include	"query/Select.hpp"
#include	"query/Transaction.hpp"
#include	"Result.hpp"
#include	"Connector.hpp"
#include	"Exception.hpp"

#endif
