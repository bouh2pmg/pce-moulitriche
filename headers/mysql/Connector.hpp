//
// Mysql.hpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec  6 16:15:06 2013 geoffrey bauduin
// Last update Wed May  7 15:29:26 2014 geoffrey bauduin
//

#ifndef MYSQL_CONNECTOR_HPP_
# define MYSQL_CONNECTOR_HPP_

#include	<mysql/mysql.h>
#include <fstream>
namespace	Mysql {

  /**
   ** SQL CONNECTOR
   */
  class	Connector {

  private:
    MYSQL	*_bdd; /**<Pointeur pour la libraire mysql */
    std::ofstream	_logfile; /**<Fichier de log contenant toutes les requêtes effectuées */

    /**
     ** Ecrit dans le fichier de log la requête si le fichier existe
     ** @param request La requête à logger
     */
    void	log(const std::string &);

  public:
    Connector(void);
    ~Connector(void);

    /**
     ** Connecte l'objet à la base de données spécifiée
     ** @param hostname Chemin vers l'hôte de la base sql
     ** @param user Nom d'utilisateur
     ** @param password Mot de passe
     ** @param database Nom de la base de données
     ** @return True si la connexion a réussie
     */
    bool		connect(const std::string &, const std::string &, const std::string &, const std::string &);

    /**
     ** Déconnecte l'objet de la base de données
     */
    void		disconnect(void);

    /**
     ** Execute une requête
     ** @param query La requête à executer
     ** @return Le résultat de la requête (NULL si la requête a échoué)
     */
    Mysql::Result	*execute(const Mysql::Query::AQuery &);

    /**
     ** Retourne le dernier ID inséré en base de données
     ** @return L'ID
     */
    const std::string	lastInsertId(void);

    /**
     ** Retourne la dernière erreur rencontrée
     ** @return Une string contenant l'erreur. Ne pas free cette chaîne.
     */
    const char		*error(void) const;

  };

}

#endif
