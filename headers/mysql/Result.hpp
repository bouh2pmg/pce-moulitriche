//
// Result.hpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec  6 17:17:09 2013 geoffrey bauduin
// Last update Fri Dec 20 11:22:11 2013 geoffrey bauduin
//

#ifndef MYSQL_RESULT_HPP_
# define MYSQL_RESULT_HPP_

#include	<mysql/mysql.h>

namespace	Mysql {

  class	Result {

  private:
    unsigned int				_rows; /**<Le nombre de rows */
    bool					_valid; /**<Définit si le résultat est valide */
    std::map<unsigned int, Mysql::Fields>	_fields; /**<Map de Fields indexée sur les rows */

  public:
    /**
     ** Constructeur de la classe
     ** @param bdd Objet de la lib mysql
     */
    Result(MYSQL *);
    ~Result(void);

    /**
     ** Indique si le résultat est valide ou non
     ** @return True si le résultat est valide
     */
    bool	valid(void) const;

    /**
     ** Retournes le tableau de Fields
     ** @param row Le row qu'on veut récupérer
     ** @return Les fields du row qu'on récupère
     */
    const Mysql::Fields	&operator[](unsigned int) const;

    /**
     ** Retournes le nombre de row dans la requête
     ** @return Le nombre de row
     */
    unsigned int	rowCount(void) const;
  };

}

#endif
