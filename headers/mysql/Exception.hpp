//
// Exception.hpp for  in /home/geoffrey/Projects/mysql
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec 13 10:42:52 2013 geoffrey bauduin
// Last update Fri Dec 20 11:21:45 2013 geoffrey bauduin
//

#ifndef MYSQL_EXCEPTION_HPP_
# define MYSQL_EXCEPTION_HPP_

#include	<stdexcept>

namespace	Mysql {

  /**
   ** Exception Mysql
   */
  class	Exception: public std::exception {

  private:
    std::string	_what; /**<Message définissant l'exception */

  public:
    /**
     ** Constructeur de la classe
     ** @param what Message
     */
    Exception(const std::string &) throw();
    virtual ~Exception(void) throw() {}

    /**
     ** Retournes le message
     ** @return Le message
     */
    const char	*what(void) const throw();

  };

}

#endif
