//
// HashTab.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:59:02 2012 romuald scharre
// Last update Fri Dec 20 10:33:54 2013 geoffrey bauduin
//

#ifndef				__HASHTAB_HH__
# define			__HASHTAB_HH__

#include			<unordered_map>

/**
 ** Définit les ngrams
 */
typedef struct			s_infos_ngram
{
  std::unordered_map<Release *,
		     int>	_users;
  int				_weight;
  int				_nbOccurTot;
}				InfosNgram;

/**
 ** Définit une hashtab
 */
class				HashTab : public std::unordered_map<std::string, InfosNgram >
{

};

#endif
