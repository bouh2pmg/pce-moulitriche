//
// Logger.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 10:00:14 2012 romuald scharre
// Last update Wed Apr  9 10:40:24 2014 geoffrey bauduin
//

#ifndef				__LOGGER_HH__
# define			__LOGGER_HH__

#include			<map>
#include			"Singleton.hh"
#include			"IMutex.hh"

/**
 ** Type de niveau de Log
 ** Les types forces permettent d'afficher même si le mode verbose n'est pas activé
 */
enum				LOG_LEVEL
  {
    NONE = -1,
    ERROR,
    FATAL,
    WARNING,
    FORCE_NONE,
    FORCE_ERROR,
    FORCE_FATAL,
    FORCE_WARNING
  };

/**
 ** Permet d'afficher des informations pendant l'execution du programme.
 ** Classe en Singleton
 */
class				Logger : public Singleton<Logger>
{
private:
  friend class			Singleton<Logger>;
  /**
   ** Niveau de log
   */
  LOG_LEVEL			_level;
  /**
   ** Mutex de la classe
   */
  IMutex			*_mutex;

  char				_opt;

  std::map<LOG_LEVEL, std::string>	_stringLevel;

private:
  Logger();
  ~Logger();

public:
  /**
   ** Affiches un le message sur l'entrée standard avec une couleur en fonction du niveau
   ** @param log Le message à afficher
   ** @param level Le niveau
   */
  void				log(const std::string &, LOG_LEVEL level = NONE);

public:
  /**
   ** Définit un niveau
   ** @param level Le niveau
   */
  void				setLevel(LOG_LEVEL level);

};

#endif
