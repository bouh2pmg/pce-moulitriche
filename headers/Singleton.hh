//
// Singleton.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 10:00:35 2012 romuald scharre
// Last update Sat Jan 11 14:35:41 2014 Grégory Neut
//

#ifndef			__SINGLETON_HH__
# define		__SINGLETON_HH__

#include		<iostream>

/**
 ** A implémenter pour créer une classe Singleton
 */
template<typename T>
class			Singleton
{
protected:
  Singleton() {}
  ~Singleton() {}

public:
  /**
   ** Initialise une instance de la classe
   */
  static void		initialize()
  {
    _instance = new T;
  }

  /**
   ** Récupères l'instance de la classe
   ** @return L'instance de la classe
   */
  static T		*getSingleton()
  {
    if (!_instance)
      _instance = new T ;
    return (static_cast<T*>(_instance));
  }

  /**
   ** Supprime l'instance de la classe
   */
  static void		dispose()
  {
    if (_instance)
      delete _instance;
    _instance = NULL;
  }


private:
  /**
   ** L'instance de la classe
   */
  static T		*_instance;
};

template<typename T>
T				*Singleton<T>::_instance = NULL;

#endif
