//
// Cheaters.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Mon Jun 18 15:19:19 2012 romuald scharre
// Last update Fri Dec 20 15:45:31 2013 geoffrey bauduin
//

#ifndef				__CHEATERS_HH__
# define			__CHEATERS_HH__

#include			<iomanip>

#include			"Release.hh"

/**
 ** Contient un couple de cheaters ainsi que toutes les informations utiles à leur sujet
 ** afin de permettre un tri plus simple
 */
class				Cheaters
{
public:
  /**
   ** Constructeur de la classe
   ** @param couple Couple de cheaters
   ** @param power "power"
   ** @param language Langage de la release
   */
  Cheaters(const std::pair<Release *, Release *> &,
	   const std::pair<float, float> &,
	   const std::string &);
  ~Cheaters();

public:
  /**
   ** Getter sur le couple
   ** @return La paire de Release définissant le couple de Cheaters
   */
  std::pair<Release*, Release*>	&getCouple();

  /**
   ** Getter sur "power"
   ** @return Le power
   */
  std::pair<float, float>	&getPower();

  /**
   ** Getter sur le langage
   ** @return std::string le langage de la release
   */
  const std::string		&getLanguage() const;

  /**
   ** Getter sur le nombre de hits
   ** @return Le nombre de hits
   */
  int				getNbHit() const;

  /**
   ** Getter sur le pourcentage
   ** @return Le pourcentage
   */
  std::pair<float, float>	&getPercent();

public:

  /**
   ** Setter de pourcentage
   ** @param percent Pourcentage
   */
  void				setPercent(std::pair<float, float> &);

  /**
   ** Setter du nombre de hits
   ** @param nbHit nombre de hits
   */
  void				setNbHit(int);

private:
  std::pair<Release*, Release*>	_couple; /**< Paire de Release qui définit le couple de cheaters */
  std::pair<float, float>		_power; /**< Puissance */
  std::string			_language; /**< Langage */
  std::pair<float, float>	_percent; /**< Pourcentage */
  int				_nbHit; /**< Nombre de hit */
};

/**
 ** Décrit entièrement la classe Cheaters
 ** @param os Stream de sortie
 ** @param cheaters Couple de cheaters à décrire
 */
std::ostream			&operator<<(std::ostream &os, Cheaters &cheaters);

#endif
