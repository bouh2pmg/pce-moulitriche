//
// Option.hpp for  in /home/romuald/Projects/moulitriche
//
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
//
// Started on  Mon Jun 18 15:15:46 2012 romuald scharre
// Last update Wed Apr  9 10:13:58 2014 geoffrey bauduin
//

#ifndef				__OPTION_HPP__
# define			__OPTION_HPP__

#include			"Singleton.hh"

class				Option : public Singleton<Option>
{

private:
  friend class                  Singleton<Option>;

  /**
   ** Constructeur de la classe
   ** Mets les options à NONE
   */
  Option(void);

  /**
   ** Destructeur de la classe
   */
  virtual ~Option(void);


public:
  /**
   ** Enum décrivant les options
   */
  enum Opt {
    NONE =		0b00000, /** < Aucune option n'est activée */
    VERBOSE =		0b00001, /** < Le mode verbose affiche quelques informations pour pouvoir suivre le fil d'exécution */
    HARDVERBOSE =	0b00010 /** < Le mode hardverbose affiche toutes les informations possibles */
  };

  /**
   ** Ajoute une option
   ** @param option L'option à ajouter
   */
  void	setOption(Option::Opt);

  /**
   ** Retire une option
   ** @param option L'option à retirer
   */
  void	unsetOption(Option::Opt);

  /**
   ** Testes si une option est activée
   ** @param option L'option à tester
   ** @return TRUE si l'option est activée
   */
  bool	hasOption(Option::Opt) const;

private:

  /**
   ** Options
   */
  int	_options;

};

#endif
