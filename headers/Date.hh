//
// Date.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:58:45 2012 romuald scharre
// Last update Fri Dec 20 11:50:53 2013 geoffrey bauduin
//

#ifndef				__DATE_HH__
# define			__DATE_HH__

#include			<string>
#include			<sstream>
#include			<time.h>

/**
 * Objet retournant des informations précises sur la date
 */
class				Date
{
private:
  Date();
  ~Date();

public:
  /**
   * Retourne la date actuelle au format texte (i.e. Dimanche 8 Decembre)
   * @return La date actuelle au format texte
   */
  static const std::string	getDate();

  /**
   * Retourne l'heure actuelle au format texte (i.e. "-- 18:30:32 -- ")
   * @return L'heure actuelle au format texte
   */
  static const std::string	getTime();
};

#endif
