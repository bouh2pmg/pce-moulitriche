//
// Project.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 10:00:25 2012 romuald scharre
// Last update Thu Apr  3 14:27:41 2014 geoffrey bauduin
//

#ifndef				__PROJECT_HH__
# define			__PROJECT_HH__

#include			<iostream>
#include			<unordered_map>
#include			<string>
#include	<vector>
#include	<map>
#include	"tinyxml2.h"
/**
 ** Projet
 */
class				Project
{
public:
  Project();
  virtual ~Project() {}

public:

  /**
   ** Parse les fichiers XML de projet
   */
  class	Parser {

  private:
    /** Enum représentant les tokens */
    enum Token {
      PROJECTS,
      PROJECT,
      PRJ_NAME,
      PRJ_PATH_PROJECT,
      PRJ_SEMESTER,
      PRJ_MODULE,
      PRJ_PROMOTION
    };

  private:
    std::map<Token, std::string>	_tokens; /** Hashtab de tokens */
    std::vector<Project *>		_projects; /* Tableau des projets à controller */

    /**
     ** Parse un fichier XML
     ** @param filename Le chemin vers le fichier à parser
     */
    void	parseFile(const std::string &);

    /**
     ** Parse un projet
     ** @param element Le pointeur sur l'élément <projet>
     ** @param tokens Un tableau de tokens à retrouver dans l'élément
     */
    void	parseProject(const tinyxml2::XMLElement *, const std::vector<Token> &);

  public:
    /**
     ** Constructeur prenant le chemin du fichier à parser en paramètres
     ** @param filename Chemin vers le fichier à parser
     */
    Parser(const std::string &);

    /**
     ** Destructeur
     */
    ~Parser(void);

    /**
     ** Permet de récupérer un tableau contenant les projets à parser
     ** @return Le tableau de projets
     */
    const std::vector<Project *>	&getProjects(void) const;

  };

  /**
   ** Récupère la valeur correspondant à la clef passée
   ** @param key La clef dont on veut la valeur
   ** @return La valeur correspondant à la clef
   */
  const std::string		&getValue(const std::string &key) const;

  /**
   ** Définit une valeur à une clef
   ** @param key La clef dont on veut définir la valeur
   ** @param value La valeur qu'on veut définir à la clef
   */
  void				setValue(const std::string &key,
					 const std::string &value);

private:
  /**
   ** Map de valeurs
   */
  std::unordered_map<std::string,
		     std::string>	_value;
};

/**
 ** Surcharge permettant d'afficher les informations d'un projet
 ** @param os Le stream d'affichage
 ** @param proj Le projet à afficher
 */
std::ostream			&operator<<(std::ostream &os, Project &proj);

#endif
