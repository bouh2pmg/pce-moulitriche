//
// Release.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 10:00:28 2012 romuald scharre
// Last update Tue Dec 16 15:19:31 2014 Philip Garnero
//

#ifndef				__RELEASE_HH__
# define			__RELEASE_HH__

#include			<iostream>
#include			<string>
#include			<boost/filesystem.hpp>
#include			<map>

#include			"FileTree.hh"

/**
 ** Release
 */
class				Release
{
public:
  /**
   ** Constructeur de la classe
   ** @param id ID de la release
   ** @param login Login du propriétaire de la release
   ** @param promo La promotion du propriétaire
   ** @param city La ville du propriétaire
   ** @param path Le chemin vers sa release
   */
  Release(int, const std::string &,
	  int, const std::string &,
	  const std::string &,
	  const std::string &);
  ~Release();

public:
  /**
   ** Getter sur le chemin de la release
   ** @return Une string contenant le chemin vers la release
   */
  const std::string		&getPath() const;

  /**
   ** Getter sur le FileTree de la Release
   ** @return Le FileTree de la Release
   */
  FileTree			&getTree();

  /**
   ** Getter sur l'id de la Release
   ** @return L'id de la Release
   */
  int				getID() const;

  /**
   ** Getter sur le login du propriétaire
   ** @return Une string contenant le login du propriétaire
   */
  const std::string		&getLogin() const;

  /**
   ** Getter sur la CommonRelease
   ** @return La CommonRelease
   */
  std::unordered_map<Release *,
		     int>	&getCommonRelease(const std::string &);

  /**
   ** Getter sur le potentiel
   ** @return Le potentiel
   */
  int				getPotentiel(const std::string &);

  /**
   ** Getter sur la promo
   ** @return La promo
   */
  int				getPromo() const;

  /**
   ** Getter sur la ville
   ** @return Une string contenant la ville
   */
   const std::string		&getCity() const;

  /**
   ** Getter sur le code de la ville
   ** @return Une string contenant le code de la ville
   */
   const std::string		&getCityCode() const;

public:

  /**
   ** Définit le potentiel
   ** @param language Le langage
   ** @param potentiel Le potentiel
   */
  void				setPotentiel(const std::string &, int);

  /**
   ** Ajoute une release
   ** @param language Le langage
   ** @param release La Release
   ** @param value La valeur de la release
   */
  void				addRelease(const std::string &, Release *, int);

private:

  /**
   ** FileTree de la Release
   */
  FileTree			_tree;

  /**
   ** Chemin vers la Release
   */
  std::string			_path;

  /**
   ** ID de la Release
   */
  int				_id;

  /**
   ** Login du propriétaire
   */
  std::string			_login;

  /**
   ** Promotion du propriétaire
   */
  int				_promo;

  /**
   ** Ville du propriétaire
   */
  std::string			_city;

  /**
   ** Code de la ville du propriétaire
   */
  std::string			_cityCode;

  /**
   ** Potentiel
   */
  std::unordered_map<std::string, int>				_potentiel;

  /**
   ** CommonRelease
   */
  std::unordered_map<std::string, std::unordered_map<Release *, int> >	_commonRelease;
};

/**
 ** Affiches des informations sur la Release
 ** @param os Le stream de sortie
 ** @param release La Release
 ** @return Le stream de sortie
 */
std::ostream			&operator<<(std::ostream &os, const Release &release);

#endif
