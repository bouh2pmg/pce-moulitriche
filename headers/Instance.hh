//
// Instance.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:59:29 2012 romuald scharre
// Last update Fri Dec 20 11:54:54 2013 geoffrey bauduin
//

#ifndef				__INSTANCE_HH__
# define			__INSTANCE_HH__


#include			"AThreadUnix.hh"
#include			"MutexUnix.hh"
#include			"LoaderUnix.hh"



#include			"IMutex.hh"
#include			"IThread.hh"

class				Instance
{
public:
  /**
   ** Récupères une instance de mutex
   ** @return Une mutex
   */
  static IMutex			*getMutexInstance();

  /**
   ** Récupères une instance d'un thread
   ** @return Un Thread
   */
  static IThread		*getThreadInstance();

  /**
   ** Récupères une instance d'un Loader
   ** @return Un Loader
   */
  static ILoader		*getLoaderInstance();
};

#endif
