//
// FileTree.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:58:50 2012 romuald scharre
// Last update Fri Dec 20 11:51:16 2013 geoffrey bauduin
//

#ifndef						__FILETREE_HH__
# define					__FILETREE_HH__

#include					<unordered_map>
#include					<vector>
#include					<string>

/**
 ** Arborescence d'un rendu
 */
class						FileTree
{
public:
  FileTree();
  virtual ~FileTree() {}

public:
  /**
   ** Ajoute une entrée dans le FileTree
   ** @param key La clef à ajouter
   ** @param value La valeur à ajouter
   */
  void						push(const std::string &key, const std::string &value);

  /**
   ** Supprime une entrée dans le FileTree
   ** @param key La clef où la valeur à supprimer se trouve
   ** @param value La valeur à supprimer
   */
  void						erase(const std::string &key, const std::string &value);

  /**
   ** Supprime toutes les entrées du FileTree
   */
  void						clearAll();

  /**
   ** Supprime la totalité des valeurs d'une clef
   ** @param key La clef à supprimer
   */
  void						eraseByKey(const std::string &key);

public:

  /**
   ** Retournes la globalité de l'arbre
   ** @return std::unordered_map<std::string, std::vector<std::string> > le FileTree
   */
  std::unordered_map<std::string,
		     std::vector<std::string> >	&getAllTree();

  /**
   ** Retournes les valeurs correspondant à une clef
   ** @param key La clef dont on veut récupérer les valeurs
   ** @return std::vector<std::string> Le tableau de valeurs correspondant à la clé
   */
  std::vector<std::string>			&getTreeByKey(const std::string &key);

private:
  std::unordered_map<std::string,
		     std::vector<std::string> >	_tree; /**< le Filetree */
};

#endif
