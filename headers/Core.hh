//
// Core.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:58:39 2012 romuald scharre
// Last update Tue Apr 21 15:00:07 2015 raphael defreitas
//

#ifndef				__CORE_HH__
# define			__CORE_HH__

#include			<iostream>
#include			<map>
#include			<unordered_map>
#include			<vector>
#include			<boost/filesystem.hpp>
#include			"re2/re2.h"
#include			<string>
#include			<algorithm>
#include	"Project.hh"
#include			"LanguageDetector.hh"
#include			"Cheaters.hh"
#include			"ILoader.hh"
#include			"Release.hh"
#include			"StaticFunc.hh"
#include			"Type.hh"
#include			"mysql/Mysql.hpp"

/**
 ** Nom du répertoire courant
 */
#define				CURRENT_DIR	std::string(get_current_dir_name())

/**
 ** Nom du répertoire des modules
 */
#define				MOD_DIR		"modules"

/**
 **  Macro qui permet de definir les 40 % des ngram a ne pas conserver
 */
#define				FOURTY_PERCENT(x)      (40 * x) / 100

/**
 ** Extension des modules
 */
#define				EXT		".so"

/**
 ** Nombre d'occurences maximal d'un élève dans une liste de cheaters
 */
#define				MAX_OCCUR_LOGIN	3

/**
 ** structure definissant les infos des ngrams
 */
typedef struct			s_infos_ngram
{
  std::unordered_map<Release *,
		     int>	_users;  /*!<     */
  int				_weight; /*!<      */
  int				_nbOccurTot; /*!<Nombre d'occurence du ngram     */
}				InfosNgram;

typedef				std::unordered_map<std::string, InfosNgram >	HashTab;

class				Core
{

public:
  /**
   ** constructeur prenant en paramètre le nom du fichier de conf
   ** @param path Chemin vers le fichier
   */
  Core(const std::string &path);

  /**
   ** destructeur
   */
  ~Core();


public:
  /**
   ** Fonction qui est la base de la classe Core. Coordine et effectue toutes les actions
   */
  void			run();

  /**
   ** Selection des tricheurs
   ** @param quantity liste des tricheurs par quantite
   ** @param average liste des tricheurs par moyenne
   ** @param cheaters liste des tricheurs final
   */
  void			selection(std::list<Cheaters *> &quantity,
				  std::list<Cheaters *> &average,
				  std::list<Cheaters *> &cheaters);

  /**
   ** Match des cheaters
   ** @param cheaters Liste de Cheaters
   ** @param project Nom du projet
   ** @param promotion Promotion
   ** @param project_path Chemin vers le projet
   ** @param module Module
   ** @param semester Semestre
   */
  void			match(const std::list<Cheaters *> &,
			      const std::string &,
			      const std::string &,
			      const std::string &,
			      const std::string &,
			      const std::string &);

  /**
   ** Insère le résultat dans la base de données
   ** @param cheaters Le couple de cheaters
   ** @param v_match Vecteur de Match
   ** @param bdd Connecteur Mysql
   ** @param id_project ID du projet
   ** @param functions Transaction contenant toutes les fonctions
   */
  void			insertBdd(Cheaters *,
				  std::vector<Match> &,
				  Mysql::Connector *,
				  const std::string &,
				  Mysql::Query::Transaction &);

  /**
   ** Récupère l'id du projet en base de données (en le créant si nécessaire)
   ** @param bdd Connecteur Mysql
   ** @param project Nom du projet
   ** @param promotion Promotion
   ** @param module Module
   ** @param semester Semestre
   ** @param path Le chemin vers les fichiers du projet
   ** @return L'id du projet
   */
  const std::string	insertProject(Mysql::Connector *,
				      const std::string &,
				      const std::string &,
				      const std::string &,
				      const std::string &,
				      const std::string &);

  /**
   ** Insère le résultat dans un fichier
   ** @param cheaters Le couple de cheaters
   ** @param v_match Vecteur de Match
   */
  void			insertFiles(Cheaters *,
				    std::vector<Match> &);

  /**
   ** Affiche l'attribut _hash qui contient pour chaque language touotes les infos sur les ngram
   */
  void			printHash();

  void			printRelease();

  /**
   ** Fonction qui ecrit dans les fichiers 'match_par_quantite' et 'match_par_valeur_moyenne_des_rendus'
   ** les tricheurs potentiel.
   ** @param path chemin ou ecrire les fichiers
   ** @param v_quantity liste des tricheurs par quantite
   ** @param v_average liste des tricheurs par moyenne
   */ 
  void			printInFile(const std::string &path,
				    std::list<Cheaters *> &v_quantity,
				    std::list<Cheaters *> &v_average);

  /**
   ** return le numero de promo
   ** @param p chemin du rendu
   ** @return numero de la promo
   */
  int			getPromo(const std::string &p);

  /**
   ** return le login de l'etudiant
   ** @param p chemin du rendu
   ** @return login de l'etudiant
   */
  const std::string	 getLogin(const std::string &p);

private:

  /**
   ** Methode qui charge tous les modules existant en memoires
   */
  void			loadModules(std::list<ILoader *> &modulesList);

  /**
   ** Methode qui free tous les modules existant en memoires
   */
  void			unloadModules(std::list<ILoader *> &modulesList);

  /**
   ** Verifie l'extension du fichier
   ** @param file nom du fichier
   ** @param ext nom de l'extension recherchee
   ** @return True si l'extension du fichier est bonne flase sinon
   */
  bool			checkExtension(const std::string &file,
				       const std::string &ext);

  /**
   ** Crée une release
   ** @param p Le chemin
   */
  void			createRelease(const std::string &);

  /**
   ** Crée un FileTree pour une release
   ** @param release La release
   ** @param p Le chemin
   */
  void			createFileTree(Release *release, const boost::filesystem::path &p);

  /**
   ** Compile les fichiers sources d'une Release
   ** @param release La release
   ** @param project_path Le chemin vers le projet
   */
  void			compilation(Release *, const std::string &);

  /**
   ** Crée et remplit la hashtab
   ** @param token Vecteur de tokens
   ** @param release La Release
   ** @param name Le nom
   */
  void			createHashTab(VectorToken &,
				      Release *,
				      const std::string &);

  /**
   ** Clean la hashtab afin de faire un tri parmis les tricheurs
   */
  void			cleanHashTab();

  /**
   ** Pré-sélectionne les tricheurs en vidant la table de hash
   ** @param language Le langage
   ** @param hashtab La HashTab
   */
  void			preSelection_bis(const std::string &, HashTab &);

  /**
   ** Pré-sélectionne les tricheurs
   */
  void			preSelection();

  /**
   ** Supprimes un couple de cheateur si une release est récurrente en fonction du nombre de hits
   ** @param release1 La release 1
   ** @param release2 La release 2
   ** @param value La valeur
   ** @param cheaters La liste de Cheaters
   ** @param isAverage ?
   ** @return True s'il faut delete
   */
  bool			getRecurrentLogin(Release *, Release *,
					  float,
					  std::list<Cheaters *> &,
					  bool);

  /**
   ** Supprimes un couple de cheateur si une release est récurrente en fonction du pourcentage
   ** @param release1 La release 1
   ** @param release2 La release 2
   ** @param value La valeur
   ** @param cheaters La liste de Cheaters
   ** @return True si un couple de cheaters a été supprimé
   */
  bool			getRecurrentLoginByAverage(Release *, Release *,
						   float,
						   std::list<Cheaters *> &);

  /**
   ** Insert sort des tricheurs par quantité de tokens communs
   ** @param release1 Premiere Release du couple de cheaters
   ** @param release2 Seconde Release du couple de cheaters
   ** @param power La puissance
   ** @param language Le langage
   ** @param cheaters La liste de Cheaters
   */
  void			insertCheatersByQuantity(Release *,
						 Release *,
						 float,
						 const std::string &,
						 std::list<Cheaters *> &);

  /**
   ** Insert sort des tricheurs par valeur moyenne des rendus
   ** @param release1 Premiere Release du couple de cheaters
   ** @param release2 Seconde Release du couple de cheaters
   ** @param power La puissance
   ** @param language Le langage
   ** @param cheaters La liste de Cheaters
   */
  void			insertCheatersByAverage(Release *,
						Release *,
						float,
						const std::string &,
						std::list<Cheaters *> &);

  /**
   ** Cherche des tricheurs en parcourant la liste des choses communes de chaque rendu
   ** @param quantity La liste de Cheaters par quantité de tokens
   ** @param average La liste de Cheaters en fonction d'une moyenne
   ** @param promotion La promotion
   */
  void			createCheaters(std::list<Cheaters *> &, std::list<Cheaters *> &, int);


  /**
   ** Affiche tous les cheaters trouvés et le nombre d'occurence pour chacun d'entre eux
   ** @param quantity La liste de Cheaters par nombre de hits
   ** @param average La liste de Cheaters par pourcentage
   */
  void			printCheaters(const std::list<Cheaters *> &, const std::list<Cheaters *> &) const;

public:

  void			resetNbCheater();
  int			getNbCheater();
  void			resetNbRequest();
  int			getNbRequest();

private:

  Project::Parser		*_conf; /**<Conf XML */

  LanguageDetector	_languageDetector; /**< Détecteur de langages */

  /**
   * hashtables pour faire le lien entre les noms court et long des villes
   */
  std::map<std::string,
	   std::string>	_mCities;

  /**
   * _vRelease contient des objets Release * qui sont la représentation d'un dossier d'un projet. exemple : Bomberman
   * Ce dossier comprend un créateur, avec le nom de sa ville et l'année de sa promo. Prise en compte de l'arborescence en -R
   */
  std::vector<Release *>	_vRelease;

  /**
   * Vecteur de paire contenant un ILoader et un ILanguage
   * il est genere par loadModules
   */
  std::vector<std::pair<ILoader *, ILanguage *> >	_vLanguage;

  /**
   * hashtables qui pour chaque langage contient une hashtable
   *
   * Cette hashtable contient les ngrams sous forme de std::string
   * et #InfosNgram en second champs pour contenir toutes les infos des ngrams
   */
  std::unordered_map<std::string, std::unordered_map<std::string, InfosNgram > >	_hash;

  int		_nbCheater;
  int		_nbRequest;

};

#endif
