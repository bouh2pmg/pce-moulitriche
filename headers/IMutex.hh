//
// IMutex.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:59:16 2012 romuald scharre
// Last update Fri Dec 20 11:53:40 2013 geoffrey bauduin
//

#ifndef					__IMUTEX_HH__
# define				__IMUTEX_HH__

/**
 ** Interface permettant l'abstraction des mutex
 */
class					IMutex
{
public:
  virtual ~IMutex() {}

  /**
   ** Initialise une mutex
   ** @return True si la mutex est initialisée
   */
  virtual bool				Initialise() = 0;

  /**
   ** Détruit une mutex
   ** @return True si la mutex est détruite
   */
  virtual bool				Destroy() = 0;

  /**
   ** Lock une mutex
   ** @return True si la mutex est lock
   */
  virtual bool				Lock() = 0;

  /**
   ** Délock une mutex
   ** @return True si la mutex est délock
   */
  virtual bool				Unlock() = 0;

  /**
   ** Trylock une mutex
   ** @return True si la mutex est lock
   */
  virtual bool				Trylock() = 0;
};

#endif
