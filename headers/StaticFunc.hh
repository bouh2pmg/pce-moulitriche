//
// StaticFunc.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Mon Jun 18 15:22:56 2012 romuald scharre
// Last update Sat Jan 11 14:24:32 2014 Grégory Neut
//

#ifndef				__STATICFUNC_HH__
# define			__STATICFUNC_HH__

#include			<string>
#include			<iostream>
#include			<sstream>

#include			"Func.hh"
#include			"Cheaters.hh"
#include			"Logger.hh"

class				StaticFunc
{
public:
  StaticFunc();
  virtual ~StaticFunc() {}

public:
  /**
   * Compare deux string en utilisant la fonction std::string::compare
   * @param s1 premiere string
   * @param s2 seconde string
   * @return true si s1 < s2
   */
  static bool			compareString(const std::string &, const std::string &);


  /**
   * Compare deux couples de cheater en fonction du "power"
   * @param cheat1 premier couple de cheaters
   * @param cheat2 second couple de cheaters
   * @return true si le "power" du premier est supérieur à celui du second
   */
  static bool			greater(Cheaters *, Cheaters *);

  /**
   * Compare deux paires de fonctions
   * @param f1 premiere paire de fonctions
   * @param f2 seconde paire de fonctions
   * @return true si la premiere paire de fonctions a un plus gros pourcentage que la seconde
   */
  static bool			greaterMatch(const std::pair<Func, Func> &,
					const std::pair<Func, Func> &);
  /**
   * Compare deux couples de cheater en fonction du "power"
   * @param cheat1 premier couple de cheaters
   * @param cheat2 second couple de cheaters
   * @return true si le "power" du premier est inférieur à celui du second
   */
  static bool			smaller(Cheaters *, Cheaters *);

  /**
   * Appelle la surchage de l'operateur << sur Cheater
   * @param cheat Cheater que l'on veut passer à l'operateur <<
   */
  static void			func(Cheaters *);
};

#endif
