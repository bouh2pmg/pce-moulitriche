//
// LanguageDetector.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:59:38 2012 romuald scharre
// Last update Fri Dec 20 11:55:55 2013 geoffrey bauduin
//

#ifndef				__LANGUAGEDETECTOR_HH__
# define			__LANGUAGEDETECTOR_HH__

#include			<re2/re2.h>
#include			<fstream>

#include			"Release.hh"
#include			"ILoader.hh"
#include			"ILanguage.hh"
#include			"FileTree.hh"

/**
 * Detecte le langage utilisé pour le rendu
 */
class				LanguageDetector
{
public:

  /**
   * Constructeur
   * @param language tableau de langages
   */
  LanguageDetector(std::vector<std::pair<ILoader *, ILanguage *> > &);

public:

  /**
   * Remplit le Filetree en associant le fichier au bon langage.
   * @param tree Filetree à remplir
   * @param file Fichier à ajouter dans le Filetree
   */
  void				flag(FileTree &tree, const std::string &file);

private:

  /**
   * Teste le shebang du fichier
   * @param vShebang vecteur de shebang
   * @param file Chemin du fichier
   * @return True si un shebang du vecteur est celui du fichier
   */
  bool				getByShebang(std::vector<std::string> &,
					     const std::string &);

  /** 
   * Teste l'extension du fichier
   * @param vExt vecteur d'extensions
   * @param file Chemin du fichier
   * @return True si une extension du vecteur est celle du fichier
   */
  bool				getByExtension(std::vector<std::string> &,
					       const std::string &);

private:
  std::vector<std::pair<ILoader *,
			ILanguage *> >	&_vLanguage; /**< Vecteur de paires Loader/Langage */
};


#endif
