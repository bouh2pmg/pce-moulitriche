//
// AMutexUnix.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:57:38 2012 romuald scharre
// Last update Fri Dec 20 11:23:54 2013 geoffrey bauduin
//

#ifndef					__AMUTEXUNIX_HH__
# define				__AMUTEXUNIX_HH__

#include				<pthread.h>

#include				"IMutex.hh"

/**
 ** Abstraction des mutex pour Unix
 */
class					AMutexUnix : public IMutex
{
public:
  AMutexUnix();
  ~AMutexUnix();

public:
  /**
   ** Initialise la mutex (pthread_mutex_init)
   ** @return True si la mutex s'est bien initialisée
   */
  bool					Initialise();

  /**
   ** Détruit la mutex (pthread_mutex_destroy)
   ** @return True si la mutex s'est bien détruite
   */
  bool					Destroy();

  /**
   ** Lock la mutex (pthread_mutex_lock)
   ** @return True si la mutex est lock
   */
  bool					Lock();

  /**
   ** Dévérouille la mutex (pthread_mutex_unlock)
   ** @return True si la mutex est dévérouillée
   */
  bool					Unlock();

  /**
   ** Essaie de lock la mutex (pthread_mutex_trylock)
   ** @return True si la mutex est lock
   */
  bool					Trylock();

private:
  pthread_mutex_t			_mutex; /**<Mutex */
};

#endif
