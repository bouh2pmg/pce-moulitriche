//
// Func.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:45:26 2012 romuald scharre
// Last update Fri Apr 25 11:09:48 2014 geoffrey bauduin
//

#ifndef			__FUNC_HH__
# define		__FUNC_HH__

#include		<iostream>
#include		<string>
#include		<vector>

#include		"Type.hh"

/**
 **
 */
class			Func
{
public:
  Func();
  virtual ~Func() {}

public:
  /**
   ** Getter sur le contenu
   ** @return Le contenu
   */
  const VectorString			&getContent() const;

  /**
   ** Getter sur le nom de la fonction
   ** @return Le nom de la fonction
   */
  const std::string			&getName() const;

  /**
   ** Getter sur le path
   ** @return Le path
   */
  const std::string			&getPath() const;

  /**
   ** Getter sur le pourcentage
   ** @return Le pourcentage
   */
  float					getPercent() const;

  /**
   ** Getter sur le contenu (non constant)
   ** @return Le contenu
   */
  VectorString				&content();

  /**
   ** Getter sur le nbHit
   ** @return Le nombre de Hit
   */
  int					getNbHit() const;

  /**
   ** Retournes le contenu final
   ** @return Le contenu final
   */
  const std::string			&getFinalContent() const;

  /**
   ** Retournes le contenu final (non constant)
   ** @return Le contenu final
   */
  std::string				&getFinalContent();

public:

  /**
   ** Ajoute du contenu
   ** @param content Le contenu à ajouter
   */
  void					addContent(const std::string &content);

  /**
   ** Définit le pourcentage
   ** @param percent Le pourcentage
   */
  void					setPercent(float percent);

  /**
   ** Définit le nom de la fonction
   ** @param name Le nom de la fonction
   */
  void					setName(const std::string &name);

  /**
   ** Définit le chemin de la fonction
   ** @param name Le chemin de la fonction
   */
  void					setPath(const std::string &name);

  /**
   ** Définit le nombre de hit
   ** @param nbHit Le nombre de hit
   */
  void					setNbHit(int);

  /**
   ** Ajoute le contenu final
   ** @param content Le contenu final à ajouter
   */
  void					addFinalContent(const std::string &);

private:
  VectorString				_content; /**< Le contenu */
  std::string				_finalContent; /**< Le contenu final */
  std::string				_name; /**< Le nom */
  float					_percent; /**< Le pourcentage */
  int					_nbHit; /**< Le nombre de hits */
  std::string				_path; /**< Le chemin */
};

/**
 ** Surcharge permettant d'afficher des infos sur une Func
 ** @param os Stream de sortie
 ** @param func La fonction à afficher
 ** @return Le stream de sortie
 */
std::ostream			&operator<<(std::ostream &os, const Func &func);

#endif
