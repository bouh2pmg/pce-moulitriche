//
// ThreadUnix.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:57:56 2012 romuald scharre
// Last update Fri Dec 20 15:37:03 2013 geoffrey bauduin
//

#ifndef				__ATHREADUNIX_HH__
# define			__ATHREADUNIX_HH__

#include			<signal.h>
#include			<pthread.h>

#include			"IThread.hh"

/**
 ** Abstraction des Thread pour Unix
 */
class				AThreadUnix : public IThread
{
public:
  AThreadUnix();
  ~AThreadUnix();

public:
  /**
   ** Crée un nouveau thread (pthread_create)
   ** @param func Pointeur sur la fonction à lancer
   ** @param param Paramètre de la fonction à lancer
   ** @return True si le thread a bien été créé
   */
  bool				Create(void *(*func)(void *), void *param);

  /**
   ** Détruit le thread (pthread_cancel)
   ** @return True si le thread a bien été détruit
   */
  bool				Destroy();

  /**
   ** Attends un thread (pthread_join)
   ** @return True si le thread a bien été attendu
   */
  bool				Wait();

private:
  pthread_t			_t; /**< Le Thread */
};

#endif
