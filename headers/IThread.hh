//
// IThread.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:59:35 2012 romuald scharre
// Last update Fri Dec 20 15:31:25 2013 geoffrey bauduin
//

#ifndef				__ITHREAD_HH__
# define			__ITHREAD_HH__

/**
 ** Interface implémentée pour abstraire les thread
 */
class				IThread
{
public:
  virtual ~IThread() {}

  /**
   ** Crée un thread
   ** @param func Pointeur sur la fonction à appeler
   ** @param param Paramètre de la fonction à appeler
   ** @return True si le thread est créé
   */
  virtual bool			Create(void *(*func)(void *), void *param) = 0;

  /**
   ** Détruit un thread
   ** @return True si le thread est détruit
   */
  virtual bool			Destroy() = 0;

  /**
   ** Attends un thread
   ** @return True si le thread a bien été attendu
   */
  virtual bool			Wait() = 0;
};

#endif
