//
// Match.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Mon Jun 18 15:15:46 2012 romuald scharre
// Last update Fri Apr 25 11:10:41 2014 geoffrey bauduin
//

#ifndef				__MATCH_HH__
# define			__MATCH_HH__

#include			"Func.hh"

/**
 ** Conteneur de match
 */
class				Match
{
public:
  Match();
  virtual ~Match() {}

public:
  /**
   ** Renvoies le nombre de hits
   ** @return Le nombre de hits
   */
  int						getNbHit() const;

  /**
   ** Récupère un vecteur de paires de Func
   ** @return Le vecteur de paires de Func
   */
  const std::vector<std::pair<Func, Func> >	&getFunc() const;

  /**
   ** Récupère un vecteur de paires de Func (non constant)
   ** @return Le vecteur de paires de Func (non constant)
   */
  std::vector<std::pair<Func, Func> >		&func();

public:
  /**
   ** Définit le nombre de hits
   ** @param nbHit Le nombre de hits
   */
  void						setNbHit(int);

  /**
   ** Ajoute une paire de Func dans le match
   ** @param func Paire de Func à ajouter
   */
  void						addFunc(const std::pair<Func, Func> &);

private:
  /**
   ** Nombre de hits
   */
  int						_nbHit;

  /**
   ** Vecteur de paires de Func
   */
  std::vector<std::pair<Func, Func> >		_func;
};

/**
 ** Surcharge permettant d'afficher le contenu d'un match
 ** @param out_stream Le stream d'affichage
 ** @param Match Le match à afficher
 ** @return Le stream d'affichage
 */
std::ostream	&operator<<(std::ostream &, const Match &);

#endif
