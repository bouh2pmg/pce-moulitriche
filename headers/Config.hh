//
// Config.hh for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <geoffrey@epitech.net>
// 
// Started on  Wed Apr  2 11:09:24 2014 geoffrey bauduin
// Last update Thu Apr  3 14:33:47 2014 geoffrey bauduin
//

#ifndef CONFIG_HH_
# define CONFIG_HH_

#include	<string>
#include	<map>
#include	"Singleton.hh"
#include	"tinyxml2.h"

/**
 ** Classe contenant toutes les informations sur la configuration de la moulitriche.
 ** Parse un fichier "config.xml" à la racine
 */
class	Config : public Singleton<Config> {

  friend class Singleton;

private:
  /** Enum symbolisant les balises XML */
  enum Token {
    CONFIG,
    CAT_MYSQL,
    SQL_HOSTNAME,
    SQL_USER,
    SQL_PASSWORD,
    SQL_DBNAME,
    CAT_MODULES,
    MOD_NAME,
    MOD_FLAG
  };

  std::map<Token, std::string>	_tokens; /** Hashtab des Token */

public:
  struct sqlData {
    std::string hostname; /** Adresse du serveur SQL */
    std::string	username; /** Nom de l'utilisateur SQL */
    std::string	password; /** Mot de passe de l'utilisateur SQL */
    std::string	dbname; /** Nom de la base de données à utiliser */
  };

private:
  std::map<std::string, std::string>	_modulesFlags; /** Flag pour chaque module, correspondant à l'AST sorti par le compilateur (ex: .165t.optimized) */
  sqlData				_sql; /** Données SQL */
  bool					_correct; /** Vérifie qu'on a bien réussi à parser */

  /**
   ** Constructeur de la classe
   */
  Config(void);
  /**
   ** Destructeur de la classe
   */
  virtual ~Config(void);

  /**
   ** Parse un fichier
   ** @param filename Le chemin vers le fichier à parser
   */
  void	parseFile(const std::string &);

  /**
   ** Parse les données SQL
   ** @param element L'élément qui contient les balises SQL
   */
  void	parseSQL(const tinyxml2::XMLElement *);

  /**
   ** Parse les modules
   ** @param element L'élément qui contient les balises <module>
   */
  void	parseModules(const tinyxml2::XMLElement *);

public:
  /**
   ** Retournes l'extension du dump de l'AST (change en fonction des machines)
   ** @param language La string du langage
   ** @return L'extension
   */
  const std::string	&getModuleFlag(const std::string &) const;

  /**
   ** Retournes les infos SQL
   ** @return Les infos SQL
   */
  const sqlData		&getSQLData(void) const;

  /**
   ** Vérifie que le parsing s'est bien effectué
   ** @return TRUE si il n'y a pas eu d'erreurs
   */
  bool			isCorrect(void) const;

};

#endif
