//
// Convert.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:58:32 2012 romuald scharre
// Last update Fri Dec 20 11:31:46 2013 geoffrey bauduin
//

#ifndef		__CONVERT_HH__
# define	__CONVERT_HH__

#include	<iostream>
#include	<sstream>

/**
 ** Convertit un type en un autre
 ** @param Ret Type d'arrivée
 ** @param Type Type de départ
 */
template<typename Ret, typename Type>
class		Convert
{
public:

  /**
   ** Convertit l'argument en un type donné
   ** @param value La variable à convertire
   ** @return La valeur convertie
   */
  static const Ret	convert(const Type &value)
  {
    Ret			ret;
    std::stringstream	cv;

    cv << value;
    cv >> ret;
    return (ret);
  }
};

# define		stringToInt(x)		Convert<std::string, int>::convert(x)
# define		stringToFloat(x)	Convert<std::string, float>::convert(x)
# define		intOfString(x)		Convert<int, std::string>::convert(x)
#endif
