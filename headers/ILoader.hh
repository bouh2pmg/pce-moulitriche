//
// ILoader.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:59:09 2012 romuald scharre
// Last update Fri Dec 20 15:31:38 2013 geoffrey bauduin
//

#ifndef			__ILOADER_HH__
# define		__ILOADER_HH__

#include		<string>

#include		"ILanguage.hh"

/**
 ** Interface permettant l'abstraction du chargement de libraries dynamiques
 */
class			ILoader
{
public:
  virtual ~ILoader() {}

public:
  /**
   ** Charge une librairie dynamique
   ** @return Objet de langage créé lors du chargement de la librairie dynamique
   */
  virtual ILanguage		*load() = 0;

  /**
   ** Décharge une librairie dynamique
   */
  virtual void			unload() = 0;

  /**
   ** Ouvre une librairie dynamique
   ** @param path Chemin vers la librairie
   ** @param name Nom de la librairie
   ** @return True si la librairie a bien été chargée
   */
  virtual bool			open(const std::string &path, const std::string &name) = 0;
};

#endif
