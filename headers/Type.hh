//
// Type.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:43:57 2012 romuald scharre
// Last update Tue Jun 19 09:43:58 2012 romuald scharre
//

#ifndef				__TYPE_HH__
# define			__TYPE_HH__

#include			<string>
#include			<vector>
#include			<list>

typedef				std::string			Token;
typedef				std::vector<std::string>	VectorString;
typedef				std::vector<Token>		VectorToken;
typedef				std::list<std::string>		ListString;

#endif
