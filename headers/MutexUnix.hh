//
// AMutexUnix.hh for  in /home/romuald/Projects/moulitriche
// 
// Made by romuald scharre
// Login   <scharr_r@epitech.net>
// 
// Started on  Tue Jun 19 09:57:38 2012 romuald scharre
// Last update Fri Dec 20 15:14:10 2013 geoffrey bauduin
//

#ifndef					__AMUTEXUNIX_HH__
# define				__AMUTEXUNIX_HH__

#include				<pthread.h>

#include				"IMutex.hh"

/**
 * Classe pour gerer les mutex sous Unix
 * herite de #IMutex
 */
class					MutexUnix : public IMutex
{
public:
  MutexUnix();
  ~MutexUnix();

public:
  /**
   * methode permettant d'initialiser la Mutex
   * @return booleen qui sert de retour d'erreur
   */
  bool					Initialise();
  /**
   * supprime la mutex
   * @return booleen qui sert de retour d'erreur
   */
  bool					Destroy();
  /**
   * methode pour locker la mutex
   * @return booleen qui sert de retour d'erreur
   */
  bool					Lock();
  /**
   * methode pour delocker la mutex
   * @return booleen qui sert de retour d'erreur
   */
  bool					Unlock();
  /**
   * methode pour essayer de locker la mutex
   * @return booleen qui sert de retour d'erreur
   */
  bool					Trylock();

private:
  /**
   * attribut contenant la mutex
   */
  pthread_mutex_t			_mutex;
};

#endif
