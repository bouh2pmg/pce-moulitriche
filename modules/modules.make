ifndef MODULES_PATH
$(error MODULES_PATH is not defined)
endif

GREEN	?=	\033[0;32m
BLUE	?=	\033[0;34m
NOCOLOR	?=	\033[0m

C_LANGUAGE=	$(MODULES_PATH)/C_Language.so
CPP_LANGUAGE=	$(MODULES_PATH)/CPP_Language.so
PERL_LANGUAGE=	$(MODULES_PATH)/Perl_Language.so
PYTHON_LANGUAGE= $(MODULES_PATH)/Python_Language.so

C_SRCS=		$(MODULES_PATH)/srcs/C_Language.cpp		\
		$(MODULES_PATH)/srcs/ALanguage.cpp

PERL_SRCS=	$(MODULES_PATH)/srcs/Perl_Language.cpp

CPP_SRCS=	$(MODULES_PATH)/srcs/CPP_Language.cpp		\
		$(MODULES_PATH)/srcs/ALanguage.cpp

PYTHON_SRCS=	$(MODULES_PATH)/srcs/Python_Language.cpp

C_OBJS=		$(C_SRCS:.cpp=.o)
CPP_OBJS=	$(CPP_SRCS:.cpp=.o)
PERL_OBJS=	$(PERL_SRCS:.cpp=.o)
PYTHON_OBJS=	$(PYTHON_SRCS:.cpp=.o)

DEPS_C=		$(C_OBJS:.o=$(DEPS_EXT))
DEPS_CPP=	$(CPP_OBJS:.o=$(DEPS_EXT))
DEPS_PERL=	$(PERL_OBJS:.o=$(DEPS_EXT))
DEPS_PYTHON=	$(PYTHON_OBJS:.o=$(DEPS_EXT))

CXXFLAGS_MODULE=	-fPIC

LDFLAGS_MODULE=		-shared

module:			$(C_LANGUAGE) $(CPP_LANGUAGE) $(PYTHON_LANGUAGE)
			@echo "Libraries compiled."

$(C_LANGUAGE):		CXXFLAGS += $(CXXFLAGS_MODULE)
$(C_LANGUAGE):		$(C_OBJS)
			$(CXX) -o $(C_LANGUAGE) $(C_OBJS) $(LDFLAGS_MODULE)

$(CPP_LANGUAGE):	CXXFLAGS += $(CXXFLAGS_MODULE)
$(CPP_LANGUAGE):	$(CPP_OBJS)
			$(CXX) -o $(CPP_LANGUAGE) $(CPP_OBJS) $(LDFLAGS_MODULE)

$(PERL_LANGUAGE):	CXXFLAGS += $(CXXFLAGS_MODULE)
$(PERL_LANGUAGE):	$(PERL_OBJS)
			$(CXX) -o $(PERL_LANGUAGE) $(PERL_OBJS) $(LDFLAGS_MODULE)


$(PYTHON_LANGUAGE):	CXXFLAGS += $(CXXFLAGS_MODULE)
$(PYTHON_LANGUAGE):	$(PYTHON_OBJS)
			$(CXX) -o $(PYTHON_LANGUAGE) $(PYTHON_OBJS) $(LDFLAGS_MODULE)


clean_module:
			@$(RM) $(C_OBJS) $(CPP_OBJS) $(PYTHON_OBJS)
			@echo "Object(s) cleaned."

fclean_module:		clean
			@$(RM) $(C_LANGUAGE) $(PERL_LANGUAGE) $(CPP_LANGUAGE) $(PYTHON_LANGUAGE)
			@echo "Library cleaned."

hardclean_module:
			@$(RM) $(DEPS_CPP) $(DEPS_C) $(DEPS_PERL) $(DEPS_PYTHON)

re_module:		fclean_module module

-include $(DEPS_CPP)
-include $(DEPS_C)
-include $(DEPS_PERL)
-include $(DEPS_PYTHON)
