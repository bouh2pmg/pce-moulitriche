//
// ILanguage.hh for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec 20 10:35:16 2013 geoffrey bauduin
// Last update Mon May  5 11:52:58 2014 geoffrey bauduin
//

#ifndef			__ILANGUAGE_HH__
# define		__ILANGUAGE_HH__

#include		<iostream>
#include		<string>
#include		<vector>
#include		"re2/re2.h"

#include		"Cheaters.hh"
#include		"Release.hh"
#include		"Match.hh"
#include		"Type.hh"

class			ILanguage
{
public:
  virtual ~ILanguage() {}

public:
  /**
   ** Clean la release
   ** @param file Fichier à supprimer
   ** @return true si le fichier a été supprimé
   */
  virtual bool					cleanRelease(const std::string &file) = 0;

  /**
   ** Renvoies les tokens d'un fichier ayant l'extention .gram
   ** @param release La release sur laquelle on travaille
   ** @param path Le chemin du fichier
   ** @return Les tokens du fichier stockés dans un VectorToken
   */
  virtual VectorToken				getCache(Release *release, const std::string &path) = 0;

  /**
   ** Retournes l'identificateur des fichier du langage
   ** @return Paire de VectorString nécessaire à l'identification
   */
  virtual std::pair<VectorString, VectorString>	&identifier() = 0;

  /**
   ** "Compile" les fichiers sources puis crée un fichier .concat des sources "compilées"
   ** @param release La Release
   ** @param path Le chemin
   */
  virtual void					preprocess(Release *release, const std::string &path) = 0;

  /**
   ** Parser le fichier (avec l'extension .concat, créé via ILanguage::preprocess) en utilisant les regex (libRE2)
   ** @param release La Release
   ** @param path Le chemin
   ** @return Les tokens stockés dans un VectorToken
   */
  virtual VectorToken				lexer(Release *release, const std::string &path) = 0;

  /**
   ** Strip le fichier avec l'extension .concat pour matcher les lignes de ces fichiers entre les 2 cheaters
   ** @param cheaters Les cheaters
   ** @param path Le chemin
   ** @return Vector de Match
   */
  virtual std::vector<Match>			match(Cheaters *cheaters, const std::string &path) = 0;

  /**
   ** Retournes le nom du module
   ** @return Le nom du module
   */
  virtual const std::string			&getLanguageName() const = 0;
};

#endif

