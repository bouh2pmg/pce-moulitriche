/*
// ALanguage.hh for moulitriche in /home/neut_g/PROJETS/Tek3/moulitriche/trichounette/trichounette
// 
// Made by Grégory Neut
// Login   <neut_g@epitech.net>
// 
// Started on Wed Nov 20 16:06:11 2013 Grégory Neut
// Last update Tue Nov 25 16:50:52 2014 Philip Garnero
*/

#ifndef			__ALANGUAGE_HH__
# define		__ALANGUAGE_HH__

#include		<iostream>
#include		<string>
#include		<vector>
#include		<boost/filesystem.hpp>
#include		<fstream>
#include		<limits.h>
#include		<stdlib.h>

#include		"ILanguage.hh"
#include		"IThread.hh"
#include		"IMutex.hh"
#include		"StaticFunc.hh"

/*
** Définition du nombre maximal de thread
*/
# define			MAX_THREADS	4

/*
** Récupération du répertoire courant
*/
# define			CURRENT_DIR	std::string(get_current_dir_name())

class				ALanguage;
class				Scope;

typedef				std::vector<Func>			VectorFunc;
typedef				void (ALanguage::*FuncPtr)(std::string &, std::string &);
typedef				void (*StaticFuncPtr)(Scope&, std::string &, std::string &);

/**
 **	Structure contenant les données utilisé pour la compilation (structure donnée au thread en paramètre)
 */
typedef struct			s_compile
{
  ListString			l;			/**< Nom absolu du fichier a compiler*/
  std::string			flags;			/**< Flags de compilation a ajouter*/
  std::string			path;			/**< Repertoire du projet de l'etudiant*/
  std::string			compilator;		/**< Nom du compilateur a executer*/
  std::string			languageName;		/**< Nom du language*/
  std::string			genFileExtent;		/**< Extension du ficher generé pour la creation du concat*/
  Release			*release;		/**< Classe release de l'etudiant*/
  IMutex			*mConcat;		/**< Mutex permettant de verouiller l'ecriture du fichier concat*/
  IMutex			*mList;			/**< Mutex permettant de verouiller l'acces a la liste */
}				t_compile;

/**
**	Classe deffinissant un token
*/
class				Tokens
{
public:

  /**
   ** Constructeur de la classe
   ** @param regex Regex du token
   ** @param token Token
   ** @param func Pointeur sur la methode qui traduit la ligne pour former le fichier concat_strip
   ** @param nbLines Nombre de lignes
   ** @param toLine Regex de reprise
   */
  Tokens(const std::string &regex,
	 const std::string &token,
	 FuncPtr func,
	 unsigned int nbLines = 1,
	 std::string toLine = "") :
    _regex(regex), _token(token), _func(func), _nbLines(nbLines), _toLine(toLine) {}

  /**
   * < Regex correspondant a un token
   */
  std::string		_regex;

  /**
   * < Tag du token pour la formation des gram
   */
  std::string		_token;

  /**
   * < Pointeur sur methode permettant de traduire la ligne pour la formation du fichier concat_strip
   */
  FuncPtr			_func;

  /**
   * < Nombre de ligne a ignorer (1 par defaut)
   */
  unsigned int		_nbLines;
	
  /**
   * < Regex de reprise de la tokenisation (si _nbLignes vaut 0)
   */
  std::string		_toLine;
};

/**
 **	Classe deffinissant une ligne a ne pas tokeniser
 */
class				SkipLines
{
public:

  /**
   ** Constructeur de la classe
   ** @param regex Regex permettant de récupérer la ligne
   ** @param nbLines nombre de lignes à ignorer
   ** @param toLine Regex pour savoir quand reprendre la tokenisation
   */
  SkipLines(const std::string &regex,
	    unsigned int nbLines = 1,
	    std::string toLine = "") :
    _regex(regex), _nbLines(nbLines), _toLine(toLine) {}

  /**
   * < Regex permettant de recuperer la ligne
   */
  std::string		_regex;

  /**
   * < Nombre de ligne a ignorer (1 par defaut)
   */
  unsigned int		_nbLines;

  /**
   * < Regex de reprise de la tokenisation (si _nbLignes vaut 0)
   */
  std::string		_toLine;
};

/**
 **	Classe définissant un scope
 */
class				Scope
{
public:

  /**
   ** Constructeur de la classe
   ** @param regexBegin Regex de début
   ** @param regexEnd Regex de fin
   ** @param func Pointeur sur fonction
   */
  Scope(const std::string&regexBegin,
	const std::string&regexEnd,
	StaticFuncPtr func = NULL) :
    _regexBegin(regexBegin), _regexEnd(regexEnd), _func(func) {}
  
  /**
   * < Regex de debut de scope
   */
  std::string		_regexBegin;
  
  /**
   * < Regex de fin de scope
   */
  std::string		_regexEnd;

  /**
   * < Pointeur sur fonction pour definir une fonction de nomage du scope (NULL par defaut)
   */
  StaticFuncPtr		_func;
};

/**
 ** classe abstraite de chaque module de language
 */
class						ALanguage : public ILanguage
{

public:

  /**
   ** Constructeur de la classe
   ** @param name Nom du module
   ** @param comp Compilateur
   ** @param compExtent Extension du compilateur
   */
  ALanguage(const std::string&, const std::string&, const std::string&);

  /**
   ** Destructeur de la classe
   */
  virtual ~ALanguage();


protected:

  /**
   * < Nom du language
   */
  std::string				_name;
	
  /**
   * < Nom du compilateur a excecuter
   */
  std::string				_compilator;				

  /**
   * < Extension du fichier generé par le compilateur
   */
  std::string				_genFileCompExtent;
	
  /**
   * < Thread Pool
   */
  IThread*				_thPool[MAX_THREADS];
	
  /**
   * < Liste des regex permettant de recuperer les fichiers a supprimer
   */
  ListString				_releaseToClean;
	
  /**
   * < Map contenant les regex recuperant des types de fichier associé a un tag de type std::string
   */
  std::map<std::string, std::string>	_filesTypes;

  /**
   * < le tag correspondant au type de ficher a compiler
   */
  std::string				_fileTypeToCompile;

  /**
   * < Permet la detection de language : le premier parametre est un tableau d'extension, le second un tableau de shebang
   */
  std::pair<VectorString, VectorString>	_identifier;

  /**
   * < Tableau contenant la liste des scopes a isoler dans le fichier concat
   */
  std::vector<Scope>			_vScopes;

  /**
   * < Tableau contenant la liste des scopes a isoler dans le code source de l'etudiant
   */
  std::vector<Scope>			_vCodeScopes;

  /**
   * < Tableau contenant la liste des scopes a ignorer dans le fichier concat
   */
  std::vector<Scope>			_skipScopes;

  /**
   * < Liste des tokens permettant de tokenisé chaque scope
   */
  std::vector<Tokens>			_vTokens;

  /**
   * < Liste d'objets nous permettant de ne pas tokenisé certaines lignes
   */
  std::vector<SkipLines>			_skip;

  /**
   * < Liste de regex nous permettant de couper les gram d'un scope
   */
  VectorString				_vSeparator;

  VectorString				_localFunc;

  /**
   * < Liste des fonctions à ignorer pour éviter le match sur la libmy
   */
  VectorString        _ignFuncs;

public:

  /**
   ** Fonction executé par le thread de compilation.
   ** Compile les fichiers en paramètre
   ** @param param Structure contenant les informations sur les fichiers à compiler
   */
  static void					*compile(void *param);

  /**
   ** Fonction appelé par le thread de compilation pour completer le fichier concat.
   ** @param comp Informations sur les fichiers compilés
   ** @param path Chemin vers les fichiers
   */
  static void					copyDataToConcatFile(t_compile *, const std::string &);
  

  /**
   ** methode qui permet de verifier si le fichier est connu par le langage
   ** @param file Fichier à vérifier
   ** @return True si le fichier est connu par le langage
   */
  virtual bool				cleanRelease(const std::string &);

  /**
   ** methode qui recupere les tokens d'un fichier avec l'extention '.gram' si celui ci est existant
   ** @param release La release dont on veut récupérer les tokens
   ** @param path Le chemin vers la release
   ** @return Un vecteur de tokens contenus dans la release
   */
  virtual VectorToken			getCache(Release *, const std::string &);

  /**
   ** methode qui permet d'identifier quels fichiers sont necessaire au langage
   ** Voir ILanguage
   */
  virtual std::pair<VectorString, VectorString>	&identifier();

  /**
   ** methode qui permet de compiler les fichiers sources si celui ci permet de le faire.
   ** Et ensuite creer un fichier avec l'extension '.concat' des sources compile si il y en a.
   ** Voir ILanguage
   ** @param release La release
   ** @param p Le chemin
   */
  virtual void				preprocess(Release *, const std::string &);

  /**
   ** methode qui parse le fichier avec l'extension '.concat', precedemment creer par la methode 'preprocess',
   ** grace aux regex et la lib RE2.
   ** Voir ILanguage
   ** @param release La release
   ** @param path Le chemin
   ** @return Un vecteur de tokens
   */
  virtual VectorToken			lexer(Release *, const std::string &);

  /**
   * methode qui strip le fichier avec l'extension '.concat'
   * pour en creer un avec l'extension '.concat_strip'
   * pour ensuite matcher les lignes de ces fichiers entre les 2 cheaters
   * 2 methodes sont utilises :
   *	- line par line.
   *	- fonction par fonction :
   *		- methode de la fonction qui match avec un pourcentage >= 80%
   *		- methode de recuperation des fonctions dont la somme etant <= 120%
   * @param cheaters Un couple de cheaters
   * @param p Le chemin
   * @return Un vecteur de Match
   */
  virtual std::vector<Match>		match(Cheaters *, const std::string &);

  /**
   ** Methode qui permet de recuperer le nom du module.
   ** @return Le nom du module
   */
  virtual const std::string		&getLanguageName() const;

  /**
   ** methode utilise pour la formation du fichier concat_strip : Correspondant au token FUNC
   ** @param line La ligne où le token est présent
   ** @param newLine La nouvelle ligne
   */
  virtual void				func(std::string &, std::string &);

  /**
   ** methode utilise pour la formation du fichier concat_strip : Correspondant au token COND
   ** @param line La ligne où le token est présent
   ** @param newLine La nouvelle ligne
   */
  virtual void				cond(std::string &, std::string &);

  /**
   ** methode utilise pour la formation du fichier concat_strip : Correspondant au token OPE
   ** @param line La ligne où le token est présent
   ** @param newLine La nouvelle ligne
   */  
  virtual void				ope(std::string &, std::string &);

  /**
   ** methode utilise pour la formation du fichier concat_strip : Correspondant au token MEM
   ** @param line La ligne où le token est présent
   ** @param newLine La nouvelle ligne
   */
  virtual void				mem(std::string &, std::string &);

  /**
   ** methode utilise pour la formation du fichier concat_strip : Correspondant au token CAST
   ** @param line La ligne où le token est présent
   ** @param newLine La nouvelle ligne
   */
  virtual void				cast(std::string&, std::string&);


protected:

  /**
   ** Vérifie sur la ligne est un nombre
   ** @param line Ligne à vérifier
   ** @return True si la ligne est un nombre
   */
  virtual bool				checkNumber(const std::string &);

  /**
   ** Récupère le paramètre
   ** @param line Ligne où il faut récupérer le paramètre
   ** @param newLine String où stocker le paramètre
   ** @param idx Index maximal pour la recherche dans la string
   */
  virtual void				getParam(std::string &, std::string &, int);

  /**
   ** "trim" la ligne
   ** @param line Ligne à "trim"
   */
  virtual void				trimLine(std::string &);
  
  /**
   * Cette methode permet de recuperer tout les fichiers correspondant
   * a un type de fichier defini dans _filesTypes classé dans une map par type.
   * @param tree Vecteur de fichiers
   * @param files Fichiers correspondant au type de fichier défini dans la classe
   */
  virtual void				getFiles(std::vector<std::string> &,
						 std::map<std::string, ListString> &);

  /**
   ** Split une liste en 2
   ** @param l Liste à split
   ** @param size Taille de la nouvelle liste
   ** @return La nouvelle liste de string
   */
  virtual std::list<std::string>		split(std::list<std::string> &, int);

  /**
   ** Cette methode retourne une string corespondant aux flags de compilation.
   ** Par defaut vaut une chaine vide
   ** @param files Liste de fichiers
   ** @return Une string contenant les flags de compilation
   */
  virtual std::string			getCompilationOptions(std::map<std::string, ListString> &);

  /**
   * Cette methode retourne une ListString de fichier a compiler
   * Par defaut retourne la liste corespondant au _fileTypeToCompile ou une liste vide si le _fileTypeToCompile
   * @param files Fichiers
   * @return Liste de fichiers à compiler
   */
  virtual ListString			getFileToBeCompiled(std::map<std::string, ListString> &);

  /**
   **  Cette methode permet de recuperer tout les scopes non ignoré present dans le fichier concat.
   ** @param release La release
   ** @param path Le chemin
   ** @param data Un conteneur pour stocker les scopes ignorés
   ** @return True si le fichier ".concat" a pu être ouvert
   */
  virtual bool				getScopedData(Release *release,
						      const std::string &path,
						      std::map<std::string,
							       std::list<ListString> >& data);

  /**
   * Methode permettant de retirer les scopes que l'on ne souhaite pas traiter une fois recuperer par getScopedData.
   * Par defaut cette methode ne fait rien.
   * @param data Conteneur de scopes
   */
  virtual void				trimScopedData(std::map<std::string, std::list<ListString> >&);

  /**
   * Methode qui permet de passé d'une ligne a une autre en ignorant des lignes au besoin.
   * Décrémente nbLines jusqu'à 0, puis utilise la regex
   * @param list La liste de lignes
   * @param it Iterateur sur la liste de lignes
   * @param nbLines Nombre de lignes à skip
   * @param toLine Regex à matcher pour recommencer la tokenisation
   */
  virtual void				tokenizationSkipLine(ListString& list,
							     ListString::iterator& it,
							     unsigned int nbLines,
							     const std::string& toLine);

  /**
   * Methode appelé par startTokenization permettant de traiter un scope.
   * @param list La liste de lignes
   * @param gram Le gram
   * @param token Un vecteur de tokens
   * @param out Stream de sortie
   */
  void					tokenizeList(ListString&,
						     std::string&,
						     VectorToken&,
						     std::ofstream* = NULL);

  /**
   * Methode permettant de lancer la tokenization sur tout les scopes de tout les fichiers.
   * @param data Conteneur de scopes
   * @param token Vecteur de tokens
   * @param out Stream de sortie
   * @return True
   */  
  virtual bool				startTokenization(std::map<std::string,
								   std::list<ListString> > &,
							  VectorToken &,
							  std::ofstream &);

  /**
   * Cette methode ecrit le gram donné en parametre dans le fichier
   * si il est different de NULL et l'insert dans le VectorToken de la trichounette
   * @param gram Gram à push
   * @param token Liste de tokens
   * @param out Stream de sortie
   */
  virtual void				pushGram(std::string&, VectorToken&, std::ofstream*);

  /**
   * Methode permettant d'initialiser le nom d'une fonction
   * en utilisant la premiere ligne du scope
   * et copie le nom dans le troisieme parametre si il est different de NULL.
   * @param line Ligne contenant le nom d'une fonction
   * @param func Objet fonction à définir
   * @param funcName Nom de la fonction
   */
  virtual void				setFuncName(std::string&, Func&, std::string* = NULL);

  /**
   * Cette methode permet de créer une fonction a partir d'un scope.
   *  Elle recupere le token corespondant a chaque ligne pour formater
   * chaque ligne en instruction generale en utilisant les pointeurs sur fonction des tokens.
   *  Réecrit le nouveau scope generé dans le ficher concat_strip.
   * @param path Le chemin
   * @param scope Une liste de scopes
   * @param v_func Un vecteur de Func
   * @param v_content Un vecteur de contenus
   * @param out Stream de sortie
   */
  virtual void				createFunc(const std::string &path,
						   ListString &scope,
						   VectorFunc &v_func,
						   VectorString &v_content,
						   std::ofstream &out);

  /**
   * Cette methode ecrit chaque scope dans le concat_strip
   * @param scopedData données de scope
   * @param v_func Un vecteur de Func
   * @param v_content Un vecteur de contenu
   * @param out Stream de sortie
   */
  virtual void				storeScopeInConcatStrip(std::map<std::string,
									 std::list<ListString> > & scopedData,
								VectorFunc &v_func,
								VectorString &v_content,
								std::ofstream &out);

  /**
   * Cette methode crée le fichier concat_strip.
   *  Utilisé dans la methode match.
   * @param release La release
   * @param v_func Un vecteur de Func
   * @param v_content Un vecteur de contenu
   * @param path Le chemin
   */
  virtual void				createFileConcatStrip(Release *release,
							      VectorFunc& v_func,
							      VectorString& v_content,
							      const std::string& path);

  /**
   * Methode permettant de recuperer le cache du fichier concat_strip.
   *  Utilisé dans la methode match.
   * @param v_func Un vecteur de Func
   * @param v_content Un vecteur de contenu
   * @param filename Nom du fichier
   */
  virtual void				getDataFromConcatStrip(VectorFunc& v_func, VectorString& v_content, const std::string& filename);

  /**
   * Cette methode permet de recuperer les scopes present dans le fichier concat_strip.
   * @param concat_strip Fichier d'entrée (.concat)
   * @param v_func Un vecteur de Func
   * @param v_content Un vecteur de contenu
   * @param path Le chemin
   * @param funcName Le nom de la fonction
   */
  virtual void				createFunc(std::ifstream & concat_strip,
						   VectorFunc &v_func,
						   VectorString &v_content,
						   const std::string &path,
						   const std::string &funcName);

  /**
   * Cette methode compare ligne par ligne et renvoie le nombre de ligne identique.
   * @param vLog1 Vecteur 1
   * @param vLog2 Vecteur 2
   * @return Le nombre de lignes identiques
   */
  virtual int			        matchLineByLine(const VectorString& vLog1, const VectorString& vLog2);

  /**
   * Cette methode calcule le pourcentage de correspondance a partir du nombre de ligne identique
   * @param match Match
   * @param count (définit pendant la fonction) Addition du nombre de lignes qui match
   * @return False si il a été impossible de récupérer le code source de la fonction
   */
  virtual bool				calcPercentMatch(Match&, int&);

  /**
   * Cette methode calcule un pourcentage de correspondance propre a un couple de fonction.
   * @param v_funcLog1 Un vecteur de Func
   * @param v_funcLog2 Un second vecteur de Func
   * @param v_match Un vecteur de Match
   * @param count Addition du nombre de lignes qui match
   */
  virtual void				matchFuncByFunc(VectorFunc&, VectorFunc&, std::vector<Match>&, int&);

  /**
   * Methode permettant de recuperer le code source d'une fonction.
   * @param name Nom de la fonction
   * @param path Chemin vers le fichier
   * @param func Func à remplir
   * @return True si le fichier source a été trouvé
   */
  virtual bool				getFuncFromSrcsFile(const std::string&, const std::string&, Func&);

  /**
   ** Fonction (pas compris l'utilité)
   ** @param totalScope nombre de scopes
   ** @param line Ligne vérifiée
   */
  virtual void				checkString(int&, std::string);

  /**
   * Methode permettant de charger un scope au complet dans un objet de type Func
   * @param in Stream d'entrée
   * @param line Contenu d'une fonction
   * @param scope Scope
   * @param func Func remplit
   * @return True si la regex de fin est matchée et que le nombre de scopes vaut 0
   */
  virtual bool				loadScope(std::ifstream&, std::string&, Scope&, Func&);

  /**
   * Methode permettant d'ignorer un scope au complet
   * @param in Stream d'entrée
   * @param line Ligne
   * @param scope Scope
   * @return True si la regex de fin est matchée et que le nombre de scopes vaut 0
   */
  virtual bool				skipScope(std::ifstream&, std::string&, Scope&);

  /**
   * Methode appele dans la methode match apres la recuperation des données de chaque eleves.
   * Elle permet un traitement sur les données des eleves avant l'etape de matching.
   * Par defaut cette methode ne fait rien.
   * @param v_func Un vecteur de Func
   * @param v_string ...
   */
  virtual void				workOnCheater(VectorFunc &v_func, VectorString &v_string);
};

/**
 * Fonction permettant d'isoler le nom du fichier a partir tu path complet.
 * @param path Chemin vers le fichier
 * @return Le nom du fichier
 */
std::string					getFileName(const std::string &path);


#endif
