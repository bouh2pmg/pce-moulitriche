
#ifndef		__CLANGUAGE_HH__
#define		__CLANGUAGE_HH__

#include	"ALanguage.hh"

class		C_Language : public ALanguage
{
public:
  C_Language();
  virtual ~C_Language() {}

public:
  virtual std::string			getCompilationOptions(std::map<std::string, ListString> &);
};

#endif
