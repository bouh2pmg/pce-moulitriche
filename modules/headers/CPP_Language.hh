
#ifndef		__CPPLANGUAGE_HH__
#define		__CPPLANGUAGE_HH__

#include	"ALanguage.hh"

class		CPP_Language : public ALanguage
{
public:
  CPP_Language();
  virtual ~CPP_Language() {}

public:
  virtual std::string			getCompilationOptions(std::map<std::string, ListString> &);
  virtual bool	       			getFuncFromSrcsFile(const std::string&, const std::string&, Func&);
  virtual void				workOnCheater(VectorFunc&, VectorString&);
  static void				nameFunc(Scope&, std::string&, std::string&);
};

#endif
