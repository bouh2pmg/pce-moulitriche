//
// Perl_Language.hh for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec 20 15:25:42 2013 geoffrey bauduin
// Last update Fri Dec 20 15:30:19 2013 geoffrey bauduin
//

#ifndef			__PERL_LANGUAGE_HH__
# define		__PERL_LANGUAGE_HH__

#include		<iostream>
#include		<string>
#include		<vector>
#include		"ILanguage.hh"

/**
 ** Module Perl
 ** Implémente #ILanguage
 */
class			Perl_Language : public ILanguage
{
public:
  Perl_Language();
  virtual ~Perl_Language() {}

public:
  /**
   ** Clean la release
   ** @param file Fichier à supprimer
   ** @return True si le fichier a été supprimé
   */
  bool					cleanRelease(const std::string &);

  /**
   ** Renvoies les tokens d'un fichier ayant l'extention .gram
   ** @param release La release sur laquelle on travaille
   ** @return Les tokens du fichier stockés dans un VectorToken
   */
  std::vector<Token>			getCache(Release *);

  /**
   ** Retournes l'identificateur des fichier du langage
   ** @return Paire de VectorString nécessaire à l'identification
   */
  std::pair<std::vector<std::string>,
	    std::vector<std::string> >	&identifier();

  /**
   ** Parser le fichier (avec l'extension .concat, créé via ILanguage::preprocess) en utilisant les regex (libRE2)
   ** @param release La Release
   ** @return Les tokens stockés dans un VectorToken
   */
  std::vector<Token>			lexer(Release *);

  /**
   ** Strip le fichier avec l'extension .concat pour matcher les lignes de ces fichiers entre les 2 cheaters
   ** @param cheaters Les cheaters
   ** @return Vector de Match
   */
  void					match(const Cheaters &);

  /**
   ** "Compile" les fichiers sources puis crée un fichier .concat des sources "compilées"
   ** @param release La Release
   */
  void					preprocess(Release *);

  /**
   ** Retournes le nom du module
   ** @return Le nom du module
   */
  const std::string			&getLanguageName() const;

private:
  std::string				_name; /**< Nom du module */
  std::pair<std::vector<std::string>,
	    std::vector<std::string> >	_identifier; /**<Identifiants des fichiers du langage */
};

#endif
