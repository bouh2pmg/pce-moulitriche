//
// Skeleton.hh for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec 20 15:08:43 2013 geoffrey bauduin
// Last update Fri Dec 20 15:27:39 2013 geoffrey bauduin
//

#ifndef				__SKELETON_HH__
# define			__SKELETON_HH__

#include			"ILanguage.hh"

class				Skeleton : public ILanguage
{
public:
  Skeleton();

  ~Skeleton();

public:
  /**
   ** Clean la release
   ** @param file Fichier à supprimer
   ** @return True si le fichier a été supprimé
   */
  bool					cleanRelease(const std::string &file);

  /**
   ** Renvoies les tokens d'un fichier ayant l'extention .gram
   ** @param release La release sur laquelle on travaille
   ** @param path Le chemin du fichier
   ** @return Les tokens du fichier stockés dans un VectorToken
   */
  const VectorToken			getCache(Release *release, const std::string &path);

  /**
   ** Retournes l'identificateur des fichier du langage
   ** @return Paire de VectorString nécessaire à l'identification
   */
  std::pair<VectorString, VectorString>	&identifier();

  /**
   ** "Compile" les fichiers sources puis crée un fichier .concat des sources "compilées"
   ** @param release La Release
   ** @param path Le chemin
   */
  void					preprocess(Release *release, const std::string &path);


  /**
   ** Parser le fichier (avec l'extension .concat, créé via ILanguage::preprocess) en utilisant les regex (libRE2)
   ** @param release La Release
   ** @param path Le chemin
   ** @return Les tokens stockés dans un VectorToken
   */
  const VectorToken			lexer(Release *release, const std::string &path);

  /**
   ** Strip le fichier avec l'extension .concat pour matcher les lignes de ces fichiers entre les 2 cheaters
   ** @param cheaters Les cheaters
   ** @param path Le chemin
   ** @return Vector de Match
   */
  const std::vector<Match>		match(Cheaters *cheaters, const std::string &path);

  /**
   ** Retournes le nom du module
   ** @return Le nom du module
   */
  const std::string			&getLanguageName() const;

private:
  const std::string			&_name; /**< Nom du module */

  std::pair<VectorString, VectorString>	_identifier; /**< Identificateur des fichiers du langage */
};

#endif
