/*
** Python_Language.hh for moulitriche in /home/garner_p/Workplace/AER/trichounette/modules/headers
**
** Made by Philip Garnero
** Login   <garner_p@epitech.net>
**
** Started on  Fri Jul 18 18:35:32 2014 Philip Garnero
** Last update Sat Feb 28 05:19:25 2015 Philip Garnero
*/

#ifndef PYTHON_LANGUAGE_H_
# define PYTHON_LANGUAGE_H_

#include		"ILanguage.hh"

class Python_Language : public ILanguage
{
  /**
   * < Nom du language
   */
  std::string				_name;
  /**
   * < Liste des regex permettant de recuperer les fichiers a supprimer
   */
  std::list<std::string>        _releaseToClean;

  /**
   * < Liste des nodes importants a considerer dans l'ast python
   */
  std::list<std::string>				_importantNodes;

  /**
   * < Permet la detection de language : le premier parametre est un tableau d'extension, le second un tableau de shebang
   */
  std::pair<std::vector<std::string>, std::vector<std::string> >	_identifier;

public:
	Python_Language();
	~Python_Language() {}

  /**
   ** Clean la release
   ** @param file Fichier à supprimer
   ** @return True si le fichier a été supprimé
   */
  bool					cleanRelease(const std::string &);

  /**
   ** Renvoies les tokens d'un fichier ayant l'extention .gram
   ** @param release La release sur laquelle on travaille
   ** @return Les tokens du fichier stockés dans un std::vector<Token>
   */
std::vector<Token>       getCache(Release *release, const std::string &path);

  /**
   ** Retournes l'identificateur des fichier du langage
   ** @return Paire de std::vector<std::string> nécessaire à l'identification
   */
  std::pair<std::vector<std::string>,
	    std::vector<std::string> >	&identifier();

  /**
   ** Parser le fichier (avec l'extension .concat, créé via ILanguage::preprocess) en utilisant les regex (libRE2)
   ** @param release La Release
   ** @return Les tokens stockés dans un std::vector<Token>
   */
  std::vector<Token>			lexer(Release *, const std::string &);

  /**
   ** Strip le fichier avec l'extension .concat pour matcher les lignes de ces fichiers entre les 2 cheaters
   ** @param cheaters Les cheaters
   ** @return Vector de Match
   */
  std::vector<Match>			match(Cheaters *, const std::string &);

  /**
   ** "Compile" les fichiers sources puis crée un fichier .concat des sources "compilées"
   ** @param release La Release
   */
  void					preprocess(Release *, const std::string &);

  /**
   ** Retournes le nom du module
   ** @return Le nom du module
   */
  const std::string			&getLanguageName() const;

  void trimLine(std::string &);
  std::string getTrimmedLine(std::string);
  void pushGram(std::string &, std::vector<Token> &, std::ofstream &);
  int  detectIndentationType(std::ifstream &);
  int  getIndentLevel(const std::string &, int);
  std::vector<Token>   genGrams(Release *, const std::string &, std::ofstream &);
  std::map<int, std::string> getFuncNames(Release *, const std::string &);
  void  createFunc(Release *, std::vector<Func> &, std::ifstream &, int &, std::map<int, std::string> &, const std::string &, int, const std::string &);
  std::vector<Func> recupFuncs(Release *, const std::string &);
  bool  getFuncFromSrcsFile(const std::string &, const std::string &, Func &);
  int   matchLineByLine(const std::vector<std::string> &, const std::vector<std::string> &);
  bool  calcPercentMatch(Match &, int &);
  void  matchFuncs(std::vector<Func> &, std::vector<Func> &, std::vector<Match> &, int &);
  std::pair<std::vector<std::string>, std::vector<std::string> >  getAllLines(Release *, Release *, const std::string &);
};

#endif /* PYTHON_LANGUAGE_H_ */