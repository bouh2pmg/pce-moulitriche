//
// CPP_Language.cpp for moulitriche in /home/neut_g/PROJETS/Tek3/moulitriche/git/trichounette
// 
// Made by Grégory Neut
// Login   <neut_g@epitech.net>
// 
// Started on Wed Jan  8 17:50:39 2014 Grégory Neut
// Last update Tue Nov 25 15:54:51 2014 Philip Garnero
//

/*
** Includes externes
*/
#include	<sstream>

/*
** Includes internes
*/
#include	"CPP_Language.hh"
#include	"Logger.hh"
#include	"Config.hh"

CPP_Language::CPP_Language() : ALanguage("CPP", "c++", Config::getSingleton()->getModuleFlag("CPP"))
{
  VectorString	ext;
  VectorString	shebang;

  _filesTypes["SRC"] = "(.*\\.(cpp)$)";
  _filesTypes["HDS"] = "(.*\\.(h|hh|hpp)$)";
  _releaseToClean.push_back(".*(Makefile|a.out)$");
  _releaseToClean.push_back(".*\\.(o)$");
  _releaseToClean.push_back(".*\\.c.\\d+t\\.optimized");
  _fileTypeToCompile = "SRC";

  _vScopes.push_back(Scope("^(?:(?:[A-Za-z0-9:&*_ ]|<[A-Za-z0-9:&*_, ]*>)+ )?((?:[A-Za-z0-9:&*_~+-=<>!\\[\\]]|<[A-Za-z0-9:&*_, ]*>)+)\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)[a-z ]*\\s*\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)$", "^\\}$", &CPP_Language::nameFunc));

  _skipScopes.push_back(Scope("^(?:(?:[A-Za-z0-9:&*_ ]|<[A-Za-z0-9:&*_, ]*>)+ )?__gnu_cxx::((?:[A-Za-z0-9:&*_~+-=<>!\\[\\]]|<[A-Za-z0-9:&*_, ]*>)+)\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)[a-z ]*\\s*\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)$", "^\\}$"));
  _skipScopes.push_back(Scope("^(?:(?:[A-Za-z0-9:&*_ ]|<[A-Za-z0-9:&*_, ]*>)+ )?std::((?:[A-Za-z0-9:&*_~+-=<>!\\[\\]]|<[A-Za-z0-9:&*_, ]*>)+)\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)[a-z ]*\\s*\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)$", "^\\}$"));
  _skipScopes.push_back(Scope("^(?:(?:[A-Za-z0-9:&*_ ]|<[A-Za-z0-9:&*_, ]*>)+ )?sf::((?:[A-Za-z0-9:&*_~+-=<>!\\[\\]]|<[A-Za-z0-9:&*_, ]*>)+)\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)[a-z ]*\\s*\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)$", "^\\}$"));
  _skipScopes.push_back(Scope("^(?:(?:[A-Za-z0-9:&*_ ]|<[A-Za-z0-9:&*_, ]*>)+ )?_Z41__static_initialization_and_destruction_((?:[A-Za-z0-9:&*_~+-=<>!\\[\\]]|<[A-Za-z0-9:&*_, ]*>)+)\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)[a-z ]*\\s*\\((?:[A-Za-z0-9:;,&*_=. ]|<[A-Za-z0-9:&*_, ]*>)*\\)$", "^\\}$"));

  _vCodeScopes.push_back(Scope("((?:[A-Za-z0-9:&*_~+-=<>!\\[\\]]|<[A-Za-z0-9:&*_, ]*>)+)\\s*\\(.*", "\\}", &CPP_Language::nameFunc));

  _vTokens.push_back(Tokens("if\\s+\\(.*\\)$",                     "01_COND_",    &ALanguage::cond,	4));
  _vTokens.push_back(Tokens("^\\s+goto\\s+<bb\\s+\\d+>;$",         "02_GOTO_",    NULL));
  _vTokens.push_back(Tokens("\\Wwrite\\s+\\(.+\\)",                "03_WRITE_",   &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wopen\\s+\\(.+\\)",                 "04_OPEN_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wclose\\s+\\(.+\\)",                "05_CLOSE_",   &ALanguage::func));
  _vTokens.push_back(Tokens("\\Ws?brk\\s+\\(.+\\)",                "06_BRK_",     &ALanguage::func));
  // _vTokens.push_back(Tokens("\\Wmalloc\\s+\\(.+\\)",               "07_MALLOC_",  &C_Language::func));
  // _vTokens.push_back(Tokens("\\Wfree\\s+\\(.+\\)",                 "08_FREE_",    &C_Language::func));
  // _vTokens.push_back(Tokens("\\W[fl]?stat\\s+\\(.+\\)",            "09_STAT_",    &C_Language::func));
  _vTokens.push_back(Tokens("\\W(u|nano)?sleep\\s+\\(.+\\)",       "10_SLEEP_",   &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wtime\\s+\\(.+\\)",                 "11_TIME_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\Waccept\\s+\\(.+\\)",               "12_ACCEPT_",  &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wbind\\s+\\(.+\\)",                 "13_BIND_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wconnect\\s+\\(.+\\)",              "14_CONNECT_", &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wselect\\s+\\(.+\\)",               "15_SELECT_",  &ALanguage::func));
  _vTokens.push_back(Tokens("\\W(?:[A-Za-z0-9:&*_~+-=<>!\\[\\]]|<[A-Za-z0-9:&*_, ]*>)+\\s+\\(.*\\)",                 "16_FUNC_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\WPHI\\W<.*>",                       "17_PHI_",     NULL));
  _vTokens.push_back(Tokens("\\Wreturn\\s*.*;$",                   "18_RET_",     NULL));
  // _vTokens.push_back(Tokens("(.+)\\s+=\\s+\\(.+\\)\\s+.+;$",       "19_CAST_",    &C_Language::cast));
  _vTokens.push_back(Tokens(".+\\s+=\\s+.+(\\s+[-+*/%]\\s+.+)+;$", "20_OPE_",     &ALanguage::ope));
  _vTokens.push_back(Tokens("MEM\\[.+\\]",                         "21_MEM_",     &ALanguage::mem));
  _vTokens.push_back(Tokens(".+\\s+=\\s+.+;$",                     "22_ASSIGN_",  NULL));

  _skip.push_back(SkipLines("^$"));
  _skip.push_back(SkipLines("^Removing"));
  _skip.push_back(SkipLines("^Merging"));
  _skip.push_back(SkipLines("^;;"));
  _skip.push_back(SkipLines("^<"));
  _skip.push_back(SkipLines("^Invalid sum"));
  _skip.push_back(SkipLines("^\\s+(_Bool|size_t|ssize_t|float|double|int|struct|char)\\s(.*);$"));
  _skip.push_back(SkipLines("(^[ \t]*<bb\\s+\\d+>:$)"));

  _vSeparator.push_back("(^[ \t]*<bb\\s+\\d+>:$)");

  ext.push_back(".*\\.(hh|hpp|cpp)$");
  _identifier = std::make_pair(ext, shebang);
}

/*
** Cette fonction renvoi les options de compilations. Le but est de pouvoir compiler tout les fichiers .cpp
*/
std::string				CPP_Language::getCompilationOptions(std::map<std::string, ListString> &files)
{
  VectorString				fileToAdd;
  std::string				path;
  VectorString::const_iterator		ita;

  for (ListString::const_iterator it = files["HDS"].begin();
       it != files["HDS"].end(); ++it)
    {
      path = realpath(it->substr(0, it->find_last_of("/")).c_str(), NULL);
      for (ita = fileToAdd.begin();
	   ita != fileToAdd.end(); ++ita)
	if (*ita == path)
	  break;
      if (ita == fileToAdd.end())
	fileToAdd.push_back(path);
    }
  path = "";
  for (ita = fileToAdd.begin();
       ita != fileToAdd.end(); ++ita)
    path += "-I" + *ita + " ";
  return ("-c -w -O2 -fno-inline -fdump-tree-optimized " + path);
}

bool					CPP_Language::getFuncFromSrcsFile(const std::string &name, const std::string &path, Func &func)
{
  std::string				tmp1 = "", tmp2 = "";
  StaticFuncPtr				fptr;
  std::ifstream				in;
  std::string				line;
  std::string				tmpname;
  std::list<std::string>		namespaces;

  func.setName(name);
  in.open(path.c_str());
  if (in.is_open() == true)
    {
      while (in.eof() == false)
        {
	  std::getline(in, line);
	  if (RE2::PartialMatch(line, "^[ \t]*namespace[ \t]+(.*)[ \t]*$", &tmpname) == true)
	    namespaces.push_back(tmpname);
	  else if (RE2::PartialMatch(line, "^[ \t]*}[ \t]*$") == true && namespaces.size() > 0)
	    namespaces.pop_back();
	  else
	    {
	      for (std::vector<Scope>::iterator it = this->_vCodeScopes.begin();
		   it != this->_vCodeScopes.end(); ++it)
		{
		  if (RE2::PartialMatch(line, (*it)._regexBegin, &tmpname) == true)
		    {
		      if ((fptr = (*it)._func) != NULL)
			(*fptr)(*it, line, tmpname);
		      else if (tmpname == "")
			tmpname = "unknown";
		      for (std::list<std::string>::reverse_iterator itn = namespaces.rbegin(); itn != namespaces.rend(); ++itn)
			tmpname = *itn + "::" + tmpname;
		      if (name == tmpname)
			{
			  loadScope(in, line, *it, func);
			  CPP_Language::nameFunc(this->_vCodeScopes.front(), tmp1, tmp2);
			  if (std::count(func.getFinalContent().begin(), func.getFinalContent().end(), '\n') < 9)
			    return (false);
			  return (true);
			}
		      skipScope(in, line, *it);
		      break;
		    }
		}
	    }
	}
      in.close();
    }
  else
    Logger::getSingleton()->log("Unable to open '" + path + "'", ERROR);
  CPP_Language::nameFunc(this->_vCodeScopes.front(), tmp1, tmp2);
  return (false);
}

void				CPP_Language::nameFunc(Scope& scope, std::string& line, std::string& name)
{
  std::string				tmpname;
  std::ostringstream			finalstring;
  static std::map<std::string, int>	tab;

  if (line != "")
    {
      RE2::PartialMatch(line, scope._regexBegin, &tmpname);
      finalstring << tmpname << "@" << tab[tmpname];
      tab[tmpname] += 1;
      name = finalstring.str();
    }
  else
    tab.clear();
}

void				CPP_Language::workOnCheater(VectorFunc&, VectorString&)
{
  std::string			tmp1 = "", tmp2 = "";

  CPP_Language::nameFunc(this->_vCodeScopes.front(), tmp1, tmp2);
}

extern "C"
{
  ILanguage			*create_module()
  {
    return (new CPP_Language);
  };
}
