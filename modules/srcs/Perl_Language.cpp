//
// Perl_Language.cpp for  in /home/geoffrey/Projects/trichounette
// 
// Made by geoffrey bauduin
// Login   <baudui_g@epitech.net>
// 
// Started on  Fri Dec 20 15:26:09 2013 geoffrey bauduin
// Last update Fri Dec 20 15:29:02 2013 geoffrey bauduin
//

#include			"../headers/Perl_Language.hh"

Perl_Language::Perl_Language() : _name("Perl")
{
  std::vector<std::string>	ext;
  std::vector<std::string>	shebang;

  shebang.push_back("#!/usr/bin/perl");
  _identifier = std::make_pair(ext, shebang);
}

bool					Perl_Language::cleanRelease(const std::string &file)
{
  (void) file;
  return (false);
}

std::vector<Token>			Perl_Language::getCache(Release *release)
{
  (void) release;
  return (std::vector<Token>());
}

void					Perl_Language::preprocess(Release *release)
{
  (void) release;
}

std::pair<std::vector<std::string>,
	  std::vector<std::string> >	&Perl_Language::identifier()
{
  return (_identifier);
}

std::vector<Token>			Perl_Language::lexer(Release *release)
{
  (void) release;
  return (std::vector<Token>());
}

void					Perl_Language::match(const Cheaters &cheaters)
{
  (void) cheaters;
}

const std::string			&Perl_Language::getLanguageName() const
{
  return (_name);
}

extern "C"
{
  ILanguage			*create_module()
  {
    return (new Perl_Language);
  };
}
