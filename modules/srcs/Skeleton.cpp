#include			"Skeleton.hh"

Skeleton() : _name("Skeleton")
{

}

~Skeleton()
{

}

bool					Skeleton::cleanRelease(const std::string &/* fileNAme */)
{

  return (false);
}

VectorToken				Skeleton::getCache(Release */* release */)
{

  return (VectorToken());
}

std::pair<VectorString, VectorString>	&Skeleton::identifier()
{

  return (_identifier);
}

void					Skeleton::preprocess(Release */* release */)
{

}

VectorToken				Skeleton::lexer(Release */* release */)
{

  return (VectorToken());
}

std::vector<MouliTriche::Match>		Skeleton::match(Cheaters */* cheaters */)
{

  return (_v_match);
}

const std::string			&Skeleton::getLanguageName() const
{
  return (_name);
}
