/*
** Python_Language.cpp for moulitriche in /home/garner_p/Workplace/AER/trichounette/modules/srcs
**
** Made by Philip Garnero
** Login   <garner_p@epitech.net>
**
** Started on  Fri Jul 18 18:35:44 2014 Philip Garnero
// Last update Wed Mar 25 17:39:24 2015 Philip Garnero
*/

#include      <re2/re2.h>
#include      <fstream>
#include      <boost/algorithm/string.hpp>
#include      <algorithm>
#include      "Instance.hh"
#include      "Logger.hh"
#include      "Date.hh"
#include      "Core.hh"
#include      "Option.hpp"
#include      "Convert.hh"
#include      "Python_Language.hh"

Python_Language::Python_Language() : _name("Python")
{
  std::vector<std::string>	ext;
  std::vector<std::string>	shebang;

  shebang.push_back("#!.*python.*");
  ext.push_back(".*\\.py$");
  this->_identifier = std::make_pair(ext, shebang);
  this->_releaseToClean.push_back(".*\\.(pyc)$");

//  this->_importantNodes.push_back("Assign");
//  this->_importantNodes.push_back("Call");
  this->_importantNodes.push_back("Assert");
  this->_importantNodes.push_back("Break");
  this->_importantNodes.push_back("ClassDef");
  this->_importantNodes.push_back("Continue");
  this->_importantNodes.push_back("Delete");
  this->_importantNodes.push_back("Expr");
  this->_importantNodes.push_back("FunctionDef");
  this->_importantNodes.push_back("If");
  this->_importantNodes.push_back("Import");
  this->_importantNodes.push_back("ImportFrom");
  this->_importantNodes.push_back("ListComp");
  this->_importantNodes.push_back("GeneratorExp");
  this->_importantNodes.push_back("Print");
  this->_importantNodes.push_back("Yield");
  this->_importantNodes.push_back("Return");
  this->_importantNodes.push_back("Lambda");
  this->_importantNodes.push_back("TryFinally");
  this->_importantNodes.push_back("TryExcept");
  this->_importantNodes.push_back("Raise");
  this->_importantNodes.push_back("While");
  this->_importantNodes.push_back("With");
}

bool					Python_Language::cleanRelease(const std::string &file)
{
  for (ListString::const_iterator it = this->_releaseToClean.begin();
      it != this->_releaseToClean.end(); ++it)
    {
      if (RE2::PartialMatch(file, (*it)) == true)
	{
	  system(std::string("rm " + file).c_str());
	  return (true);
	}
    }
  return (false);
}

std::vector<Token>       Python_Language::getCache(Release *release, const std::string &path)
{
  std::vector<Token>				token;
  std::ifstream				in;
  std::string				line;

  in.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".gram").c_str());
  if (in.is_open() == true)
    {
      while (in.eof() == false)
  	{
  	  std::getline(in, line);
	  if (line != "")
	    token.push_back(line);
  	}
    }
  return (token);
}

void					Python_Language::preprocess(Release *release, const std::string &path)
{
  std::vector<std::string>  pythonFiles;
  std::vector<std::string>::iterator  it;
  std::string   execution;
  std::string   files;

  pythonFiles = release->getTree().getTreeByKey(this->_name);
  files = "";
  for (it = pythonFiles.begin(); it < pythonFiles.end(); ++it)
  {
    files += " ";
    files += *it;
    Logger::getSingleton()->log("Getting ast from '" + *it + "'", WARNING);
  }
  if (files != "")
    {
      execution = "python " + CURRENT_DIR + "/" + MOD_DIR + "/python-ast.py" + " --detailed --out " + std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo())) + ".concat" + files;
      system(execution.c_str());
      execution = "python " + CURRENT_DIR + "/" + MOD_DIR + "/python-ast.py" + " --out " + std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo())) + ".concat_strip" + files;
      system(execution.c_str());
    }
}

std::pair<std::vector<std::string>,
	  std::vector<std::string> >	&Python_Language::identifier()
{
  return (this->_identifier);
}

/**
** Le but de la fonction est d'enlever chaque espace superflu contenu dans une chaine de type string
**
** Exemple :
**         avant   - "  je suis  une  phrase  "
**         après   - "je suis une phrase"
*/
void          Python_Language::trimLine(std::string &line)
{
  int         idx = 0;

  if (!line.empty())
  {
    while (line.find(" ") == 0)
      line.erase(0, 1);
    while ((idx = line.find(" ", idx + 1)) != -1)
      if (line[idx + 1] == ' ')
        line.erase(idx, 1);
    while (line.rfind(" ") == line.size() - 1)
      line.erase(line.size() - 1, 1);
  }
}

std::string Python_Language::getTrimmedLine(std::string line)
{
  this->trimLine(line);
  return (line);
}

/**
** On ajoute le gram à la liste de token et on réinitialise le gram
*/
void          Python_Language::pushGram(std::string &gram, std::vector<Token> &token, std::ofstream &out)
{
  if (gram != "")
    {
      token.push_back(gram);
      if (out != NULL)
  out << gram << std::endl;
      gram = "";
    }
}

int       Python_Language::detectIndentationType(std::ifstream &file)
{
  std::string  line;
  unsigned int i;
  int          counter;
  long         beg;

  counter = 0;
  if (file.is_open())
  {
    beg = file.tellg();
    file.seekg(0);
    while (file.eof() == false)
    {
      std::getline(file, line);
      if (RE2::PartialMatch(line, "\\S") == true)
      {
        i = 0;
        while (i < line.size())
        {
          if (line[i] == ' ')
            counter += 1;
          else if (line[i] == '\t')
            counter += 8;
          else
            break;
          i++;
        }
      }
      if (counter > 0)
        break;
    }
    file.seekg(beg);
  }
  return (counter);
}

/**
** retourne le niveau d'indentation pour une ligne
*/
int       Python_Language::getIndentLevel(const std::string &line, int indent)
{
  unsigned int     i;
  int     counter;

  i = 0;
  counter = 0;
  while (i < line.size())
  {
    if (line[i] == ' ')
      counter += 1;
    else if (line[i] == '\t')
      counter += 8;
    else
      break;
    i++;
  }
  return (counter / indent);
}

/**
** Parcours le concat_strip pour créer des grams.
** Tente de trouver des blocs de code a partir de l'ast
*/
std::vector<Token>         Python_Language::genGrams(Release *release, const std::string &path, std::ofstream &out)
{
  std::vector<std::string> blocks;
  std::map<int, std::string> levels;
  std::vector<Token>       grams;
  std::ifstream     in;
  std::string       line;
  int               oldlevel;
  int               difflevel;
  int               level;
  int               indent;
  long              file_offset;
  int               block_size;

  in.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".concat_strip").c_str());
  oldlevel = 0;
  if (in.is_open() == true)
    {
      indent = this->detectIndentationType(in);
      while (in.eof() == false && in.fail() == false)
      {
        std::getline(in, line);
        file_offset = in.tellg();
        level = this->getIndentLevel(line, indent);
        this->trimLine(line);
        oldlevel = level;
        difflevel = level;
        block_size = 0;
        if (std::find(this->_importantNodes.begin(), this->_importantNodes.end(), line) != this->_importantNodes.end())
        {
          while (in.eof() == false && in.fail() == false)
          {
            if (level > 0) // pour ignorer le nom du fichier
            {
              if (level < oldlevel)
                {
                  levels[oldlevel] += "|";
                  break;
                }
              if (std::find(this->_importantNodes.begin(), this->_importantNodes.end(), line) != this->_importantNodes.end())
              {
                if (block_size > 5 || level - oldlevel > 2)
                {
                  levels[oldlevel] += "|";
                  break;
                }
                if (levels.count(oldlevel) == 0)
                {
                  levels[oldlevel] = "";
                  block_size++;
                }
                else if (block_size)
                {
                  if (difflevel == level)
                  {
                    block_size++;
                    levels[oldlevel] += ";";
                  }
                  else if (difflevel < level)
                    levels[oldlevel] += ">";
                  else if (difflevel > level)
                    levels[oldlevel] += "<";
                }
                else
                    block_size++;
                levels[oldlevel] += line;
                difflevel = level;
              }
            }
            std::getline(in, line);
            level = this->getIndentLevel(line, indent);
            this->trimLine(line);
          }
          in.clear();
          in.seekg(file_offset);
        }
      }
      for (std::map<int, std::string>::iterator iter = levels.begin(); iter != levels.end(); ++iter) {
        boost::split(blocks, iter->second, boost::is_any_of("|"));
        for (std::vector<std::string>::iterator it = blocks.begin(); it != blocks.end(); ++it) {
          if (std::count(it->begin(), it->end(), ';') > 0)
            this->pushGram(*it, grams, out);
        }
        blocks.clear();
      }
      in.close();
    }
  else
    {
      Logger::getSingleton()->log("impossible to open '" + path + release->getLogin() + "-"
          + stringToInt(release->getPromo()) + ".concat_strip'", FORCE_ERROR);
    }
  return (grams);
}

std::vector<Token>      Python_Language::lexer(Release *release, const std::string &path)
{
  std::vector<Token>         token;
  std::ofstream         gram;

  if (release->getTree().getTreeByKey(_name).size() == 0)
    return (token);
  gram.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".gram").c_str());
  if (gram.is_open() == false) {
    Logger::getSingleton()->log("impossible to open '" + path + release->getLogin() +
        "-" + stringToInt(release->getPromo()) + ".gram'", FORCE_ERROR);
    return (token);
  }
  token = this->genGrams(release, path, gram);
  gram.close();
  return (token);
}

/*
** Retrouve un nom de fonction a partir de sa position dans l'ast
*/
std::map<int, std::string> Python_Language::getFuncNames(Release *release, const std::string &path)
{
  std::ifstream     in;
  std::map<int, std::string> func_names;
  std::string       cur_class;
  int               class_level;
  std::string       line1;
  std::string       line2;
  std::string       trimmed;
  int               cur_num;
  int               indent;

  cur_num = 0;
  cur_class = "";
  class_level = 0;
  in.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".concat").c_str());
  if (in.is_open() == true)
  {
    indent = this->detectIndentationType(in);
    std::getline(in, line1);
    std::getline(in, line2);
    while (in.eof() == false)
    {
      if (this->getIndentLevel(line1, indent) <= class_level)
      {
        cur_class = "";
        class_level = 0;
      }
      if (RE2::PartialMatch(line1, "ClassDef"))
      {
        class_level = this->getIndentLevel(line1, indent);
        cur_class = line2;
        trimmed = this->getTrimmedLine(std::string(cur_class));
        cur_class = trimmed.substr(1, trimmed.size() - 3);
      }
      if (RE2::PartialMatch(line2, "Module") || RE2::PartialMatch(line1, "FunctionDef"))
        cur_num++;
      if (RE2::PartialMatch(line2, "Module"))
        func_names[cur_num] = ("Module " + line1.substr(line1.find_last_of('/') + 1, line1.size() - line1.find_last_of('/')));
      else if (RE2::PartialMatch(line1, "FunctionDef"))
      {
        trimmed = this->getTrimmedLine(std::string(line2));
        func_names[cur_num] = (cur_class + (cur_class.empty() ? "" : ".") + trimmed.substr(1, trimmed.size() - 3));
      }
      line1 = line2;
      std::getline(in, line2);
    }
    in.close();
  }
  return (func_names);
}

/**
**
*/
void          Python_Language::createFunc(Release *release,
  std::vector<Func> &funcs,
  std::ifstream &in,
  int &function_num,
  std::map<int, std::string> &func_names,
  const std::string &src_file,
  int level,
  const std::string &path)
{
  Func        new_func;
  std::string line;
  int         indent;
  int         new_level;
  long   beg;

  new_func.setPath(src_file);
  new_func.setName(func_names[function_num]);
  indent = this->detectIndentationType(in);
  while (in.eof() == false)
  {
    beg = in.tellg();
    std::getline(in, line);
    new_level = this->getIndentLevel(line, indent);
    if (new_level <= level)
    {
      in.clear();
      in.seekg(beg);
      break;
    }
    else if (RE2::PartialMatch(line, "Module") || RE2::PartialMatch(line, "FunctionDef"))
    {
      function_num++;
      this->createFunc(release, funcs, in, function_num, func_names, src_file, new_level, path);
    }
    else
      new_func.addContent(line);
  }
  sort(new_func.content().begin(), new_func.content().end(), StaticFunc::compareString);
  funcs.push_back(new_func);
}

std::vector<Func> Python_Language::recupFuncs(Release *release, const std::string &path)
{
  std::vector<Func> funcs;
  std::ifstream in;
  std::string       line;
  int               indent;
  int               level;
  int               function_num;
  std::string       cur_src_file;
  std::map<int, std::string> func_names;

  in.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".concat_strip").c_str());
  function_num = 1;
  if (in.is_open())
  {
    func_names = this->getFuncNames(release, path);
    indent = this->detectIndentationType(in);
    while (in.eof() == false)
    {
      if (!std::getline(in, line))
        break;
      level = this->getIndentLevel(line, indent);
      if (level == 0)
        cur_src_file = line;
      else
      {
        if (RE2::PartialMatch(line, "Module") || RE2::PartialMatch(line, "FunctionDef"))
        {
          this->createFunc(release, funcs, in, function_num, func_names, cur_src_file, level, path);
          function_num++;
        }
      }
    }
    in.close();
  }
  else
    Logger::getSingleton()->log("Unable to open '" + std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".concat_strip") + "'", FORCE_ERROR);
  return (funcs);
}

/**
** récupere le code source de la fonction en question dans le fichier de l'étudiant
** @param name Le nom de la fonction à retrouver
** @param path Le chemin du fichier contenant la fonction
** @param func La classe Func dans laquelle recopier le code source
*/
bool          Python_Language::getFuncFromSrcsFile(const std::string &name,
                       const std::string &path,
                       Func &func)
{
  std::ifstream     in;
  std::string       line;
  std::string       cur_class;
  std::string       func_name;
  int               class_level;
  int               indent;
  int               level;
  int               new_level;
  long         beg;

  class_level = 0;
  cur_class = "";
  func_name = "";
  in.open(path.c_str());
  if (in.is_open() == true)
  {
    indent = this->detectIndentationType(in);
    if (RE2::PartialMatch(name, "Module "))
    {
      while (in.eof() == false)
      {
        std::getline(in, line);
        if (RE2::PartialMatch(line, "^(\\s)?def(\\s)+"))
        {
          func.addFinalContent(line);
          level = this->getIndentLevel(line, indent);
          while (in.eof() == false)
          {
            beg = in.tellg();
            std::getline(in, line);
            if (RE2::PartialMatch(line, "\\S") == true)
            {
              new_level = this->getIndentLevel(line, indent);
              if (new_level <= level)
              {
                in.seekg(beg);
                break;
              }
            }
          }
        }
        else
          func.addFinalContent(line);
      }      
    }
    else
    {
      while (in.eof() == false)
      {
        std::getline(in, line);
        func_name = line.substr(line.rfind(' ', line.rfind('(')) + 1);
        func_name = func_name.substr(0, func_name.find('('));
        if (this->getIndentLevel(line, indent) <= class_level)
        {
          cur_class = "";
          class_level = 0;
        }
        if (RE2::PartialMatch(line, "class(\\s)+"))
        {
          class_level = this->getIndentLevel(line, indent);
          cur_class = line.substr(line.rfind(' ', line.rfind('(')) + 1);
          cur_class = cur_class.substr(0, cur_class.find('('));
        }
        if (RE2::PartialMatch(line, "(\\s)?def(\\s)+")
          && RE2::PartialMatch(name, cur_class)
          && RE2::PartialMatch(name, func_name))
        {
          level = this->getIndentLevel(line, indent);
          func.addFinalContent(line);
          std::getline(in, line);
          while (this->getIndentLevel(line, indent) > level)
          {
            func.addFinalContent(line);
            if (!std::getline(in, line))
              break;
          }
        }
      }
    }
    in.close();
    return (true);
  }
  else
    Logger::getSingleton()->log("Unable to open '" + path + "'", FORCE_ERROR);
  return (false);
}

/**
**
*/
int         Python_Language::matchLineByLine(const std::vector<std::string> &vLog1,
                   const std::vector<std::string> &vLog2)
{
  int         count = 0;
  std::vector<std::string>::const_iterator    itLog1, itLog2;

  itLog1 = vLog1.begin();
  itLog2 = vLog2.begin();
  while (itLog1 != vLog1.end() && itLog2 != vLog2.end())
    {
      if (*itLog1 == *itLog2)
  {
    ++count;
    ++itLog1;
    ++itLog2;
  }
      else if ((*itLog1).compare(*itLog2) < 0)
        ++itLog1;
      else
  ++itLog2;
    }
  return (count);
}

/**
**
*/
bool          Python_Language::calcPercentMatch(Match &match, int &count)
{
  std::vector<std::pair<Func, Func> >::iterator it;
  bool          find = true;

  it = match.func().begin();
  if (it == match.func().end()) {
    return (false);
  }
  if (it->first.getPercent() >= 70 && it->second.getPercent() >= 60)
    {
      count += matchLineByLine((*it).first.getContent(), (*it).second.getContent());
      if (!getFuncFromSrcsFile(it->first.getName(), it->first.getPath(), it->first) ||
    !getFuncFromSrcsFile(it->second.getName(), it->second.getPath(), it->second))
  find = false;
      ++it;
      match.func().erase(it, match.func().end());
      return (find);
    }
  match.func().erase(it, match.func().end());
  return (find);
}

/**
**
*/
void          Python_Language::matchFuncs(std::vector<Func> &v_funcLog1,
                   std::vector<Func> &v_funcLog2,
                   std::vector<Match> &v_match,
                   int &count)
{
  std::vector<Func>::iterator      it1, it2;
  int         tmp_count = 0;
  Match         match;

  for (it1 = v_funcLog1.begin(); it1 != v_funcLog1.end(); ++it1)
  {
    for (it2 = v_funcLog2.begin(); it2 != v_funcLog2.end(); ++it2)
    {
      tmp_count = matchLineByLine((*it1).getContent(), (*it2).getContent());
      (*it1).setNbHit(tmp_count);
      (*it2).setNbHit(tmp_count);
      (*it1).setPercent(((*it1).getContent().size() > 0 ? tmp_count * 100 / (*it1).getContent().size() : 0));
      (*it2).setPercent(((*it2).getContent().size() > 0 ? tmp_count * 100 / (*it2).getContent().size() : 0));
      match.addFunc(std::make_pair(*it1, *it2));
    }
    sort(match.func().begin(), match.func().end(), StaticFunc::greaterMatch);
    if (calcPercentMatch(match, count) && match.func().size() > 0)
      v_match.push_back(match);
    match.func().clear();
  }
}

std::pair<std::vector<std::string>, std::vector<std::string> >    Python_Language::getAllLines(Release *first, Release *second, const std::string &path)
{
    std::ifstream             in;
    std::string               line;
    std::vector<std::string>  file1;
    std::vector<std::string>  file2;

    in.open(std::string(path + first->getLogin() + "-" + stringToInt(first->getPromo()) + ".concat_strip").c_str());
    if (in.is_open())
    {
      while (in.eof() == false)
      {
        std::getline(in, line);
        file1.push_back(line);
      }
      in.close();
    }
    else
      Logger::getSingleton()->log("Unable to open '" + std::string(path + first->getLogin() + "-" + stringToInt(first->getPromo()) + ".concat_strip") + "'", FORCE_ERROR);

    in.open(std::string(path + second->getLogin() + "-" + stringToInt(second->getPromo()) + ".concat_strip").c_str());
    if (in.is_open())
    {
      while (in.eof() == false)
      {
        std::getline(in, line);
        file2.push_back(line);
      }
      in.close();
    }
    else
      Logger::getSingleton()->log("Unable to open '" + std::string(path + second->getLogin() + "-" + stringToInt(second->getPromo()) + ".concat_strip") + "'", FORCE_ERROR);

    sort(file1.begin(), file1.end());
    sort(file2.begin(), file2.end());

    return (std::make_pair(file1, file2));
}

std::vector<Match>					Python_Language::match(Cheaters *cheaters, const std::string &path)
{
  std::vector<Func>         cheaterFuncs1, cheaterFuncs2;
  std::pair<std::vector<std::string>, std::vector<std::string> >   all_lines;;
  std::vector<Match>        matching;
  int                       count;
  std::pair<float, float>   percent;

  count = 0;
  if (Option::getSingleton()->hasOption(Option::HARDVERBOSE)) {
    Logger::getSingleton()->log(cheaters->getCouple().first->getLogin() + " - "
        + cheaters->getCouple().second->getLogin() + " - " + path, NONE);
  }
  cheaterFuncs1 = this->recupFuncs(cheaters->getCouple().first, path);
  cheaterFuncs2 = this->recupFuncs(cheaters->getCouple().second, path);

  all_lines = this->getAllLines(cheaters->getCouple().first, cheaters->getCouple().second, path);
  cheaters->setNbHit(this->matchLineByLine(all_lines.first, all_lines.second));
  this->matchFuncs(cheaterFuncs1, cheaterFuncs2, matching, count);
  percent = std::make_pair(
    static_cast<float>(all_lines.first.size() > 0 ? count * 100 / all_lines.first.size() : 0),
    static_cast<float>(all_lines.second.size() > 0 ? count * 100 / all_lines.second.size() : 0)
  );
  cheaters->setPercent(percent);
  return (matching);
}

const std::string			&Python_Language::getLanguageName() const
{
  return (this->_name);
}

extern "C"
{
  ILanguage			*create_module()
  {
    return (new Python_Language);
  };
}
