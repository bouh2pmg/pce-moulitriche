/*
// ALanguage.cpp for moulitriche in /home/neut_g/PROJETS/Tek3/moulitriche/trichounette/trichounette
//
// Made by Grégory Neut
// Login   <neut_g@epitech.net>
//
// Started on Wed Nov 20 18:09:58 2013 Grégory Neut
// Last update Tue Nov 25 16:51:23 2014 Philip Garnero
*/

#include			<re2/re2.h>
#include			<fstream>
#include			"ALanguage.hh"
#include			"Instance.hh"
#include			"Logger.hh"
#include			"Date.hh"
#include			"Convert.hh"
#include			"Option.hpp"

using namespace			boost::filesystem;

/**
 ** Constructeur de la classe
 ** Initialises la thread pool.
 ** @param name Nom du langage
 ** @param compilator Compilateur
 ** @param compExtent Extension du fichier compilé
 */
ALanguage::ALanguage(const std::string& name,
		     const std::string& comp,
		     const std::string& compExtent) : _name(name),
						      _compilator(comp),
						      _genFileCompExtent(compExtent)
{
  for (int i = 0; i < MAX_THREADS; ++i)
    _thPool[i] = Instance::getThreadInstance();
}

/**
 ** Destructeur de la classe
 ** Détruit toute la thread pool.
 */
ALanguage::~ALanguage()
{
  for (int i = 0; i < MAX_THREADS; ++i)
    delete _thPool[i];
}

/**
 ** Getter sur l'identifier.
 */
std::pair<VectorString, VectorString >	&ALanguage::identifier()
{
  return (_identifier);
}

/**
 ** Effectue un travail sur la ligne pour tokeniser l'appel de fonction
 ** @param line Ligne travaillée (consommé au fur et à mesure)
 ** @param newLine Nouvelle ligne (remplie au fur et à mesure)
 */
void					ALanguage::func(std::string &line, std::string &newLine)
{
  VectorString::iterator		it;
  std::string				tmp;
  int					idx;

  if ((idx = line.find("=")) != -1)
    {
      newLine += "V = ";
      line.erase(0, idx + 1);
    }
  for (it = _localFunc.begin(); it != _localFunc.end() && static_cast<int>(line.find(" " + *it)) == -1; ++it);
  if (it == _localFunc.end())
    newLine += "F(";
  else
    {
      idx = line.find("(");
      newLine += line.substr(0, idx - 1) + "(";
    }
  idx = line.find("(");
  line.erase(0, idx + 1);
  while ((idx = line.find(",")) != -1)
    {
      getParam(line, newLine, idx);
      newLine += ", ";
    }
  if ((idx = line.find(")")) != 0)
    getParam(line, newLine, idx);
  newLine = " " + newLine;
  trimLine(newLine);
  newLine = " " + newLine + ");";
}

/**
 ** Effectue un travail sur la ligne pour tokeniser une condition
 ** @param line Ligne travaillée (consommé au fur et à mesure)
 ** @param newLine Nouvelle ligne (remplie au fur et à mesure)
 */
void					ALanguage::cond(std::string &line, std::string &newLine)
{
  std::string				tmp;
  int					idx;

  newLine += " if (V ";
  RE2::PartialMatch(line, "((!=|==|>=|=<|<|>))", &tmp);
  newLine += tmp + " ";
  line.erase(line.size() - 1, 1);
  idx = line.rfind(" ");
  line.erase(0, idx + 1);
  if (checkNumber(line) == true)
    newLine += "NB";
  else
    newLine += "V";
  newLine += ")";
}

/**
 ** Effectue un travail sur la ligne pour tokeniser un cast
 ** @param line Ligne travaillée (consommé au fur et à mesure)
 ** @param newLine Nouvelle ligne (remplie au fur et à mesure)
 */
void					ALanguage::cast(std::string &line, std::string &newLine)
{
  std::string				castLine = "";

  RE2::PartialMatch(line, "(\\s+\\(.*\\)\\s+)", &castLine);
  trimLine(castLine);
  newLine = " V = " + castLine + "V;";
}


/**
 ** Effectue un travail sur la ligne pour tokeniser une opération sur la mémoire
 ** @param line Ligne travaillée (consommé au fur et à mesure)
 ** @param newLine Nouvelle ligne (remplie au fur et à mesure)
 */
void					ALanguage::mem(std::string &line, std::string &newLine)
{
  if (line.find("  MEM[") == 0)
    newLine = " MEM[...] = V;";
  else
    newLine = " V = MEM[...];";
}


/**
 ** Effectue un travail sur la ligne pour tokeniser une opération
 ** @param line Ligne travaillée (consommé au fur et à mesure)
 ** @param newLine Nouvelle ligne (remplie au fur et à mesure)
 */
void					ALanguage::ope(std::string &line, std::string &newLine)
{
  int					idx;

  line.erase(line.size() - 1, 1);
  idx = line.rfind(" ");
  if (line[idx - 1] == ')')
    idx = line.rfind("(", idx - 1) - 1;
  if (checkNumber(&line[idx + 1]) == true)
    {
      line.erase(0, idx - 1);
      line.erase(1, line.size() - 1);
      line += " NB";
    }
  else
    {
      line.erase(0, idx - 1);
      idx = line.find(" ");
      line.erase(idx, line.size() - idx);
      line += " V";
    }
  trimLine(line);
  newLine = " V = V " + line + ";";
}


/**
 ** Supprime le chemin passé en paramètre s'il est dans la liste des release à supprimer
 ** @param file Chemin
 */
bool					ALanguage::cleanRelease(const std::string &file)
{
  boost::system::error_code ign;
  for (ListString::const_iterator it = this->_releaseToClean.begin();
       it != this->_releaseToClean.end(); ++it)
    {
      if (RE2::PartialMatch(file, (*it)) == true)
	{
	  boost::filesystem::remove(file, ign);
	  return (true);
	}
    }
  return (false);
}

/*
** Cette fonction ouvre le fichier login_x-promo.gram si il est existant
** Puis il récupère tout les tokens qui y sont stocké
**
** Cette fonction permet de ne pas regénérer le fichier .gram si il est déja présent et donc de gagner de temps
** Ps: c'est dans la même idée que make
*/
VectorToken				ALanguage::getCache(Release *release, const std::string &path)
{
  VectorToken				token;
  std::ifstream				in;
  std::string				line;

  in.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".gram").c_str());
  if (in.is_open() == true)
    {
      while (in.eof() == false)
  	{
  	  std::getline(in, line);
	  if (line != "")
	    token.push_back(line);
  	}
    }
  return (token);
}

/**
 ** Point d'entrée des threads
 ** Cette fonction compile un fichier passé en paramètre et génére un .concat
 **
 ** En détail :
 **
 ** 1/ On récupére le path du fichier
 ** 2/ On effectue une commande shell à l'aide de la fonction system() pour se déplacer
 **    dans le répertoire ou se situe le fichier et on compile le fichier à l'aide des flags
 **    donné précédemment par la fonction getCompilationOptions()
 ** 3/ On accapare le mutex général (voir Instance.hh - getMutexInstance())
 ** 4/ On copie (ajoute) l'intégralité du fichier généré par la compilation dans un fichier .concat
 **
 ** @param param Structure contenant les informations sur la compilation
 */
void					*ALanguage::compile(void *param)
{
  t_compile				*comp = reinterpret_cast<t_compile *>(param);
  std::string				filename;
  size_t				found;
  std::string				current;

  do {
    {
      comp->mList->Lock();
      if (comp->l.empty()) {
	current = "";
      }
      else {
	current = comp->l.front();
	comp->l.pop_front();
      }
      comp->mList->Unlock();
    }
    if (current.empty() == false) {
      filename = getFileName(current);
      Logger::getSingleton()->log("Compiling file '" + current + "'", WARNING);
      found = current.find_last_of('/');
      system(std::string("cd " + current.substr(0, found + 1) + " && " +
			 comp->compilator + " " + comp->flags + " " + filename).c_str());
      comp->mConcat->Lock();
      ALanguage::copyDataToConcatFile(comp, current);
      comp->mConcat->Unlock();
    }
  } while (current.empty() == false);
  return (NULL);
}

/**
 ** Cette fonction copie le fichier compilé par compile() dans le fichier .concat
 ** puis supprime le fichier compilé par compile()
 ** @param comp Les informations de compilation
 ** @param path Le chemin du fichier à compiler
 */
void					ALanguage::copyDataToConcatFile(t_compile *comp, const std::string &path)
{
  std::string				line;
  std::ofstream				out;
  std::ifstream				in;

  in.open(std::string(path + comp->genFileExtent).c_str());
  if (in.is_open() == true)
    {
      out.open(std::string(comp->path + comp->release->getLogin() + "-" +
			   stringToInt(comp->release->getPromo()) +
			   ".concat").c_str(), std::ios::app);
      if (out.is_open() == true)
	{
	  out << "## " << path << std::endl;
	  while (in.eof() == false)
	    {
	      std::getline(in, line);
	      out << line << std::endl;
	    }
	  out.close();
	}
      else
	Logger::getSingleton()->log("impossible to open '" +
				    comp->path + comp->release->getLogin() + "-" +
				    stringToInt(comp->release->getPromo()) +
				    ".concat'", FORCE_ERROR);
      in.close();
      boost::system::error_code ign;
      boost::filesystem::path p = path + comp->genFileExtent;
      boost::filesystem::remove(p, ign);
    }
  else
    Logger::getSingleton()->log("impossible to open '" + path + comp->genFileExtent + "'", FORCE_ERROR);
}

/**
 ** Cette fonction permet de compiler chaque fichier compilable par le langage utilisé
 ** Exemple : ALanguage est hérité par CPP_Language, on compilera tout les fichiers .cpp
 **
 ** Dans le détail :
 **
 ** 1/ Récupére tout les fichiers de la release
 ** 2/ Récupère la liste des fichiers compilables grâce à la fonction getFileToBeCompiled()
 ** 3/ Récupère les options de compilation
 ** 4/ Pour chaque fichier
 **    5/ On crée un Thread (Dans la limite de MAX_THREAD cf: ALanguage.hh)
 **    6/ On crée un objet t_compile, on set ses attributs et on compile par appel à la fonction compile()
 ** 7/ On attend que chaque thread se termine
 ** 8/ On détruit chaque thread
 */
void					ALanguage::preprocess(Release *release, const std::string &p)
{
  t_compile*				comp;
  std::map<std::string, ListString>	files;

  if (release->getTree().getTreeByKey(_name).size() == 0)
    return;
  getFiles(release->getTree().getTreeByKey(_name), files);
  comp = new t_compile;
  comp->l = getFileToBeCompiled(files);
  comp->flags = getCompilationOptions(files);
  comp->path = p;
  comp->compilator = this->_compilator;
  comp->languageName = this->_name;
  comp->genFileExtent = this->_genFileCompExtent;
  comp->release = release;
  comp->mConcat = Instance::getMutexInstance();
  comp->mList = Instance::getMutexInstance();
  for (int i = 0; i < MAX_THREADS; ++i) {
    _thPool[i]->Create(&compile, comp);
  }
  for (int i = 0; i < MAX_THREADS; ++i) {
    _thPool[i]->Wait();
  }
  comp->mConcat->Destroy();
  comp->mList->Destroy();
  delete comp->mConcat;
  delete comp->mList;
}

/**
** Cette fonction parse le fichier .concat créé par la fonction compile() et récupére des tokens
**
** En détail :
**
** 1/ Récupére tout les scopes compatible avec les scopes situés dans this->_vScope 
** 2/ Crée/ouvre un fichier path/login_x-promo.gram
** 3/ On va remplir une liste de token grâce à la fonction startTokenization()
** 4/ Ferme le fichier .gram
**
*/
VectorToken				ALanguage::lexer(Release *release, const std::string &path)
{
  VectorToken					token;
  std::map<std::string, std::list<ListString> >	scopedData;
  std::ofstream					gram;

  if (release->getTree().getTreeByKey(_name).size() == 0)
    return (token);
  getScopedData(release, path, scopedData);
  trimScopedData(scopedData); // Cette fonction ne sert à rien
  gram.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".gram").c_str());
  if (gram.is_open() == false) {
    Logger::getSingleton()->log("impossible to open '" + path + release->getLogin() +
				"-" + stringToInt(release->getPromo()) + ".gram'", FORCE_ERROR);
    return (token);
  }
  startTokenization(scopedData, token, gram);
  gram.close();
  return (token);
}

/**
** Simple getter
*/
const std::string			&ALanguage::getLanguageName() const
{
  return (_name);
}

/**
** Le but de la fonction est d'enlever chaque espace superflu contenu dans une chaine de type string
**
** Exemple :
**         avant   - "  je suis  une  phrase  "
**         après   - "je suis une phrase"
*/
void					ALanguage::trimLine(std::string &line)
{
  int					idx = 0;

  while (line.find(" ") == 0)
    line.erase(0, 1);
  while ((idx = line.find(" ", idx + 1)) != -1)
    if (line[idx + 1] == ' ')
      line.erase(idx, 1);
  while (line.rfind(" ") == line.size() - 1)
    line.erase(line.size() - 1, 1);
}

/**
** Cette fonction vérifie si la chaine passée en paramètre est un nombre
*/
bool					ALanguage::checkNumber(const std::string &line)
{
  size_t				i;

  for (i = 0; (i < line.size() && line[i] >= '0' && line[i] <= '9') || line[i] == '-'; ++i);
  if (i != line.size())
    return (false);
  return (true);
}

/**
**
*/
void					ALanguage::getParam(std::string &line, std::string &newLine, int idx)
{
  std::string				tmp;

  tmp = line.substr(0, idx);
  line.erase(0, idx + 2);
  if (static_cast<int>(tmp.find("\"")) != -1)
    newLine += "STRING";
  else if (checkNumber(tmp) == true)
    newLine += "NB";
  else
    newLine += "V";
}

/**
** On récupére tout les fichiers dont l'extension est présente dans this->_filesTypes
**
** Exemple :
**
** Module CPP
**
** Fichiers de type (.h .hh .cpp) etc ...
**
*/
void					ALanguage::getFiles(VectorString &tree, std::map<std::string, ListString> &files)
{
  VectorString::iterator		it;

  it = tree.begin();
  while (it != tree.end())
    {
      for(std::map<std::string, std::string>::const_iterator itt = this->_filesTypes.begin();
	  itt != this->_filesTypes.end(); ++itt)
	{
	  if (RE2::PartialMatch(*it, itt->second) == true)
	    {
	      files[itt->first].push_back(*it);
	      break;
	    }
	}
      ++it;
    }
}

/**
** Cette fonction enlève un élément à la liste passé en argument1 et retourne cet élément
*/
ListString				ALanguage::split(ListString &l, int size)
{
  ListString				newList;

  for (int i = 0; i < size && l.empty() == false; ++i)
    {
      newList.push_back(l.back());
      l.pop_back();
    }
  return (newList);
}

/**
** Methode qui retourne les options de compilation (Cette méthode est redéfinie par les classes filles de ALangage)
*/
std::string				ALanguage::getCompilationOptions(std::map<std::string, ListString> &files)
{
  (void)files;
  return ("");
}

/**
** Cette fonction retourne la liste des fichier à compiler
**
** Exemple :
**
** Module CPP
**
** Fichiers de type .hh .h .cpp -> extrait seulement les .cpp
**
** Permet de faire un tri et de ne prendre que les fichiers compilables
**
*/
ListString				ALanguage::getFileToBeCompiled(std::map<std::string, ListString> &files)
{
  ListString				tmp;

  if (this->_fileTypeToCompile != std::string("") && files[this->_fileTypeToCompile].size())
    return (files[this->_fileTypeToCompile]);
  return (tmp);
}

/**
** Cette fonction récupére tout les scopes matchant avec les scopes de this->_vScopes
*/
bool					ALanguage::getScopedData(Release *release,
								 const std::string &path,
								 std::map<std::string,
								 std::list<ListString> >& data)
{
  std::ifstream				in;
  std::string				line;
  std::string				filename;
  ListString				scope;
  bool					inScope;
  std::vector<Scope>::iterator		sit;

  inScope = false;
  in.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".concat").c_str());
  if (in.is_open() == true)
    {
      while (in.eof() == false)
  	{
  	  std::getline(in, line);
  	  if (inScope == false && RE2::PartialMatch(line, "^##") == true)
  	    filename = line.substr(3);
  	  else
  	    {
	      if (inScope == false)
		for (sit = this->_skipScopes.begin();
		     sit != this->_skipScopes.end(); ++sit)
		  {
		    if (RE2::PartialMatch(line, (*sit)._regexBegin) == true)
		      {
			while (RE2::PartialMatch(line, (*sit)._regexEnd) != true && in.eof() == false)
			  std::getline(in, line);
		      }
		  }
	      if (inScope == true || sit == this->_skipScopes.end())
		for (std::vector<Scope>::iterator it = this->_vScopes.begin();
		     it != this->_vScopes.end(); ++it)
		  {
		    if (inScope == false && RE2::PartialMatch(line, (*it)._regexBegin) == true)
		      {
			inScope = true;
			break;
		      }
		    else if (inScope == true && RE2::PartialMatch(line, (*it)._regexEnd) == true)
		      {
			scope.push_back(line);
			data[filename].push_back(scope);
			scope.clear();
			inScope = false;
			break;
		      }
		  }
  	      if (inScope == true)
  		scope.push_back(line);
  	    }
  	}
      in.close();
    }
  else
    {
      Logger::getSingleton()->log("impossible to open '" + path + release->getLogin() + "-"
				  + stringToInt(release->getPromo()) + ".concat'", FORCE_ERROR);
      return (false);
    }
  return (true);
}

/**
** Cette fonction fait ... En fait non elle fait rien
*/
void					ALanguage::trimScopedData(std::map<std::string, std::list<ListString> >& data)
{
  (void)data;
}

/**
** On ajoute le gram à la liste de token et on réinitialise le gram
*/
void					ALanguage::pushGram(std::string &gram, VectorToken &token, std::ofstream* out)
{
  if (gram != "")
    {
      token.push_back(gram);
      if (out != NULL)
	(*out) << gram << std::endl;
      gram = "";
    }
}

/**
** Permet de passer quelques lignes
*/
void					ALanguage::tokenizationSkipLine(ListString& list,
									ListString::iterator& it,
									unsigned int nbLines,
									const std::string& toLine)
{
  if (nbLines > 0)
    while (nbLines != 0 && it != list.end())
      {
	++it;
	nbLines--;
      }
  else
    {
      ++it;
      while (it != list.end() && RE2::PartialMatch(*it, toLine) == false)
	++it;
    }
}

/**
** Cette fonction crée une liste de token
**
** Si la string match avec un élément de this->_vSeparator alors on pushGram()
** Si la string match avec un élément de this->_skip alors on skip la ligne
** Si la string match avec un token on ajoute le token au gram
**
** Crée des gram du style : 01_COND_02_GOTO_01_COND_
** Génére un VectorToken du style : [{01_COND_01_COND_}, {01_COND_02_GOTO_01_COND_}, {02_GOTO_01_COND_02_GOTO_}]
**
*/
void					ALanguage::tokenizeList(ListString &list,
								std::string &gram,
								VectorToken &token,
								std::ofstream *out)
{
  VectorString::const_iterator			mit;
  std::vector<SkipLines>::const_iterator	its;
  std::vector<Tokens>::const_iterator		itt;

  for (ListString::iterator it = list.begin();
       it != list.end();)
    {
      for (mit = this->_vSeparator.begin(); mit != this->_vSeparator.end(); ++mit)
	if (RE2::PartialMatch(*it, *mit) == true)
	  {
	    pushGram(gram, token, out);
	    break;
	  }
      for (its = this->_skip.begin(); its != this->_skip.end(); ++its)
	if (RE2::PartialMatch(*it, its->_regex) == true)
	  {
	    tokenizationSkipLine(list, it, its->_nbLines, its->_toLine);
	    break;
	  }
      if (its == this->_skip.end())
	{
	  for (itt = this->_vTokens.begin(); itt != this->_vTokens.end(); ++itt)
	    if (RE2::PartialMatch(*it, itt->_regex) == true)
	      {
		gram += itt->_token;
		tokenizationSkipLine(list, it, itt->_nbLines, itt->_toLine);
		break;
	      }
	  if (itt == this->_vTokens.end())
	    ++it;
	}
    }
}

/**
** Pour chaque scope on va créer une liste de token et ajouter des token dans le .gram
*/
bool					ALanguage::startTokenization(std::map<std::string,
								     std::list<ListString> > &data,
								     VectorToken &token,
								     std::ofstream &out)
{
  std::string				gram;

  for (std::map<std::string, std::list<ListString> >::iterator itf = data.begin();
       itf != data.end(); ++itf)
    {
      for (std::list<ListString>::iterator its = itf->second.begin();
	   its != itf->second.end(); ++its)
	{
	  tokenizeList(*its, gram, token, &out);
	  pushGram(gram, token, &out);
	}
    }
  return (true);
}

/**
**
*/
void					ALanguage::setFuncName(std::string &line, Func &func, std::string *funcName)
{
  std::string				name = "unknown";
  StaticFuncPtr				fptr;

  for (std::vector<Scope>::iterator it = this->_vScopes.begin();
       it != this->_vScopes.end(); ++it)
    {
      if (RE2::PartialMatch(line, (*it)._regexBegin, &name) == true)
	{
	  if ((fptr = (*it)._func) != NULL)
	    (*fptr)(*it, line, name);
	  else if (name == "")
	    name = "unknown";
	  func.setName(name);
	  break;
	}
    }
  if (funcName != NULL)
    *funcName = name;
}

/**
**
*/
void					ALanguage::createFunc(const std::string &path,
							      ListString& scope,
							      VectorFunc &v_func,
							      VectorString &v_content,
							      std::ofstream& out)
{
  std::vector<SkipLines>::const_iterator	its;
  std::vector<Tokens>::const_iterator		itt;
  Func						func;
  FuncPtr					fptr;
  std::string					newLine;
  std::string					funcName;

  setFuncName(scope.front(), func, &funcName);
  for (VectorString::const_iterator ign = this->_ignFuncs.begin(); ign != this->_ignFuncs.end(); ++ign)
  {
    if (RE2::PartialMatch(*ign, funcName) == true)
      return;
  }
  out << "## -> " + funcName + " (starting scope)" << std::endl;
  func.setPath(path);
  for (ListString::iterator it = scope.begin();
       it != scope.end();)
    {
      for (its = this->_skip.begin(); its != this->_skip.end(); ++its)
	if (RE2::PartialMatch(*it, its->_regex) == true)
	  {
	    tokenizationSkipLine(scope, it, its->_nbLines, its->_toLine);
	    break;
	  }
      if (its == this->_skip.end())
	{
	  for (itt = this->_vTokens.begin(); itt != this->_vTokens.end(); ++itt)
	    if (RE2::PartialMatch(*it, itt->_regex) == true)
	      {
		if ((fptr = (*itt)._func) != NULL)
		  {
		    newLine.clear();
		    (this->*fptr)(*it, newLine);
		    v_content.push_back(newLine);
		    func.addContent(newLine);
		    out << newLine << std::endl;
		  }
		tokenizationSkipLine(scope, it, itt->_nbLines, itt->_toLine);
		break;
	      }
	  if (itt == this->_vTokens.end())
	    ++it;
	}
    }
  out << "## <- (ending scope)" << std::endl;
  sort(func.content().begin(), func.content().end(), StaticFunc::compareString);
  v_func.push_back(func);
}

/**
**
*/
void					ALanguage::storeScopeInConcatStrip(std::map<std::string,
									   std::list<ListString> >& scopedData,
									   VectorFunc &v_func,
									   VectorString &v_content,
									   std::ofstream& out)
{
  for (std::map<std::string, std::list<ListString> >::iterator itf = scopedData.begin();
       itf != scopedData.end(); ++itf)
    {
      out << "## " << itf->first << std::endl;
      for (std::list<ListString>::iterator its = itf->second.begin();
	   its != itf->second.end(); ++its)
	createFunc(itf->first, *its, v_func, v_content, out);
    }
}

/**
**
*/
void					ALanguage::createFileConcatStrip(Release *release,
									 VectorFunc &v_func,
									 VectorString &v_content,
									 const std::string &path)
{
  std::map<std::string, std::list<ListString> >	scopedData;
  std::ofstream					concat_strip;

  if (release->getTree().getTreeByKey(_name).size() == 0)
    return;
  getScopedData(release, path, scopedData);
  trimScopedData(scopedData);
  concat_strip.open(std::string(path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".concat_strip").c_str());
  if (concat_strip.is_open() == false) {
    Logger::getSingleton()->log("impossible to open '" + path + release->getLogin() + "-" + stringToInt(release->getPromo()) + ".concat_strip'", FORCE_ERROR);
    return;
  }
  storeScopeInConcatStrip(scopedData, v_func, v_content, concat_strip);
  sort(v_content.begin(), v_content.end(), StaticFunc::compareString);
  concat_strip.close();
}

/**
**
*/
void					ALanguage::createFunc(std::ifstream& concat_strip,
							      VectorFunc &v_func,
							      VectorString &v_content,
							      const std::string &path,
							      const std::string& funcName)
{
  Func					func;
  std::string				line;

  func.setPath(path);
  func.setName(funcName);
  while (concat_strip.eof() == false)
    {
      std::getline(concat_strip, line);
      if (line == "## <- (ending scope)")
	break;
      v_content.push_back(line);
      func.addContent(line);
    }
  sort(func.content().begin(), func.content().end(), StaticFunc::compareString);
  v_func.push_back(func);
}

/**
**
*/
void					ALanguage::getDataFromConcatStrip(VectorFunc &v_func,
									  VectorString &v_content,
									  const std::string &filename)
{
  std::ifstream				concat_strip;
  std::string				line;
  std::string				tmp;
  std::string				path = "";
  std::string				funcName;

  concat_strip.open(filename);
  if (concat_strip.is_open() == false) {
    Logger::getSingleton()->log("impossible to open '" + filename + "'", FORCE_ERROR);
    return;
  }
  while (concat_strip.eof() == false)
    {
      std::getline(concat_strip, line);
      if (RE2::PartialMatch(line, "^## -> (.*) \\(starting scope\\)$", &funcName) == true)
	createFunc(concat_strip, v_func, v_content, path, funcName);
      else if (RE2::PartialMatch(line, "^## (.*)", &tmp) == true)
	path = tmp;
    }
  sort(v_content.begin(), v_content.end(), StaticFunc::compareString);
  concat_strip.close();
}

/**
**
*/
int					ALanguage::matchLineByLine(const VectorString &vLog1,
								   const VectorString &vLog2)
{
  int					count = 0;
  VectorString::const_iterator		itLog1, itLog2;

  itLog1 = vLog1.begin();
  itLog2 = vLog2.begin();
  while (itLog1 != vLog1.end() && itLog2 != vLog2.end())
    {
      if (*itLog1 == *itLog2)
	{
	  ++count;
	  ++itLog1;
	  ++itLog2;
	}
      else if ((*itLog1).compare(*itLog2) < 0)
      	++itLog1;
      else
	++itLog2;
    }
  return (count);
}

/**
**
*/
void					ALanguage::checkString(int &totalScope,
							       std::string line)
{
  int					total, idx_begin, idx_end;

  if ((idx_begin = line.find('"')) >= 0)
    {
      if ((idx_end = line.find('"', idx_begin + 1)) >= 0)
        line.erase(idx_begin, idx_end - idx_begin);
      else
        line.erase(idx_begin, line.size() - idx_begin);
    }
  if ((total = count(line.begin(), line.end(), '{')) > 0)
    totalScope += total;
  if ((total = count(line.begin(), line.end(), '}')) > 0)
    totalScope -= total;
}

/**
**
*/
bool					ALanguage::loadScope(std::ifstream &in,
							     std::string &line,
							     Scope &scope,
							     Func &func)
{
  int					totalScope;

  totalScope = 0;
  func.addFinalContent(line);
  checkString(totalScope, line);
  while (in.eof() == false)
    {
      std::getline(in, line);
      func.addFinalContent(line);
      checkString(totalScope, line);
      if (RE2::PartialMatch(line, scope._regexEnd) == true && totalScope == 0)
	return (true);
    }
  return (false);
}

/**
**
*/
bool					ALanguage::skipScope(std::ifstream& in, std::string& line, Scope& scope)
{
  int					totalScope;

  totalScope = 0;
  checkString(totalScope, line);
  while (in.eof() == false)
    {
      std::getline(in, line);
      checkString(totalScope, line);
      if (RE2::PartialMatch(line, scope._regexEnd) == true && totalScope == 0)
	return (true);
    }
  return (false);
}

/**
**
*/
bool					ALanguage::getFuncFromSrcsFile(const std::string &name,
								       const std::string &path,
								       Func &func)
{
  StaticFuncPtr				fptr;
  std::ifstream				in;
  std::string				line;
  std::string				tmpname;

  func.setName(name);
  in.open(path.c_str());
  if (in.is_open() == true)
    {
      while (in.eof() == false)
        {
	  std::getline(in, line);
	  for (std::vector<Scope>::iterator it = this->_vCodeScopes.begin();
	       it != this->_vCodeScopes.end(); ++it)
	    {
	      if (RE2::PartialMatch(line, (*it)._regexBegin, &tmpname) == true)
		{
		  if ((fptr = (*it)._func) != NULL)
		    (*fptr)(*it, line, tmpname);
		  else if (tmpname == "")
		    tmpname = "unknown";
		  if (name == tmpname)
		    {
		      loadScope(in, line, *it, func);
		      return (true);
		    }
		  skipScope(in, line, *it);
		  break;
		}
	    }
	}
      in.close();
    }
  else
    Logger::getSingleton()->log("Unable to open '" + path + "'", FORCE_ERROR);
  return (false);
}

/**
**
*/
bool					ALanguage::calcPercentMatch(Match &match, int &count)
{
  std::vector<std::pair<Func, Func> >::iterator	it;
  bool					find = true;

  it = match.func().begin();
  if (it == match.func().end()) {
    return (false);
  }
  if (it->first.getPercent() >= 70 && it->second.getPercent() >= 60)
    {
      count += matchLineByLine((*it).first.getContent(), (*it).second.getContent());
      if (!getFuncFromSrcsFile(it->first.getName(), it->first.getPath(), it->first) ||
	  !getFuncFromSrcsFile(it->second.getName(), it->second.getPath(), it->second))
	find = false;
      ++it;
      match.func().erase(it, match.func().end());
      return (find);
    }
  match.func().erase(it, match.func().end());
  return (find);
}

/**
**
*/
void					ALanguage::matchFuncByFunc(VectorFunc &v_funcLog1,
								   VectorFunc &v_funcLog2,
								   std::vector<Match> &v_match,
								   int &count)
{
  VectorFunc::iterator			it1, it2;
  int					tmp_count = 0;
  Match					match;

  for (it1 = v_funcLog1.begin(); it1 != v_funcLog1.end(); ++it1)
    {
      for (it2 = v_funcLog2.begin(); it2 != v_funcLog2.end(); ++it2)
	{
	  tmp_count = matchLineByLine((*it1).getContent(), (*it2).getContent());
	  (*it1).setNbHit(tmp_count);
	  (*it2).setNbHit(tmp_count);
	  (*it1).setPercent(((*it1).getContent().size() > 0 ? tmp_count * 100 / (*it1).getContent().size() : 0));
	  (*it2).setPercent(((*it2).getContent().size() > 0 ? tmp_count * 100 / (*it2).getContent().size() : 0));
	  match.addFunc(std::make_pair(*it1, *it2));
	}
      sort(match.func().begin(), match.func().end(), StaticFunc::greaterMatch);
      if (calcPercentMatch(match, count) && match.func().size() > 0)
	v_match.push_back(match);
      match.func().clear();
    }
}

/**
**
*/
void					ALanguage::workOnCheater(VectorFunc&, VectorString&)
{
}

/**
**
*/
std::vector<Match>			ALanguage::match(Cheaters * cheaters, const std::string &p)
{
  Release				*r;
  std::string				fileName;
  VectorString				v_contentLog1, v_contentLog2;
  VectorFunc				v_funcLog1, v_funcLog2;
  int					count;
  std::pair<float, float>		percent;
  std::vector<Match>			v_match;


  if (Option::getSingleton()->hasOption(Option::HARDVERBOSE)) {
    Logger::getSingleton()->log(cheaters->getCouple().first->getLogin() + " - "
				+ cheaters->getCouple().second->getLogin() + " - " + p, NONE);
  }
  r = cheaters->getCouple().first;
  fileName = p + r->getLogin() + "-" + stringToInt(r->getPromo()) + ".concat_strip";
  if (!exists(path(fileName)))
    createFileConcatStrip(r, v_funcLog1, v_contentLog1, p);
  else
    getDataFromConcatStrip(v_funcLog1, v_contentLog1, fileName);
  workOnCheater(v_funcLog1, v_contentLog1);
  r = cheaters->getCouple().second;
  fileName = p + r->getLogin() + "-" + stringToInt(r->getPromo()) + ".concat_strip";
  if (!exists(path(fileName)))
    createFileConcatStrip(r, v_funcLog2, v_contentLog2, p);
  else
    getDataFromConcatStrip(v_funcLog2, v_contentLog2, fileName);
  workOnCheater(v_funcLog2, v_contentLog2);
  count = matchLineByLine(v_contentLog1, v_contentLog2);
  cheaters->setNbHit(count);
  count = 0;
  matchFuncByFunc(v_funcLog1, v_funcLog2, v_match, count);
  percent = std::make_pair(static_cast<float>(v_contentLog1.size() > 0 ? count * 100 / v_contentLog1.size() : 0),
 			   static_cast<float>(v_contentLog2.size() > 0 ? count * 100 / v_contentLog2.size() : 0));
  cheaters->setPercent(percent);
  return (v_match);
}

/**
**
*/
std::string				getFileName(const std::string &path)
{
  int					i = path.size();

  for (; i > 0 && path[i] != '/'; --i);
  return (path.substr(i + 1, path.size()));
}
