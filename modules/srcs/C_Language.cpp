#include "C_Language.hh"
#include "Config.hh"

C_Language::C_Language() : ALanguage("C", "gcc", Config::getSingleton()->getModuleFlag("C"))
{
  VectorString	ext;
  VectorString	shebang;

  _filesTypes["SRC"] = "(.*\\.(c|cc)$)";
  _filesTypes["HDS"] = "(.*\\.(h)$)";
  _releaseToClean.push_back(".*(Makefile|a.out)$");
  _releaseToClean.push_back(".*\\.(o)$");
  _releaseToClean.push_back(".*\\.c.\\d+t\\.optimized");
  _fileTypeToCompile = "SRC";

  _vScopes.push_back(Scope("([a-zA-Z0-9_]+)\\s*\\(.*", "\\}"));

  _vCodeScopes.push_back(Scope("([a-zA-Z0-9_]+)\\s*\\(.*", "\\}"));

  /**
   * Fonctions de la libmy
   */
  _ignFuncs.push_back("my_putchar");
  _ignFuncs.push_back("my_isneg");
  _ignFuncs.push_back("my_put_nbr");
  _ignFuncs.push_back("my_swap");
  _ignFuncs.push_back("my_putstr");
  _ignFuncs.push_back("my_strlen");
  _ignFuncs.push_back("my_getnbr");
  _ignFuncs.push_back("my_sort_int_tab");
  _ignFuncs.push_back("my_power_rec");
  _ignFuncs.push_back("my_square_root");
  _ignFuncs.push_back("my_is_prime");
  _ignFuncs.push_back("my_find_prime_sup");
  _ignFuncs.push_back("my_strcpy");
  _ignFuncs.push_back("my_strncpy");
  _ignFuncs.push_back("my_revstr");
  _ignFuncs.push_back("my_strstr");
  _ignFuncs.push_back("my_strcmp");
  _ignFuncs.push_back("my_strncmp");
  _ignFuncs.push_back("my_strupcase");
  _ignFuncs.push_back("my_strlowcase");
  _ignFuncs.push_back("my_strcapitalize");
  _ignFuncs.push_back("my_str_isalpha");
  _ignFuncs.push_back("my_str_isnum");
  _ignFuncs.push_back("my_str_islower");
  _ignFuncs.push_back("my_str_isupper");
  _ignFuncs.push_back("my_str_isprintable");
  _ignFuncs.push_back("my_showstr");
  _ignFuncs.push_back("my_showmem");
  _ignFuncs.push_back("my_strcat");
  _ignFuncs.push_back("my_strncat");
  _ignFuncs.push_back("my_strlcat");

  /**
   * Fonctions de la minilibx (mlx.h)
   */
  _ignFuncs.push_back("mlx_init");
  _ignFuncs.push_back("mlx_new_window");
  _ignFuncs.push_back("mlx_clear_window");
  _ignFuncs.push_back("mlx_pixel_put");
  _ignFuncs.push_back("mlx_new_image");
  _ignFuncs.push_back("mlx_get_data_addr");
  _ignFuncs.push_back("mlx_put_image_to_window");
  _ignFuncs.push_back("mlx_get_color_value");
  _ignFuncs.push_back("mlx_mouse_hook");
  _ignFuncs.push_back("mlx_key_hook");
  _ignFuncs.push_back("mlx_expose_hook");
  _ignFuncs.push_back("mlx_loop_hook");
  _ignFuncs.push_back("mlx_loop");
  _ignFuncs.push_back("mlx_string_put");
  _ignFuncs.push_back("mlx_xpm_to_image");
  _ignFuncs.push_back("mlx_xpm_file_to_image");
  _ignFuncs.push_back("mlx_destroy_window");
  _ignFuncs.push_back("mlx_hook");
  _ignFuncs.push_back("mlx_do_key_autorepeatoff");
  _ignFuncs.push_back("mlx_do_key_autorepeaton");
  _ignFuncs.push_back("mlx_do_sync");

  /**
   * Fonctions de la minilibx (mlx_int.h)
   */
  _ignFuncs.push_back("mlx_int_do_nothing");
  _ignFuncs.push_back("mlx_int_get_good_color");
  _ignFuncs.push_back("mlx_int_find_in_pcm");
  _ignFuncs.push_back("mlx_int_anti_resize_win");
  _ignFuncs.push_back("mlx_int_wait_first_expose");
  _ignFuncs.push_back("mlx_int_rgb_conversion");
  _ignFuncs.push_back("mlx_int_deal_shm");
  _ignFuncs.push_back("mlx_int_new_xshm_image");
  _ignFuncs.push_back("mlx_int_str_to_wordtab");
  _ignFuncs.push_back("mlx_new_image");
  _ignFuncs.push_back("shm_att_pb");


  _vTokens.push_back(Tokens("if\\s+\\(.*\\)$",                     "01_COND_",    &ALanguage::cond,	4));
  _vTokens.push_back(Tokens("^\\s+goto\\s+<bb\\s+\\d+>;$",         "02_GOTO_",    NULL));
  _vTokens.push_back(Tokens("\\Wwrite\\s+\\(.+\\)",                "03_WRITE_",   &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wopen\\s+\\(.+\\)",                 "04_OPEN_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wclose\\s+\\(.+\\)",                "05_CLOSE_",   &ALanguage::func));
  _vTokens.push_back(Tokens("\\Ws?brk\\s+\\(.+\\)",                "06_BRK_",     &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wmalloc\\s+\\(.+\\)",               "07_MALLOC_",  &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wfree\\s+\\(.+\\)",                 "08_FREE_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\W[fl]?stat\\s+\\(.+\\)",            "09_STAT_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\W(u|nano)?sleep\\s+\\(.+\\)",       "10_SLEEP_",   &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wtime\\s+\\(.+\\)",                 "11_TIME_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\Waccept\\s+\\(.+\\)",               "12_ACCEPT_",  &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wbind\\s+\\(.+\\)",                 "13_BIND_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wconnect\\s+\\(.+\\)",              "14_CONNECT_", &ALanguage::func));
  _vTokens.push_back(Tokens("\\Wselect\\s+\\(.+\\)",               "15_SELECT_",  &ALanguage::func));
  _vTokens.push_back(Tokens("\\W\\w+\\s+\\(.*\\)",                 "16_FUNC_",    &ALanguage::func));
  _vTokens.push_back(Tokens("\\WPHI\\W<.*>",                       "17_PHI_",     NULL));
  _vTokens.push_back(Tokens("\\Wreturn\\s*.*;$",                   "18_RET_",     NULL));
  _vTokens.push_back(Tokens("(.+)\\s+=\\s+\\(.+\\)\\s+.+;$",       "19_CAST_",    &ALanguage::cast));
  _vTokens.push_back(Tokens(".+\\s+=\\s+.+(\\s+[-+*/%]\\s+.+)+;$", "20_OPE_",     &ALanguage::ope));
  _vTokens.push_back(Tokens("MEM\\[.+\\]",                         "21_MEM_",     &ALanguage::mem));
  _vTokens.push_back(Tokens(".+\\s+=\\s+.+;$",                     "22_ASSIGN_",  NULL));

  _skip.push_back(SkipLines("^$"));
  _skip.push_back(SkipLines("^Removing"));
  _skip.push_back(SkipLines("^Merging"));
  _skip.push_back(SkipLines("^;;"));
  _skip.push_back(SkipLines("^<"));
  _skip.push_back(SkipLines("^Invalid sum"));
  _skip.push_back(SkipLines("^\\s+(_Bool|size_t|ssize_t|float|double|int|struct|char)\\s(.*);$"));
  _skip.push_back(SkipLines("(^[ \t]*<bb\\s+\\d+>:$)"));

  _vSeparator.push_back("(^[ \t]*<bb\\s+\\d+>:$)");

  ext.push_back(".*\\.(h|c|cc)$");
  _identifier = std::make_pair(ext, shebang);
}

std::string				C_Language::getCompilationOptions(std::map<std::string, ListString> &files)
{
  VectorString				fileToAdd;
  std::string				path;
  VectorString::const_iterator		ita;

  for (ListString::const_iterator it = files["HDS"].begin();
       it != files["HDS"].end(); ++it)
    {
      path = realpath(it->substr(0, it->find_last_of("/")).c_str(), NULL);
      for (ita = fileToAdd.begin();
	   ita != fileToAdd.end(); ++ita)
	if (*ita == path)
	  break;
      if (ita == fileToAdd.end())
	fileToAdd.push_back(path);
    }
  path = "";
  for (ita = fileToAdd.begin();
       ita != fileToAdd.end(); ++ita)
    path += "-I" + *ita + " ";
  return ("-c -w -O3 -fno-inline -fdump-tree-optimized " + path);
}

extern "C"
{
  ILanguage			*create_module()
  {
    return (new C_Language);
  };
}
