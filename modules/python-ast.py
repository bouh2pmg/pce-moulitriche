"""
Build ast from file, optimizes and pretty print it.
"""

__author__ = 'Philip Garnero'

import sys
import argparse
import ast
from ast import AST as Node
from compiler.ast import flatten
from astoptimizer import parse_ast, optimize_ast
from astoptimizer.config import Config

__all__ = ('print_ast',)


def print_ast(ast, indent='  ', stream=sys.stdout, initlevel=0, info=True):
    "Pretty-print an AST to the given output stream."
    rec_node(ast, initlevel, indent, stream.write, info)
    stream.write('\n')

def rec_node(node, level, indent, write, info=True):
    "Recurse through a node, pretty-printing it."
    pfx = indent * level
    if isinstance(node, Node):
        write(pfx)
        write(node.__class__.__name__)
        if info:
            write('(')

        children = flatten((value for attr, value in ast.iter_fields(node)))
        if any(isinstance(child, Node) for child in children):
            for i, child in enumerate(children):
                if info and i != 0:
                    write(',')
                write('\n')
                rec_node(child, level+1, indent, write, info)

            if info:
                write('\n')
                write(pfx)

        elif info:
            write(', '.join(repr(value) for attr, value in ast.iter_fields(node)))
        if info:
            write(')')

    elif info:
        write(pfx)
        write(repr(node))

def clean_white_lines(fs):
    fs.seek(0)
    lines = fs.readlines()
    fs.seek(0)
    fs.truncate()
    for line in lines:
        if line != "\n":
            fs.write(line)

if __name__ == '__main__':
    args = argparse.ArgumentParser(description='Ast pretty printer for moulitriche', add_help=False)
    args.add_argument('--detailed', action="store_true")
    args.add_argument('--out', metavar='file.concat', type=str, required=True)
    args.add_argument('files', metavar='file', type=str, nargs='+')
    args = args.parse_args(sys.argv[1:])

    with open(args.out, "w+") as fs:
        for f in args.files:
            fs.write(f)
            fs.write('\n')
            try:
                tree = parse_ast(open(f, 'r').read(), filename=f)
                tree = optimize_ast(tree, config=Config(), filename=f)
                print_ast(tree, stream=fs, initlevel=1, info=args.detailed)
            except SyntaxError:
                print >> sys.stderr, "Couldn't get ast from %s" % f
        clean_white_lines(fs)
