#!/bin/sh

if [ $# -eq 5 ];
then
    name=$1
    path=$2
    semestre=$3
    module=$4
    promo=$5
    branch=$(git branch | grep '*' | cut -d' ' -f2)
    rev=$(git rev-parse HEAD)
    date=$(date -R)
    hostname=$(hostname)
    user=$USER
    name="$name $branch $rev $user@$hostname $date"
    ./xml_gen.sh "$name" "$path" "$semestre" "$module" "$promo"
    rm -rf "$path/C" "$path/CPP" "$path/Python"
    ./moulitriche ./conf.xml
else
    echo "Usage: $0 nom path semestre module promo"
    echo "Le nom du projet sera formaté de la manière suivante: \"\$name \$branch \$rev \$user@\$hostname \$date\""
    exit 1
fi