#!/bin/bash

if [[ $BASH_ARGC -eq 5 ]]
then
    echo "<?xml version=\"1.0\"?>" > conf.xml
    echo "<projects>" >> conf.xml
    echo "	<project>" >> conf.xml
    echo "		<name>$1</name>" >> conf.xml
    echo "		<path_project>$2</path_project>" >> conf.xml
    echo "		<semester>$3</semester>" >> conf.xml
    echo "		<module>$4</module>" >> conf.xml
    echo "		<promotion>$5</promotion>" >> conf.xml
    echo "	</project>" >> conf.xml
    echo -n "</projects>" >> conf.xml
else
    echo "Usage: ./xml_gen.sh [Nom du projet] [path] B[numero du semestre] [Module] [Promotion]"
fi